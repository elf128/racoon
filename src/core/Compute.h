/*
 * Worker.h
 *
 *  Created on: Oct 12, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#ifndef SRC_CORE_COMPUTE_H_
#define SRC_CORE_COMPUTE_H_

#include <thread>
#include <mutex>
#include <vector>
#include <atomic>

class NG_Node;
class Smoke;
class NG_Structure;
class Mirror;
class Reflection;

enum class NG_ExecMode
{
    Blocking,
    Lazy,
    Normal,
    Aggressive,
    Always
};

class NG_Compute
{
    struct ExecModeCase
    {
        bool ifNormal;
        bool ifForced;
    };

    struct ExecModeSet
    {
        ExecModeCase ifClean;
        ExecModeCase ifDirty;
    };

    static const int   queue_length = 256;
    static ExecModeSet shouldCompute[];

public:
    struct QueueItem
    {
        Smoke*  link;
        int     time;
        bool    post;
        bool    force;
        QueueItem() : link( nullptr ), time( 0 ), post( false ), force( false ) {}
    };

    class Queue
    {
        std::vector< QueueItem > tasks;
        size_t                   head;
        //size_t                   runner;
        size_t                   tail;
        std::recursive_mutex     lock;

        size_t getNext( size_t idx ) const { return ( idx == queue_length - 1 ) ? 0 : idx + 1; }

    public:
        Queue();

        bool       PushCompute( Smoke* p_node, const int time, bool isPost, bool allowDuplicates, const int threadIdx  );
        QueueItem* PopCompute( const int threadIdx );
        void       FlushQueue();
        void       Eliminate( Smoke* smoke );
    };

    class Worker
    {
    public:
        enum class State
        {
            Run,
            ToRun,
            ToIdle,
            Idle,
            Exit,
        };
    private:
        std::thread*         id;
        Queue*               queue;
        std::atomic< State > todo;
        int                  idx;

        template < class Unit >
        static void process( NG_Compute::Worker* self, Unit*, const QueueItem* task );
    public:
        Worker( Queue* p_queue, const int p_idx );
        ~Worker();

        Worker( const Worker& ) = delete;
        Worker& operator=( const Worker& ) = delete;
        Worker( Worker&& ) = delete;
        Worker& operator=( Worker&& ) = delete;

        inline void markForIdle() { todo = Worker::State::ToIdle; }
        inline void markForRun()  { todo = Worker::State::ToRun;  }
        inline bool ifIdle() const { return ( todo == Worker::State::Idle ); }
        static void run( void* );
    };

    static void       allocateWorkersPool( size_t p_count );
    static void       stopWorking();
    static void       resumeWorking();

    static void       FlushQueue();
    static bool       PushCompute( Smoke* r_node, const int time );
    static QueueItem* PopCompute();
    static void       Eliminate( Smoke* smoke );
private:
    static Queue                  queue;
    static std::vector< Worker* > pool;

};


#endif /* SRC_CORE_COMPUTE_H_ */
