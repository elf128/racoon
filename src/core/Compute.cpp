/*
 * Worker.cpp
 *
 *  Created on: Oct 12, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "Compute.h"

#include "model/Node.h"
#include "model/Structure.h"
#include "model/Reflection.h"
#include <chrono>
#include <type_traits>

#define VERBOSE_COMPUTE
#define DEPENDENCY_READY_SANITY_CHECK

#if defined( VERBOSE_COMPUTE )
#define ComputeLog( A ) Global::verbQ( A )
const char pre_post[2][8] = { "PRE", "POST" };
#else
#define ComputeLog( A ) {}
#endif

NG_Compute::Queue::Queue()
{
    head   = 0;
    tail   = 0;
    //runner = 0;
    tasks.resize( queue_length );
}

#if defined( VERBOSE_COMPUTE )
QString resolveName( Smoke* r_link )
{
    QString name;
    Reflection* r = dynamic_cast< Reflection* >( r_link );
    if ( r )
        name = QString( "%1( %2 )" ).arg( r->GetName().c_str() ).arg( r->src->getName().c_str() );
    else
    {
        Mirror* m = dynamic_cast< Mirror* >( r_link );
        if ( m )
            name = QString( "%1( %2 )" ).arg( m->GetName().c_str() ).arg( m->src->getName().c_str() );
        else
            name = "Smoke(?)";
    }
    return name;
}
#else
inline QString resolveName( Smoke* r_link ) { return QString(); }
#endif

bool NG_Compute::Queue::PushCompute( Smoke* r_link, const int time, bool isPost, bool allowDuplicates, const int threadIdx )
{
    std::lock_guard< std::recursive_mutex > Lock( lock );

    if ( getNext( head ) == tail )
        return false;

    if ( allowDuplicates == false )
    {
        size_t idx = tail;
        while( idx!= head )
        {
            if ( tasks[ idx ].link  == r_link &&
                 tasks[ idx ].post == isPost &&
                 tasks[ idx ].time == time )
            {
                ComputeLog( QString( "(%1)  <-- %2 into the queue, but it's already there." ).arg( threadIdx ).arg( resolveName( r_link ) ) );
                return true;
            }

            idx = getNext( idx );
        }
    }
    tasks[ head ].link = r_link;
    tasks[ head ].post = isPost;
    tasks[ head ].time = time;
    head = ( head == queue_length - 1 ) ? 0 : head + 1;
    ComputeLog( QString( "(%1)  <-- %2 %3 into the queue." ).arg( threadIdx ).arg( resolveName( r_link ) ).arg(pre_post[ int( isPost ) ] ) );

    return true;
}

NG_Compute::QueueItem* NG_Compute::Queue::PopCompute( const int threadIdx )
{
    std::lock_guard< std::recursive_mutex > Lock( lock );

    NG_Compute::QueueItem* R = nullptr;
    if ( tail != head )
    {
        R = &tasks[ tail ];
        tail = getNext( tail );
    }

    if ( R )
        ComputeLog( QString( "(%1)  --> %2 %3 from the queue." ).arg( threadIdx ).arg( resolveName( R->link ) ).arg(pre_post[ int( R->post ) ] ) );

    return R;
}

void NG_Compute::Queue::FlushQueue()
{
    std::lock_guard< std::recursive_mutex > Lock( lock );
    tail = head;
}

void NG_Compute::Queue::Eliminate( Smoke* smoke )
{
    std::lock_guard< std::recursive_mutex > Lock( lock );

    for( size_t i = tail; i != head; i = getNext( i ) )
        if ( tasks[ i ].link == smoke )
            tasks[ i ].link = nullptr;
}

bool NG_Compute::PushCompute( Smoke* r_node, const int time )
{
    return NG_Compute::queue.PushCompute( r_node, time, false, false, -1 );
}

NG_Compute::QueueItem* NG_Compute::PopCompute()
{
    return NG_Compute::queue.PopCompute( -1 );
}

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////

void NG_Compute::allocateWorkersPool( size_t p_count )
{
    while ( pool.size() > p_count )
    {
        // need less workers
        Worker* worker = *(--pool.end());
        delete worker;
        pool.erase( --pool.end() );
    }

    int idx = pool.size();

    while ( pool.size() < p_count )
    {
        // Need more workers
        Worker* worker = new Worker( &queue, idx++ );
        pool.push_back( worker );
    }

}

void NG_Compute::stopWorking()
{
    for( auto i = pool.begin(); i != pool.end(); ++i )
        (*i)->markForIdle();

    auto i = pool.cbegin();
    while( i != pool.cend() )
    {
        if ( (*i)->ifIdle() )
            ++i;
    }
}

void NG_Compute::resumeWorking()
{
    auto i = pool.begin();
    for( ; i != pool.end(); ++i )
        (*i)->markForRun();

}

void NG_Compute::FlushQueue()
{
    NG_Compute::queue.FlushQueue();
}

void NG_Compute::Eliminate( Smoke* smoke )
{
    stopWorking();
    NG_Compute::queue.Eliminate( smoke );
    resumeWorking();
}

NG_Compute::Worker::Worker( Queue* p_queue, const int p_idx )
{
    todo      = State::Run;
    queue     = p_queue;
    id        = new std::thread( Worker::run, this );
    idx       = p_idx;
}

NG_Compute::Worker::~Worker()
{
    todo = State::Exit;
    if ( id )
    {
        id->join();
        delete id;
    }
}

template < class Unit >
void NG_Compute::Worker::process( NG_Compute::Worker* self, Unit* ref, const QueueItem* task )
{
    const NG_Node* node   = ( std::is_same< Unit, Reflection >::value )
            ? reinterpret_cast< const NG_Node* >(ref->src)
            : nullptr;

    NG_Shadow::CacheSlice* cache = ref->getCacheSlice( task->time );
    std::lock_guard< std::recursive_mutex > Lock( cache->lock );

    const NG_ExecMode execMode = ( std::is_same< Unit, Reflection >::value ) ? node->m_execMode : NG_ExecMode::Normal;
    const bool        dirty    = cache->isDirty();
    const bool        force    = task->force;

    const NG_Shadow::State state    = cache->state;

    if ( std::is_same< Unit, Reflection >::value )
        ComputeLog( QString( "(%1)   .. Processing reflection %2( %3 ) %4" ).arg( self->idx ).arg( ref->GetName().c_str() ).arg( ref->src->getName().c_str() ).arg(pre_post[ int( task->post ) ] ) );
    else
        ComputeLog( QString( "(%1)   .. Processing mirror %2( %3 ) %4" ).arg( self->idx  ).arg( ref->GetName().c_str() ).arg( ref->src->getName().c_str() ).arg(pre_post[ int( task->post ) ] ) );

    if ( task->post == false )
    {
        // Pre-compute.
        const ExecModeSet  emSet  = NG_Compute::shouldCompute[ int( execMode ) ];
        const ExecModeCase emCase = dirty ? emSet.ifDirty : emSet.ifClean;

        if ( force ? emCase.ifForced : emCase.ifNormal )
        {
            // Compute
            cache->state = NG_Shadow::State::In_Pre;

            if ( ref->preCompute( cache, self->todo ) )
            {
                const std::vector< Smoke* > deps = ref->getDependency();
                if ( deps.size() )
                    for( auto it = deps.cbegin(); it != deps.cend(); ++it )
                    {
                        NG_Compute::queue.PushCompute( *it, task->time, false, false, self->idx );
                        cache->incDepCount();
                    }

                else
                    NG_Compute::queue.PushCompute( static_cast<Smoke*>( ref ), task->time, true, true, self->idx );
            }
            else
                ComputeLog( QString("(%1)  !!!!! pre compute returned false. Error. Have to abort execution.").arg( self->idx  ) );
                //NG_Compute::queue.PushCompute( static_cast<Smoke*>( ref ), task->time, false, false, self->idx );
        }
        else
            cache->state = NG_Shadow::State::Ready;

    }
    else
    {
        // Post-compute
        if ( cache->state != NG_Shadow::State::In_Post )
        {
            int depBefore = cache->depCount.fetch_sub( 1 );
            if ( depBefore > 1 )
            {
                // Not all dependencies have been resolved.
                ComputeLog( QString("(%1)  dependency is not ready yet").arg( self->idx  ) );
                return;
            }

            // this one was the last one.
#if defined( DEPENDENCY_READY_SANITY_CHECK )
            const std::vector< Smoke* > deps = ref->getDependency();
            if ( deps.size() )
                for( auto it = deps.cbegin(); it != deps.cend(); ++it )
                {
                    NG_Shadow::CacheSlice* dep_cache = (*it)->getCacheSlice( task->time );
                    Q_ASSERT( dep_cache->state == NG_Shadow::State::Ready );
                }
#endif
        }

        cache->state = NG_Shadow::State::In_Post;

        if ( ref->postCompute( cache, self->todo ) )
        {
            cache->setClean();
            cache->state = NG_Shadow::State::Ready;

            const std::vector< Smoke* > deps = ref->getTargets();
            for( auto it = deps.cbegin(); it != deps.cend(); ++it )
                NG_Compute::queue.PushCompute( *it, task->time, true, true, self->idx );

        }
        else
            ComputeLog( QString("(%1)  !!!!! post compute returned false. Error. Have to abort execution.").arg( self->idx  ) );
            //NG_Compute::queue.PushCompute( static_cast<Smoke*>( ref ), task->time, true, false, self->idx );
    }
    ComputeLog( QString("(%1)  ----").arg( self->idx  ) );

}

void NG_Compute::Worker::run( void* param )
{
    NG_Compute::Worker* self = static_cast< NG_Compute::Worker* >( param );

    while( self->todo != State::Exit )
    {
        if ( self->todo == State::Run )
        {
            QueueItem* task = NG_Compute::queue.PopCompute( self->idx );

            if ( task && task->link )
            {
                Reflection* r = dynamic_cast< Reflection* >( task->link );
                if ( r )
                    process< Reflection >( self, r, task );
                else
                {
                    Mirror* m = dynamic_cast< Mirror* >( task->link );
                    if ( m )
                        process< Mirror >( self, m, task );
                    else
                        Global::err( "Dropping the task which is linked to nether reflection nor Mirror.");
                }
            }
            else
                std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
        }
        else
        {
            State ifState = State::ToIdle;
            self->todo.compare_exchange_strong( ifState, State::Idle );
            ifState = State::ToRun;
            self->todo.compare_exchange_strong( ifState, State::Run );
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
        }
    }
}

NG_Compute::Queue                  NG_Compute::queue;
std::vector< NG_Compute::Worker* > NG_Compute::pool;

NG_Compute::ExecModeSet            NG_Compute::shouldCompute[ int( NG_ExecMode::Always ) + 1 ] =
        {
            {
                //Blocking,
                { false, false },
                { false, false },
            },
            {
                //Lazy,
                { false, false },
                { false, true  },
            },
            {
                //Normal,
                { false, false },
                { true,  true  },
            },
            {
                //Aggressive,
                { false, true  },
                { true,  true  },
            },
            {
                //Always
                { true,  true  },
                { true,  true  },
            },
        };
