/*
 * AnyType.h
 *
 *  Created on: Sep 9, 2021
 *      Author: vlad
 */

#ifndef SRC_CORE_ANYTYPE_H_
#define SRC_CORE_ANYTYPE_H_

#include "UID.h"
#include <array>
#include <vector>
#include <type_traits>
#include <QDebug>

namespace YAML { class Node; class Emitter; }

class AnyType
{

public:
    enum class Types
    {
        Empty,
        Int,
        Count,
        String,
        Filename,
        Number,
        Vec2,
        Vec3,
        Vec4,
        IVec2,
        IVec3,
        IVec4,
        UVec2,
        UVec3,
        UVec4,
        Vector,
        VectorI,
        VectorU,
        StrList,
        Image,
        TensorCPU,
        MAX
    };

    struct Lookup
    {
        strID          name;
        //QVariant::Type varType;
    };

    struct ImageInit
    {
        bool        load_from_file;
        std::string filename;
        int         width;
        int         height;
        int         channel;
    };

    const static std::map< strID, Types > TypeNames;
    const static std::array< strID, int( AnyType::Types::MAX )  > TypeNamesRev;
    const static char   arrayIdxSymbol[4];

    AnyType();
    ~AnyType();
    AnyType( const AnyType& other ) = delete;
    AnyType& operator=( const AnyType& other ) = delete;

    AnyType( AnyType&& other );

    void clone( AnyType* copy ) const;

    Types getType() const { return type; }
    bool fromYAML( const Types p_type, const YAML::Node yaml );
    void toYAML( YAML::Emitter& emitter ) const;

    operator std::string() const;

    static inline bool isInPlace( const Types type ) { return ( type == Types::Int )    || ( type == Types::Count )   || ( type == Types::Number )  || ( type == Types::Empty  ); }
    static inline bool isPtr(     const Types type ) { return ( type == Types::String ) || ( type == Types::Filename )|| ( type == Types::Image )   || ( type == Types::TensorCPU ); }
    static inline bool isArray(   const Types type ) { return ( isNArray( type ) )      || ( isIArray( type ) )       || ( isUArray( type ) );           }
    static inline bool isNArray(  const Types type ) { return ( type == Types::Vec2 )   || ( type == Types::Vec3 )    || ( type == Types::Vec4 );  }
    static inline bool isIArray(  const Types type ) { return ( type == Types::IVec2 )  || ( type == Types::IVec3 )   || ( type == Types::IVec4 ); }
    static inline bool isUArray(  const Types type ) { return ( type == Types::UVec2 )  || ( type == Types::UVec3 )   || ( type == Types::UVec4 ); }
    static inline bool isVector(  const Types type ) { return ( type == Types::Vector ) || ( type == Types::VectorI ) || ( type == Types::VectorU ) || ( type == Types::StrList ); }

    static inline int  getSize(   const Types type )
    {
        if ( isVector( type ) )
            return 0;
        if ( isArray(  type ) )
            return ( int ( type ) - int( Types::Vec2 ) ) % 3 + 2;

        return 1;
    }

    static inline strID getTypeAsStr( const Types type ) { return TypeNamesRev[ int( type ) ]; }
    static inline Types resolveType(  const strID type ) { auto key = TypeNames.find( type ); if ( key != TypeNames.end() ) return key->second; else return Types::Empty; }

    inline bool isInPlace() const { return isInPlace( type ); }
    inline bool isPtr()     const { return isPtr(     type ); }
    inline bool isArray()   const { return isArray(   type ); }
    inline bool isNArray()  const { return isNArray(  type ); }
    inline bool isIArray()  const { return isIArray(  type ); }
    inline bool isUArray()  const { return isUArray(  type ); }
    inline bool isVector()  const { return isVector(  type ); }
    inline int  getSize()   const { return getSize(   type ); }

    inline int32_t  unsafeValueInt()    const { return inPlace< int32_t >()[ 0 ]; }
    inline uint32_t unsafeValueUInt()   const { return inPlace< uint32_t >()[ 0 ]; }
    inline float    unsafeValueNumber() const { return inPlace< float >()[ 0 ]; }

    inline int32_t  unsafeArrayInt(    const uint p_idx ) const { return ptr< int32_t >()[ p_idx ]; }
    inline uint32_t unsafeArrayUInt(   const uint p_idx ) const { return ptr< uint32_t >()[ p_idx ]; }
    inline float    unsafeArrayNumber( const uint p_idx ) const { return ptr< float >()[ p_idx ]; }

    inline int32_t&  unsafeValueInt()    { return inPlace< int32_t >()[ 0 ]; }
    inline uint32_t& unsafeValueUInt()   { return inPlace< uint32_t >()[ 0 ]; }
    inline float&    unsafeValueNumber() { return inPlace< float >()[ 0 ]; }

    inline int32_t&  unsafeArrayInt(    const uint p_idx ) { return ptr< int32_t >()[ p_idx ]; }
    inline uint32_t& unsafeArrayUInt(   const uint p_idx ) { return ptr< uint32_t >()[ p_idx ]; }
    inline float&    unsafeArrayNumber( const uint p_idx ) { return ptr< float >()[ p_idx ]; }

    inline int32_t  unsafe_iX() const { return ptr< int32_t >()[ 0 ]; }
    inline int32_t  unsafe_iY() const { return ptr< int32_t >()[ 1 ]; }
    inline int32_t  unsafe_iZ() const { return ptr< int32_t >()[ 2 ]; }
    inline int32_t  unsafe_iW() const { return ptr< int32_t >()[ 3 ]; }
    inline uint32_t unsafe_uX() const { return ptr< uint32_t >()[ 0 ]; }
    inline uint32_t unsafe_uY() const { return ptr< uint32_t >()[ 1 ]; }
    inline uint32_t unsafe_uZ() const { return ptr< uint32_t >()[ 2 ]; }
    inline uint32_t unsafe_uW() const { return ptr< uint32_t >()[ 3 ]; }
    inline float    unsafe_nX() const { return ptr< float >()[ 0 ]; }
    inline float    unsafe_nY() const { return ptr< float >()[ 1 ]; }
    inline float    unsafe_nZ() const { return ptr< float >()[ 2 ]; }
    inline float    unsafe_nW() const { return ptr< float >()[ 3 ]; }

    inline int32_t&  unsafe_iX() { return ptr< int32_t >()[ 0 ]; }
    inline int32_t&  unsafe_iY() { return ptr< int32_t >()[ 1 ]; }
    inline int32_t&  unsafe_iZ() { return ptr< int32_t >()[ 2 ]; }
    inline int32_t&  unsafe_iW() { return ptr< int32_t >()[ 3 ]; }
    inline uint32_t& unsafe_uX() { return ptr< uint32_t >()[ 0 ]; }
    inline uint32_t& unsafe_uY() { return ptr< uint32_t >()[ 1 ]; }
    inline uint32_t& unsafe_uZ() { return ptr< uint32_t >()[ 2 ]; }
    inline uint32_t& unsafe_uW() { return ptr< uint32_t >()[ 3 ]; }
    inline float&    unsafe_nX() { return ptr< float >()[ 0 ]; }
    inline float&    unsafe_nY() { return ptr< float >()[ 1 ]; }
    inline float&    unsafe_nZ() { return ptr< float >()[ 2 ]; }
    inline float&    unsafe_nW() { return ptr< float >()[ 3 ]; }

    inline const std::string&  unsafeString() const { return ptr< std::string >()[ 0 ]; }
    inline std::string&        unsafeString()       { return ptr< std::string >()[ 0 ]; }

    inline const QImage*  unsafeImage() const { return ptr< QImage >(); }
    inline QImage*        unsafeImage()       { return ptr< QImage >(); }

    void initAs( const Types p_type, const void* initData = nullptr );
    void destroy();

protected:
    template < typename T >                  bool readYAML( const YAML::Node yaml, T& target );
    template < typename T >                  bool readScalarYaml( const YAML::Node yaml, T& target );
    template < typename T >                  bool readSeqYaml( const YAML::Node yaml, std::vector< T >& target );

    template < class dataType, Types type >     bool load( dataType p_data );
    template< typename Unit, typename UnitSrc > bool loadFixedArray( std::vector < UnitSrc > p_data, const size_t size );
    template < class dstType, class srcType >   bool loadDynArray(   std::vector < srcType > p_data, Types p_type );

    template < typename T > inline           T*   inPlace()                 { return reinterpret_cast< T* >( &data ); }
    template < typename T > inline     const T*   inPlace() const           { return reinterpret_cast<const T* >( &data ); }
    template < typename T > inline           void inPlace( const T& input ) { reinterpret_cast< T* >( &data )[0] = input; }

    template < typename T > inline           T*   ptr() const                   { return reinterpret_cast< T* >( data ); }

public:
    template < typename T > inline     const T&   vectorGet( size_t idx ) const { return (*reinterpret_cast< std::vector< T >* >( data ))[ idx ]; }
    template < typename T > inline           void vectorSet( size_t idx, const T& item ) { (*reinterpret_cast< std::vector< T >* >( data ))[ idx ] = item; }

    template< typename Unit >         std::string dumpArray() const;
    template< typename Unit >         std::string dumpVector() const;

    template< typename Unit > void                dumpArray(  YAML::Emitter& emitter ) const;
    template< typename Unit > void                dumpVector( YAML::Emitter& emitter ) const;

private:
    Types type;
    void* data;
#ifdef QT_DEBUG
    int   allocate_count;
#endif

    friend class TestAnyType;
};


QDebug& operator<< ( QDebug& dbg, const AnyType::Types type );
QDebug& operator<< ( QDebug& dbg, const AnyType type );

#endif /* SRC_CORE_ANYTYPE_H_ */
