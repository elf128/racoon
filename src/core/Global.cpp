/*
 * Global.cpp
 *
 *  Created on: Feb 25, 2021
 *      Author: Vlad A.
 */

#include "Global.h"

#include <fstream>
#include <algorithm>
#include <yaml-cpp/yaml.h>
#include <QDebug>
#include <QColor>

#pragma push_macro("slots")
#undef slots
#include <Python.h>
#pragma pop_macro("slots")

#if !defined( CONFIG_FILE_NAME )
#define CONFIG_FILE_NAME "../Racoon/res/Nodes.yaml"
#endif

Global::uiColor::operator QColor() const
{
    return QColor( R, G, B );
}

const Global::uiColor Global::uiColor::operator *( const uiColor& other ) const
{
    return Global::uiColor( ( uint16_t( R ) * other.R ) >> 8,
                          ( uint16_t( G ) * other.G ) >> 8,
                          ( uint16_t( B ) * other.B ) >> 8 );

}

const Global::uiColor Global::uiColor::operator *( const float other ) const
{
    return Global::uiColor( uint8_t( std::max( std::min( float( R ) * other, 255.0f ), 0.0f ) ),
                          uint8_t( std::max( std::min( float( G ) * other, 255.0f ), 0.0f ) ),
                          uint8_t( std::max( std::min( float( B ) * other, 255.0f ), 0.0f ) ) );

}

const Global::uiColor Global::uiColor::operator +( const uiColor& other ) const
{
    uint8_t r = std::min( uint16_t( R ) + uint16_t( other.R ),  255 );
    uint8_t g = std::min( uint16_t( G ) + uint16_t( other.G ),  255 );
    uint8_t b = std::min( uint16_t( B ) + uint16_t( other.B ),  255 );

    return Global::uiColor( r, g, b );
}

void Global::__init__( const bool loadPresets )
{
    if ( single )
        return;

    single = new Global;

    if ( loadPresets )
        single->ParsePresets( CONFIG_FILE_NAME );
}

bool Global::ParsePresets( const std::string filename )
{
    qDebug().noquote() << QString( "Load node presets from %1" ).arg( filename.c_str() );

    std::ifstream Nodes( filename );

    try
    {
        YAML::Node root = YAML::Load( Nodes );

        verbQ( QString( "Found %1 systems" ).arg( root.size() ) );

        for( YAML::const_iterator it = root.begin(); it != root.end(); ++it )
        {
            if ( ! single->ParseSystem( it->first.as<std::string>(), it->second ) )
            {
                errQ( QString("Error loading preset for %1").arg( it->first.as< std::string >().c_str() ) );
                dumpYAML( it->second );
                return false;
            }
        }
        verb( "Done parsing systems" );
        return true;
    }
    catch( const std::exception& e )
    {
        err( "Yaml exception while loading main preset file. " );
        errQ( e.what() );
        return false;
    }
}

bool Global::ParseSystem( const strID& sysName, const YAML::Node& yaml )
{
    try
    {
        SysDef system;

        system.name = sysName;
        qDebug().noquote() << QString( "  Found %1 potential groups presets" ).arg( yaml.size() );

        for( YAML::const_iterator it = yaml.begin(); it != yaml.end(); ++it )
        {
            std::string Name = it->first.as<std::string>();

            if ( Name == "_properties_" )
            {
                if ( !single->ParseSysDef( sysName, it->second, system ) )
                {
                    qDebug("  Error loading system's properties");
                    dumpYAML( it->second );
                }
            }
            else
                if ( ! single->ParseGroup( Name, it->second, system.nodes ) )
                {
                    qDebug("  Error loading system's group");
                    dumpYAML( it->second );
                }
        }
        verb( "  Done parsing system" );

        everything.push_back( system );
        return true;
    }
    catch( const std::exception& e )
    {
        err( "  Yaml exception while parsing Node map. " );
        errQ( e.what() );
        return false;
    }
}

bool Global::ParseGroup( const strID& grpName,  const YAML::Node& yaml, NodeList& targets )
{
    try
    {
        const YAML::Node nodes = yaml["nodes"];

        if ( nodes.size() == 0 )
        {
            qDebug( "    Group's section is not parsed correctly" );
            dumpYAML( nodes );
            return false;
        }
        else
        {
            qDebug().noquote() << QString( "    Found %1 node presets" ).arg( nodes.size() );

            for( YAML::const_iterator it = nodes.begin(); it != nodes.end(); ++it )
            {
                if ( ! single->ParseNodeDef( it->first.as<std::string>(), grpName, it->second, targets ) )
                {
                    qDebug("    Error loading node type preset");
                    dumpYAML( it->second );
                }
            }
            verb( "    Done parsing group" );
        }

        return true;
    }
    catch( const std::exception& e )
    {
        errQ( QString( "    Yaml exception while parsing Group. %1" ).arg( grpName.c_str() ) );
        errQ( e.what() );
        return false;
    }
}
bool Global::ParseLinks( const YAML::Node& yaml, StrList& list )
{
    if ( yaml.IsSequence() )
    {
        for( YAML::const_iterator it = yaml.begin(); it != yaml.end(); ++it )
        {
            if ( it->IsScalar() )
            {
                strID type = strID( it->Scalar() );

                auto key = AnyType::TypeNames.find( type );

                if ( key == AnyType::TypeNames.end() )
                {
                    errQ( QString( "      slot's type is unknown '%1'").arg( type.c_str() ) );
                    return false;
                }
                else
                    list.push_back( type );
            }
            else
            {
                errQ( QString( "      slot %1 is not scalar").arg( it->as< std::string>().c_str() ) );
                return false;
            }
        }
        return true;
    }
    else
    {
        errQ( QString( "      slots are not defined as sequence '%1'").arg( yaml.as< std::string>().c_str() ) );
        return false;
    }

}

bool Global::ParseNodeDef( const strID& nodeName, const strID& grpName, const YAML::Node& yaml, NodeList& targets )
{
    try
    {
        NodeDef def;

        def.name = nodeName;

        verbQ( QString( "      Parsing %1").arg( nodeName.c_str() ) );

        StrList Res;
        if ( yaml["inputs"] && ParseLinks( yaml["inputs"], Res ) )
            def.inputTypes = Res;
        else
        {
            logQ( QString("      Error parsing inputs for %1::%2. Assuming no inputs").arg( grpName.c_str() ).arg( nodeName.c_str() ) );
            def.inputTypes = StrList();
        }

        Res.clear();
        if ( yaml["outputs"] && ParseLinks( yaml["outputs"], Res ) )
            def.outputTypes = Res;
        else
        {
            logQ( QString("      Error parsing outputs for %1::%2. Assuming no outputs").arg( grpName.c_str() ).arg( nodeName.c_str() ) );
            def.outputTypes = StrList();
        }

        def.group       = grpName;

        YAML::Node color = yaml["color"];

        if ( color && color.IsSequence() && color.size() == 3 )
        {
            def.color.R = color[0].as<int>();
            def.color.G = color[1].as<int>();
            def.color.B = color[2].as<int>();
        }
        else
        {
            errQ( QString( "      Something wrong with definition of color in %1. Will use grey." ).arg( nodeName .c_str() ) );
            dumpYAML( color );

            def.color.R = 128;
            def.color.G = 128;
            def.color.B = 128;
        }

        YAML::Node ref = yaml["reflection"];
        if ( ref && ref.IsScalar() )
            def.reflection = ref.as<std::string>();
        else
            def.reflection = strID("Ref_None");

        YAML::Node params = yaml["params"];

        for( YAML::const_iterator it = params.begin(); it != params.end(); ++it )
        {
            const std::string paramName     = it->first. as<std::string>();
            const std::string paramTypeName = it->second.as<std::string>();

            auto key = AnyType::TypeNames.find( paramTypeName );

            if ( key == AnyType::TypeNames.end() )
            {
                errQ( QString( "      Error loading params for %1").arg( nodeName.c_str() ) );
                errQ( QString( "      %1 has unknown type '%2'").arg( paramName.c_str() ).arg( paramTypeName.c_str() ) );
            }
            else
                def.params.push_back( typePair( paramName, key->second ) );
        }

        targets.push_back( def );

        return true;
    }
    catch( const std::exception& e )
    {
        err( "      Yaml exception while parsing Node definition. " );
        errQ( e.what() );
        return false;
    }
}

bool Global::ParseSysDef( const strID& sysName, const YAML::Node& yaml, SysDef& sysDef )
{
    try
    {
        YAML::Node ref = yaml["mirror"];
        if ( ref && ref.IsScalar() )
            sysDef.mirror = ref.as<std::string>();
        else
            sysDef.mirror = strID("M_Basic");

        YAML::Node params = yaml["params"];

        for( YAML::const_iterator it = params.begin(); it != params.end(); ++it )
        {
            const std::string paramName     = it->first. as<std::string>();
            const std::string paramTypeName = it->second.as<std::string>();

            auto key = AnyType::TypeNames.find( paramTypeName );

            if ( key == AnyType::TypeNames.end() )
            {
                errQ( QString( "    Error loading params for %1").arg( sysName.c_str() ) );
                errQ( QString( "    %1 has unknown type '%2'").arg( paramName.c_str() ).arg( paramTypeName.c_str() ) );
            }
            else
                sysDef.params.push_back( typePair( paramName, key->second ) );
        }

        return true;
    }
    catch( const std::exception& e )
    {
        err( "    Yaml exception while parsing System properties. " );
        errQ( e.what() );
        return false;
    }
}

const Global::SysDef* Global::findSysDef( const strID& sys )
{
    auto found = [sys]( SysDef def ){ return ( def.name == sys ); };
    auto sysIt = std::find_if( single->everything.cbegin(), single->everything.cend(), found );

    if ( sysIt == single->everything.end() )
        return nullptr;

    return &(*sysIt);
}

const Global::NodeDef* Global::findLayerDef( const strID& sys, const strID& name )
{
    auto foundSys   = [sys](  SysDef  def ){ return ( def.name == sys  ); };
    auto foundLayer = [name]( NodeDef def ){ return ( def.name == name ); };

    auto sysIt = std::find_if( single->everything.cbegin(), single->everything.cend(), foundSys );

    if ( sysIt == single->everything.end() )
        return nullptr;

    auto nodeIt = std::find_if( sysIt->nodes.cbegin(), sysIt->nodes.cend(), foundLayer );

    if ( nodeIt == sysIt->nodes.end() )
        return nullptr;
    else
        return &( *nodeIt );
}

const Global::StrList Global::listSystems()
{
    StrList R;

    for( auto i = single->everything.cbegin(); i != single->everything.cend(); ++i )
        R.push_back( i->name );

    return R;
}

const Global::StrList Global::listGroups( const strID& sys )
{
    const SysDef* Sys = findSysDef( sys );

    if ( Sys == nullptr )
        return StrList();

    const NodeList& system = Sys->nodes;
    StrList R;

    for( auto i = system.cbegin(); i != system.cend(); ++i )
        if ( std::find( R.cbegin(), R.cend(), i->group ) == R.cend() )
            R.push_back( i->group );

    return R;
}

const Global::StrList Global::listLayers( const strID& sys, const strID grp )
{
    const SysDef* Sys = findSysDef( sys );

    if ( Sys == nullptr )
        return StrList();

    const NodeList& system = Sys->nodes;

    StrList R;

    for( auto i = system.cbegin(); i != system.cend(); ++i )
        if ( i->group == grp )
            R.push_back( i->name );

    return R;
}

Global* Global::single = nullptr;

void Global::verb( const std::string& to_log )
{
    if ( single->m_verb )
        qDebug().noquote().nospace() << to_log.c_str();
}

void Global::log( const std::string& to_log )
{
    if ( single->m_log )
        qDebug().noquote().nospace() << to_log.c_str();
}

void Global::err( const std::string& to_log )
{
    if ( single->m_err )
        qDebug().noquote().nospace() << to_log.c_str();
}

void Global::verb( const YAML::Node& to_log )
{
    if ( single->m_verb )
        dumpYAML( to_log );
}

void Global::log( const YAML::Node& to_log )
{
    if ( single->m_log )
        dumpYAML( to_log );
}

void Global::err( const YAML::Node& to_log )
{
    if ( single->m_err )
        dumpYAML( to_log );
}

void Global::verbQ( const QString& to_log )
{
    if ( single->m_verb )
        qDebug().noquote().nospace() << to_log;
}

void Global::logQ( const QString& to_log )
{
    if ( single->m_log )
        qDebug().noquote().nospace() << to_log;
}

void Global::errQ( const QString& to_log )
{
    if ( single->m_err )
        qDebug().noquote().nospace() << to_log;
}

