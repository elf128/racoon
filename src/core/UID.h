/*
 * CRC.h
 *
 *  Created on: Feb 24, 2021
 *      Author: vlad
 */

#ifndef SRC_CORE_UID_H_
#define SRC_CORE_UID_H_

#include <stdint.h>
#include <functional>
#include <string>
#include <map>

class UID
{
    uint64_t uid;
public:
    template < typename T >
    UID( T data ) : uid(0) { *this <<= data; }
    ~UID() {}

    template < typename T >
    void operator <<= ( T data )
    {
        const uint64_t seed = 0x17c5fa6d8e9b3042;
        uint64_t h = std::hash< T > {}( data );

        uid ^= h + seed + ( uid >> 5 ) + ( uid << 3 );
    }

    bool operator ==( const UID other ) { return ( uid == other.uid ); }
    bool operator !=( const UID other ) { return ( uid != other.uid ); }

    friend class strID;
};

class strID
{
    static std::map< uint64_t, std::string > DB;

    uint64_t         m_uid;

public:
    strID( const strID&       str ) : m_uid( str.m_uid ) {}
    strID( const std::string& str );
    strID() : m_uid( 0 ) {}

    strID& operator=( const strID& other ) { m_uid = other.m_uid; return *this; }

    const UID getUID() const    { return m_uid; }
    bool      isNull() const    { return m_uid == 0; }
    operator std::string() const { return DB[ m_uid ]; }

    const char* c_str() const { return DB[ m_uid ].c_str(); }

    bool operator ==( const strID& other ) const  { return ( m_uid == other.m_uid ); }
    bool operator !=( const strID& other ) const  { return ( m_uid != other.m_uid ); }
    //bool operator ==(       strID& other ) const  { return ( m_uid == other.m_uid ); }
    //bool operator !=(       strID& other ) const  { return ( m_uid != other.m_uid ); }

#if defined( CORRECT_STR )
    bool operator < ( const strID& other )        { return ( DB[ m_uid ] < DB[ other.m_uid ] ); }
    bool operator > ( const strID& other )        { return ( DB[ m_uid ] > DB[ other.m_uid ] ); }
    bool operator <= ( const strID& other )       { return ( DB[ m_uid ] <= DB[ other.m_uid ] ); }
    bool operator >= ( const strID& other )       { return ( DB[ m_uid ] >= DB[ other.m_uid ] ); }
#else
    bool operator < ( strID  other )              { return ( m_uid < other.m_uid ); }
    bool operator < ( strID& other )              { return ( m_uid < other.m_uid ); }
    bool operator < ( const strID& other ) const  { return ( m_uid < other.m_uid ); }
    bool operator > ( const strID& other ) const  { return ( m_uid > other.m_uid ); }
    bool operator <= ( const strID& other ) const { return ( m_uid <= other.m_uid ); }
    bool operator >= ( const strID& other ) const { return ( m_uid >= other.m_uid ); }
#endif

    //bool operator ==( const std::string& other )  { return ( DB[ m_uid ] == other ); }
    //bool operator !=( const std::string& other )  { return ( DB[ m_uid ] != other ); }
};

#endif /* SRC_CORE_UID_H_ */
