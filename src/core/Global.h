/*
 * Global.h
 *
 *  Created on: Feb 25, 2021
 *      Author: vlad
 */

#ifndef SRC_CORE_GLOBAL_H_
#define SRC_CORE_GLOBAL_H_

#include <QStringList>
#include <stdint.h>
#include <vector>
#include <string>

#include "UID.h"
#include "AnyType.h"

namespace YAML { class Node; }

class QColor;

class Global
{
public:

    struct uiColor
    {
        uint8_t R;
        uint8_t G;
        uint8_t B;

        uiColor() : R(0),G(0),B(0) {}
        uiColor( const uint8_t r, const uint8_t g, const uint8_t b ) : R( r ), G( g ), B( b ) {}
        uiColor( const uint32_t rgb )
            : R( ( rgb & 0xFF0000 ) >> 16 )
            , G( ( rgb & 0x00FF00 ) >>  8 )
            , B( ( rgb & 0x0000FF ) >>  0 ) {}

        operator QColor() const;
        const uiColor operator*( const uiColor& other ) const;
        const uiColor operator*( const float other ) const;
        const uiColor operator+( const uiColor& other ) const;
    };

    struct typePair
    {
        strID          name;
        AnyType::Types type;
        typePair( const strID p_name, const AnyType::Types p_type ) : name( p_name ), type( p_type ) {}
    };

    using typeMap = std::vector< typePair >;
    using StrList = std::vector< strID >;


    struct NodeDef
    {
        strID    name;
        strID    group;
        StrList  inputTypes;
        StrList  outputTypes;
        uiColor  color;
        strID    reflection;
        typeMap  params;
    };

    using NodeList  = std::vector< NodeDef >;

    struct SysDef
    {
        strID    name;
        NodeList nodes;
        strID    mirror;
        typeMap  params;
    };

    using HyperList = std::vector< SysDef >;

    struct Storage
    {
        strID   name;
        AnyType data;
        bool    exposed;
        Storage( strID p_name ) : name( p_name ), exposed( false ) {}
    };

private:
    static Global*  single;
    HyperList       everything;
    const bool m_log  = true;
    const bool m_verb = true;
    const bool m_err  = true;


    bool ParsePresets( const std::string filename );
    bool ParseSystem(  const strID& sysName,  const YAML::Node& yaml );
    bool ParseGroup(   const strID& grpName,  const YAML::Node& yaml, NodeList& targets );
    bool ParseNodeDef( const strID& nodeName, const strID& grpName, const YAML::Node& yaml, NodeList& targets );
    bool ParseLinks(   const YAML::Node& yaml, StrList& list );
    bool ParseSysDef(  const strID& sysName,  const YAML::Node& yaml, SysDef& sysDef  );
public:
    Global() {}
    virtual ~Global() {}

    static void   __init__( const bool loadPresets = true );
    static Global* __get__() { return single; }

    static const SysDef*  findSysDef(   const strID& sys );
    static const NodeDef* findLayerDef( const strID& sys, const strID& name );
    static const StrList  listSystems();
    static const StrList  listGroups( const strID& sys );
    static const StrList  listLayers( const strID& sys, const strID grp );

    static void           verb(  const std::string& to_log );
    static void           log(   const std::string& to_log );
    static void           err(   const std::string& to_log );

    static void           verb(  const YAML::Node& to_log );
    static void           log(   const YAML::Node& to_log );
    static void           err(   const YAML::Node& to_log );

    static void           verbQ( const QString& to_log );
    static void           logQ(  const QString& to_log );
    static void           errQ(  const QString& to_log );

};

#define dumpYAML( yaml ) { YAML::Emitter E; E << yaml; qDebug().noquote() << E.c_str(); }

#endif /* SRC_CORE_GLOBAL_H_ */
