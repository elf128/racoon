/*
 * AnyType.cpp
 *
 *  Created on: Sep 9, 2021
 *      Author: vlad
 */

#include "AnyType.h"
#include "Global.h"
#include <yaml-cpp/yaml.h>
#include <sstream>

#include <QImage>
#include "microNN/TensorOp.h"

using TensorCPUUnit = float;

AnyType::AnyType()
{
    type = Types::Empty;
    data = nullptr;
#ifdef QT_DEBUG
    allocate_count = 0;
#endif
}

AnyType::AnyType( AnyType&& other )
{
    type = other.type;
    data = other.data;
    allocate_count = other.allocate_count;
    other.type = Types::Empty;
    other.data = nullptr;
    other.allocate_count = 0;
}

AnyType::~AnyType()
{
    destroy();
#ifdef QT_DEBUG
    if ( allocate_count )
        Global::errQ( QString( "Allocate counter is not zero while killiny of AnyType. We're leaking memory" ));
#endif
}

#ifdef QT_DEBUG
#define incAlloc allocate_count++
#define decAlloc allocate_count--
#else
#define incAlloc
#define decAlloc
#endif

void AnyType::destroy()
{
    if ( data )
        switch ( type )
        {
            //Inplace types;
            case Types::Int:
            case Types::Count:
            case Types::Number:
                // data is not a pointer, ignore
                break;
            // String types
            case Types::String:
            case Types::Filename:
                delete ( ptr< std::string >() );
                decAlloc;
                break;

            // Finite array type
            case Types::Vec2:
            case Types::Vec3:
            case Types::Vec4:
                delete[] ( ptr< float >() );
                decAlloc;
                break;

            case Types::IVec2:
            case Types::IVec3:
            case Types::IVec4:
                delete[] ( ptr< int32_t >() );
                decAlloc;
                break;

            case Types::UVec2:
            case Types::UVec3:
            case Types::UVec4:
                delete[] ( ptr< uint32_t >() );
                decAlloc;
                break;

            case Types::Vector:
                ptr< std::vector<float> >()->clear();
                delete ( ptr< std::vector<float> >() );
                decAlloc;
                break;
            case Types::VectorI:
                ptr< std::vector<int32_t> >()->clear();
                delete ( ptr< std::vector<int32_t> >() );
                decAlloc;
                break;
            case Types::VectorU:
                ptr< std::vector<uint32_t> >()->clear();
                delete ( ptr< std::vector<uint32_t> >() );
                decAlloc;
                break;
            case Types::StrList:
                ptr< std::vector< std::string > >()->clear();
                delete ptr< std::vector< std::string > >();
                decAlloc;
                break;
            case Types::Image:
                delete ptr< QImage >();
                decAlloc;
                break;
            case Types::TensorCPU:
                delete ptr< Tensor_cpu< TensorCPUUnit > >();
                decAlloc;
                break;
            default:
                Global::err( "Delete of the non-empty pointer, while it should be empty. We're leaking memory!" );
                break;
        }

    type = Types::Empty;
    data = nullptr;

}

void AnyType::initAs( const Types p_type, const void* initData )
{
    if ( data )
        destroy();


    switch ( p_type )
    {
        // String types
        case Types::String:
        case Types::Filename:
            data = new std::string;
            incAlloc;
            break;

        // Finite array type
        case Types::Vec2:
            data = new float[2];
            incAlloc;
            break;

        case Types::Vec3:
            data = new float[3];
            incAlloc;
            break;

        case Types::Vec4:
            data = new float[4];
            incAlloc;
            break;

        case Types::IVec2:
            data = new int32_t[2];
            incAlloc;
            break;

        case Types::IVec3:
            data = new int32_t[3];
            incAlloc;
            break;

        case Types::IVec4:
            data = new int32_t[4];
            incAlloc;
            break;

        case Types::UVec2:
            data = new uint32_t[2];
            incAlloc;
            break;

        case Types::UVec3:
            data = new uint32_t[3];
            incAlloc;
            break;

        case Types::UVec4:
            data = new uint32_t[4];
            incAlloc;
            break;

        case Types::Vector:
            data = new std::vector< float >;
            incAlloc;
            break;
        case Types::VectorI:
            data = new std::vector< int32_t >;
            incAlloc;
            break;
        case Types::VectorU:
            data = new std::vector< uint32_t >;
            incAlloc;
            break;
        case Types::StrList:
            data = new std::vector< std::string >;
            incAlloc;
            break;
        case Types::Image:
            if ( initData )
            {
                const ImageInit* params = reinterpret_cast< const ImageInit* >( initData );
                if ( params->load_from_file )
                {
                    // load from file
                    data = new QImage( QString( params->filename.c_str() ) );
                    incAlloc;
                }
                else
                {
                    // Make Empty
                    QImage::Format format = ( params->channel == 1 ) ? QImage::Format_Grayscale8 :
                                           (( params->channel == 3 ) ? QImage::Format_RGB888 : QImage::Format_ARGB32 );

                    data = new QImage( params->width, params->height, format );
                    incAlloc;
                }
            }
            else
            {
                // Make null
                data = new QImage();
                incAlloc;
            }
            break;
        case Types::TensorCPU:
            if ( initData )
            {
                const Shape* params = reinterpret_cast< const Shape* >( initData );
                data = new Tensor_cpu< TensorCPUUnit >();
                ptr< Tensor_cpu< TensorCPUUnit > >()->setShape( *params );
                ptr< Tensor_cpu< TensorCPUUnit > >()->__init__();
            }
            else
                data = new Tensor_cpu< TensorCPUUnit >();

            incAlloc;
            break;
        default:
            // In-place types doesn't need any allocation and has to be skipped
            break;
    }

    type = p_type;

}

void AnyType::clone( AnyType* copy ) const
{
    if ( isInPlace() )
    {
        if ( !copy->isInPlace() )
            copy->destroy();

        copy->data = data;
        copy->type = type;
    }
    else if ( isArray() )
    {
        if ( copy->type != type )
            copy->initAs( type );

        if ( isNArray() )
        {
            int size = int( type ) - int( AnyType::Types::Vec2 ) + 2;

            for( int i = 0 ; i < size; ++i )
                copy->ptr< float >()[ i ] = ptr< float >()[ i ];
        }
        else if ( isIArray() )
        {
            int size = int( type ) - int( AnyType::Types::IVec2 ) + 2;

            for( int i = 0 ; i < size; ++i )
                copy->ptr< int32_t >()[ i ] = ptr< int32_t >()[ i ];
        }
        else if ( isUArray() )
        {
            int size = int( type ) - int( AnyType::Types::UVec2 ) + 2;

            for( int i = 0 ; i < size; ++i )
                copy->ptr< uint32_t >()[ i ] = ptr< uint32_t >()[ i ];
        }

    }
    else if ( isPtr() )
    {
        switch( type )
        {
            case Types::String:
            case Types::Filename:
                if ( copy->type != type )
                    copy->initAs( type );

                copy->ptr< std::string >()[0] = ptr< std::string >()[0];
                break;
            case Types::Image:
                if ( copy->data )
                    copy->destroy();

                copy->ptr< QImage >()[0] = ptr< QImage >()->copy( 0, 0, ptr< QImage >()->width(), ptr< QImage >()->height() );
                copy->type = Types::Image;
                break;
            case Types::TensorCPU:
                if ( data )
                {
                    const Tensor_cpu< TensorCPUUnit >::Accessor aThis = ptr< Tensor_cpu< TensorCPUUnit > >()->makeAccessor();
                    copy->initAs( type, aThis.shape );
                    const Tensor_cpu< TensorCPUUnit >::Accessor aCopy = copy->ptr< Tensor_cpu< TensorCPUUnit > >()->makeAccessor();
                    mempcpy( aCopy.data, aThis.data, aThis.shape->Flatened() );
                }
                else
                    copy->initAs( type );
                break;
            default:
                Global::err( "cloning of unknown type of pointer-to-the-object. Undefined behaviour ahead. Expect crash!!!" );
                break;
        }

    }
    else
    {
        // it must be an vector
        if ( copy->type != type )
            copy->initAs( type );

        switch( type )
        {
            case Types::Vector:
                copy->ptr< std::vector< float > >()[0] = ptr< std::vector< float > >()[0];
                break;
            case Types::VectorI:
                copy->ptr< std::vector< int32_t > >()[0] = ptr< std::vector< int32_t > >()[0];
                break;
            case Types::VectorU:
                copy->ptr< std::vector< uint32_t > >()[0] = ptr< std::vector< uint32_t > >()[0];
                break;
            case Types::StrList:
                copy->ptr< std::vector< std::string > >()[0] = ptr< std::vector< std::string > >()[0];
                break;
            default:
                Global::err( "cloning of unknown type of vector. Undefined behaviour ahead. Expect crash!!!" );
                break;
        }
    }


}

/////////////////////////////
/////////////////////////////
/////////////////////////////
/////////////////////////////
template < typename T > T strCast( const std::string& str );

template <> std::string strCast( const std::string& str ) { return str; }
template <> int32_t     strCast( const std::string& str ) { return std::stol( str ); }
template <> uint32_t    strCast( const std::string& str ) { long data = std::stoul( str ); return ( data > 0 ) ? uint32_t( data ) : 0;  }
template <> float       strCast( const std::string& str ) { return std::stof( str ); }

template < typename T >
bool AnyType::readScalarYaml( const YAML::Node yaml, T& target )
{
    if ( yaml.IsScalar() )
    {
        try
        {
            target = strCast< T > ( yaml.Scalar() );
        }
        catch( const std::exception& e )
        {
            Global::errQ( QString( "value cannot be converted" ));
            Global::err( yaml );
            return false;
        }

        return true;
    }
    else
    {
        Global::errQ( QString( "value has to be a scalar." ));
        Global::err( yaml );
        return false;
    }
}

template <>
bool AnyType::readYAML< int32_t >( const YAML::Node yaml, int32_t& target )
{
    return readScalarYaml< int32_t >( yaml, target );
}

template <>
bool AnyType::readYAML< uint32_t >( const YAML::Node yaml, uint32_t& target )
{
    return readScalarYaml< uint32_t >( yaml, target );
}

template <>
bool AnyType::readYAML< float >( const YAML::Node yaml, float& target )
{
    return readScalarYaml< float >( yaml, target );
}

template <>
bool AnyType::readYAML< std::string >( const YAML::Node yaml, std::string& target )
{
    return readScalarYaml< std::string >( yaml, target );
}

template < typename T >
bool AnyType::readSeqYaml( const YAML::Node yaml, std::vector< T >& target )
{
    if ( yaml.IsSequence() )
    {
        //Global::verb( yaml );
        const int count = yaml.size();
        target.clear();

        for( int i = 0; i < count; i++ )
        {
            if ( yaml[ i ].IsScalar() )
            {
                T item;

                try
                {
                    item = strCast< T > ( yaml[ i ].Scalar() );
                }
                catch( const std::exception& e )
                {
                    Global::errQ( QString( "value of item %1 cannot be converted" ).arg( i ) );
                    Global::err( yaml[i] );
                    return false;
                }

                target.push_back( item );
            }
            else
            {
                Global::errQ( QString( "all items in the sequence has to be a scalar. Item %1 is not." ).arg( i ) );
                Global::err( yaml[ i ] );
                return false;
            }
        }

        return true;
    }
    else
    {
        Global::errQ( QString( "value has to be a sequence." ));
        Global::err( yaml );
        return false;
    }
}

template <>
bool AnyType::readYAML< std::vector < float  > >( const YAML::Node yaml, std::vector < float  >& target )
{
    return readSeqYaml< float  >( yaml, target );
}

template <>
bool AnyType::readYAML< std::vector < int32_t  > >( const YAML::Node yaml, std::vector < int32_t >& target )
{
    return readSeqYaml< int32_t  >( yaml, target );
}

template <>
bool AnyType::readYAML< std::vector < uint32_t  > >( const YAML::Node yaml, std::vector < uint32_t >& target )
{
    return readSeqYaml< uint32_t  >( yaml, target );
}


/////////////////////////////
/////////////////////////////
/////////////////////////////
/////////////////////////////

template <>
bool AnyType::load < int32_t, AnyType::Types::Int > ( int32_t p_data )
{
    inPlace< int32_t >( p_data );

    type = AnyType::Types::Int;
    return true;
}

template <>
bool AnyType::load < uint32_t, AnyType::Types::Count > ( uint32_t p_data )
{
    inPlace< uint32_t >( p_data );
    type = AnyType::Types::Count;
    return true;
}

template <>
bool AnyType::load < float, AnyType::Types::Number > ( float p_data )
{
    inPlace< float >( p_data );
    type = AnyType::Types::Number;
    return true;
}

template <>
bool AnyType::load < std::string, AnyType::Types::String > ( std::string p_data )
{
    destroy();
    data = new std::string( p_data );

    if ( data != nullptr )
    {
        incAlloc;
        type = AnyType::Types::String;
        return true;
    }
    else
        return false;
}

template< typename Unit, typename UnitSrc >
bool AnyType::loadFixedArray( std::vector < UnitSrc > p_data, const size_t size )
{
    Q_ASSERT( size >= 2 );
    Q_ASSERT( size <= 4 );

    destroy();
    data =  new Unit[ size ];

    if ( data != nullptr )
    {
        incAlloc;
        size_t count = p_data.size();

        if ( count > size )
            Global::logQ( QString( "loading %1 items into vec%2, extra items will be lost." ).arg( count ).arg( size ) );

        if ( count < size )
            Global::logQ( QString( "loading %1 items into vec%2, missing data will be zero" ).arg( count ).arg( size ) );

        if ( !std::is_signed< Unit >::value && std::is_signed< UnitSrc >::value )
            for( size_t i = 0 ; i < size; ++i )
                ptr< Unit >()[ i ] = ( count > i ) ? Unit( ( p_data[ i ] > 0 ) ? p_data[ i ] : 0 ) : Unit( 0 );
        else
            for( size_t i = 0 ; i < size; ++i )
                ptr< Unit >()[ i ] = ( count > i ) ? Unit(p_data[ i ]) : Unit( 0 );

        if ( std::is_same< Unit, int32_t >::value )
            type = AnyType::Types( int( AnyType::Types::IVec2 ) + size - 2 );
        else if ( std::is_same< Unit, uint32_t >::value )
            type = AnyType::Types( int( AnyType::Types::UVec2 ) + size - 2 );
        else
            type = AnyType::Types( int( AnyType::Types::Vec2 ) + size - 2 );

        return true;
    }
    else
        return false;
}


template <>
bool AnyType::load < std::vector < float  >, AnyType::Types::Vec2 > ( std::vector < float  > p_data )
{
    return loadFixedArray< float  >( p_data, 2 );
}

template <>
bool AnyType::load < std::vector < float  >, AnyType::Types::Vec3 > ( std::vector < float  > p_data )
{
    return loadFixedArray< float  >( p_data, 3 );
}

template <>
bool AnyType::load < std::vector < float  >, AnyType::Types::Vec4 > ( std::vector < float  > p_data )
{
    return loadFixedArray< float  >( p_data, 4 );
}

template <>
bool AnyType::load < std::vector < int32_t  >, AnyType::Types::IVec2 > ( std::vector < int32_t  > p_data )
{
    return loadFixedArray< int32_t >( p_data, 2 );
}

template <>
bool AnyType::load < std::vector < int32_t  >, AnyType::Types::IVec3 > ( std::vector < int32_t  > p_data )
{
    return loadFixedArray< int32_t >( p_data, 3 );
}

template <>
bool AnyType::load < std::vector < int32_t  >, AnyType::Types::IVec4 > ( std::vector < int32_t  > p_data )
{
    return loadFixedArray< int32_t >( p_data, 4 );
}

template <>
bool AnyType::load < std::vector < uint32_t  >, AnyType::Types::UVec2 > ( std::vector < uint32_t  > p_data )
{
    return loadFixedArray< uint32_t >( p_data, 2 );
}

template <>
bool AnyType::load < std::vector < uint32_t  >, AnyType::Types::UVec3 > ( std::vector < uint32_t  > p_data )
{
    return loadFixedArray< uint32_t >( p_data, 3 );
}

template <>
bool AnyType::load < std::vector < uint32_t  >, AnyType::Types::UVec4 > ( std::vector < uint32_t  > p_data )
{
    return loadFixedArray< uint32_t >( p_data, 4 );
}

template < class dstType, class srcType >
bool AnyType::loadDynArray( std::vector < srcType > p_data, Types p_type )
{
    using vecType = std::vector < dstType >;
    destroy();
    data =  new vecType;

    if ( data != nullptr )
    {
        incAlloc;
        size_t count = p_data.size();
        ptr< vecType >()->resize( count );

        for( size_t i = 0 ; i < count; ++i )
        {
            srcType item = p_data[ i ];
            dstType res;
            if ( !std::is_signed< dstType >::value && std::is_signed< srcType >::value )
                res = dstType( ( item > 0 ) ? item : 0 );
            else
                res = dstType( item );

            vectorSet< dstType >( i,  res );
        }

        type = p_type;
        return true;
    }
    else
        return false;
}

template <>
bool AnyType::load < std::vector < float  >, AnyType::Types::Vector > ( std::vector < float  > p_data )
{
    return loadDynArray<float>( p_data, AnyType::Types::Vector );
}

template <>
bool AnyType::load < std::vector < int32_t  >, AnyType::Types::VectorI > ( std::vector < int32_t  > p_data )
{
    return loadDynArray<int32_t>( p_data, AnyType::Types::VectorI );
}

template <>
bool AnyType::load < std::vector < uint32_t  >, AnyType::Types::VectorU > ( std::vector < uint32_t  > p_data )
{
    return loadDynArray<uint32_t>( p_data, AnyType::Types::VectorU );
}

template< typename Unit >
std::string AnyType::dumpArray() const
{
    if ( data == nullptr )
    {
        Global::err( "Serialization have failed. No data allocated." );
        return std::string();
    }

    std::ostringstream out;

    int size;

    if ( std::is_same< Unit, int32_t >::value )
        size = int( type ) - int( AnyType::Types::IVec2 ) + 2;
    else if ( std::is_same< Unit, uint32_t >::value )
        size = int( type ) - int( AnyType::Types::UVec2 ) + 2;
    else
        size = int( type ) - int( AnyType::Types::Vec2 ) + 2;

    out << "[ ";

    for( int i = 0; i < size; i++ )
        out << ptr< Unit >()[ i ] << ", ";

    out << "]";
    return out.str();
}

template< typename Unit >
std::string AnyType::dumpVector() const
{
    if ( data == nullptr )
    {
        Global::err( "Serialization have failed. No data allocated." );
        return std::string();
    }

    std::ostringstream out;

    const std::vector< Unit >* V = ptr< std::vector< Unit > >();

    out << "[ ";

    for( auto i = V->cbegin(); i !=V->cend(); ++i )
        out << ( *i ) << ", ";

    out << "]";
    return out.str();
}

template< typename Unit >
void AnyType::dumpArray( YAML::Emitter& emitter ) const
{
    if ( data == nullptr )
    {
        Global::err( "Serialization have failed. No data allocated." );
        return;
    }

    emitter << YAML::Value << YAML::Flow << YAML::BeginSeq;

    int size;

    if ( std::is_same< Unit, int32_t >::value )
        size = int( type ) - int( AnyType::Types::IVec2 ) + 2;
    else if ( std::is_same< Unit, uint32_t >::value )
        size = int( type ) - int( AnyType::Types::UVec2 ) + 2;
    else
        size = int( type ) - int( AnyType::Types::Vec2 ) + 2;

    for( int i = 0; i < size; i++ )
        emitter << ptr< Unit >()[ i ];

    emitter << YAML::EndSeq;
    return;
}

template< typename Unit > void AnyType::dumpVector( YAML::Emitter& emitter ) const
{
    if ( data == nullptr )
    {
        Global::err( "Serialization have failed. No data allocated." );
        return;
    }

    const std::vector< Unit >* V = ptr< std::vector< Unit > >();

    emitter << YAML::Value << YAML::BeginSeq;

    for( auto i = V->cbegin(); i !=V->cend(); ++i )
        emitter << ( *i );

    emitter << YAML::EndSeq;
    return;
}

QDebug& operator<< ( QDebug& dbg, const AnyType data )
{
    dbg << QString( "%1     # %2" )
            .arg( std::string( data ).c_str() )
            .arg( AnyType::getTypeAsStr( data.getType() ).c_str());
    return dbg;
}

QDebug& operator<< ( QDebug& dbg, const AnyType::Types type )
{
    dbg << AnyType::getTypeAsStr( type ).c_str();
    return dbg;
}

AnyType::operator std::string() const
{
    switch ( type )
    {
        case Types::Empty:
            return std::string( "none" );
        //Inplace types;
        case Types::Int:
            return std::to_string( *inPlace< int32_t >() );
        case Types::Count:
            return std::to_string( *inPlace< uint32_t >() );
        case Types::Number:
            return std::to_string( *inPlace< float >() );

        // String types
        case Types::String:
        case Types::Filename:
            return std::string( *ptr< std::string >() );

        // Finite array type
        case Types::Vec2:
        case Types::Vec3:
        case Types::Vec4:
            return dumpArray< float >();

        case Types::IVec2:
        case Types::IVec3:
        case Types::IVec4:
            return dumpArray<int32_t>();

        case Types::UVec2:
        case Types::UVec3:
        case Types::UVec4:
            return dumpArray<uint32_t>();

        case Types::Vector:
            return dumpVector< float >();
        case Types::VectorI:
            return dumpVector< int32_t >();
        case Types::VectorU:
            return dumpVector< uint32_t >();
        case Types::StrList:
            return dumpVector< std::string >();
        case Types::Image:
        case Types::TensorCPU:
        default:
            Global::err( "Serialization have failed. Unknown type." );
            return std::string();
    }
}

//////////////////////////////////
//////////////////////////////////

bool AnyType::fromYAML( const Types p_type, const YAML::Node yaml )
{
    // Integer type
    bool validYaml = yaml.IsDefined();
    destroy();

    switch ( p_type )
    {
        case Types::Empty:
            return true;

        case Types::Int:
        {
            int32_t data;
            if ( !validYaml || !readYAML < int32_t > ( yaml, data ) )
            {
                data = 0;
                validYaml = false;
            }

            return load< int32_t, Types::Int > (  data ) && validYaml;
        }

        case Types::Count:
        {
            uint32_t data;
            if ( !validYaml || !readYAML < uint32_t > ( yaml, data ) )
            {
                data = 0;
                validYaml = false;
            }

            return load< uint32_t, Types::Count >( data ) && validYaml;
        }

        case Types::Number:
        {
            float data;
            if ( !validYaml || !readYAML< float > ( yaml, data ) )
            {
                data = 0.0f;
                validYaml = false;
            }

            return load < float, Types::Number > ( data ) && validYaml;
        }

        // String types
        case Types::String:
        case Types::Filename:
        {
            std::string data;
            bool res;
            if ( !validYaml || !readYAML < std::string > ( yaml, data ) )
            {
                data = std::string();
                validYaml = false;
            }

            res = load< std::string, Types::String > ( data ) && validYaml;
            type = p_type;

            return res;
        }

        // float array type
        case Types::Vec2:
        case Types::Vec3:
        case Types::Vec4:
        case Types::Vector:
        {
            std::vector < float > data;
            if ( !validYaml || !readYAML < std::vector < float > > ( yaml, data ) )
            {
                data.resize( 0 );
                validYaml = false;
            }

            if      ( p_type == Types::Vec2 )
                return load< std::vector < float >, Types::Vec2 > ( data ) && validYaml;
            else if ( p_type == Types::Vec3 )
                return load< std::vector < float >, Types::Vec3 > ( data ) && validYaml;
            else if ( p_type == Types::Vec4 )
                return load< std::vector < float >, Types::Vec4 > ( data ) && validYaml;
            else // ( p_type == Types::Vector )
                return load< std::vector < float >, Types::Vector > ( data ) && validYaml;

        }

        // integer array type
        case Types::IVec2:
        case Types::IVec3:
        case Types::IVec4:
        case Types::VectorI:
        {
            std::vector < int32_t  > data;
            if ( !validYaml || !readYAML < std::vector < int32_t > > ( yaml, data ) )
            {
                data.resize( 0 );
                validYaml = false;
            }

            if      ( p_type == Types::IVec2 )
                return load< std::vector < int32_t >, Types::IVec2 > ( data ) && validYaml;
            else if ( p_type == Types::IVec3 )
                return load< std::vector < int32_t >, Types::IVec3 > ( data ) && validYaml;
            else if ( p_type == Types::IVec4 )
                return load< std::vector < int32_t >, Types::IVec4 > ( data ) && validYaml;
            else //   ( p_type == Types::VectorI )
                return load< std::vector < int32_t >, Types::VectorI > ( data ) && validYaml;
        }

        case Types::UVec2:
        case Types::UVec3:
        case Types::UVec4:
        case Types::VectorU:
        {
            std::vector < uint32_t  > data;
            if ( !validYaml || !readYAML < std::vector < uint32_t > > ( yaml, data ) )
            {
                data.resize( 0 );
                validYaml = false;
            }

            if      ( p_type == Types::UVec2 )
                return load< std::vector < uint32_t >, Types::UVec2 > ( data ) && validYaml;
            else if ( p_type == Types::UVec3 )
                return load< std::vector < uint32_t >, Types::UVec3 > ( data ) && validYaml;
            else if ( p_type == Types::UVec4 )
                return load< std::vector < uint32_t >, Types::UVec4 > ( data ) && validYaml;
            else // ( p_type == Types::VectorU )
                return load< std::vector < uint32_t >, Types::VectorU > ( data ) && validYaml;
        }
        case Types::StrList:
        case Types::Image:
        case Types::TensorCPU:
            //error
        default:
            break;
    }

    Global::errQ( QString( "fromYAML tries to load the type %1 it doesn't know how to load. Type to call devs" ).arg( int( p_type ) ) );
    return false;
}

void AnyType::toYAML( YAML::Emitter& emitter ) const
{
    switch ( type )
    {
        case Types::Empty:
            break;
        //Inplace types;
        case Types::Int:
            emitter << YAML::Value << *inPlace< int32_t >();
            break;
        case Types::Count:
            emitter << YAML::Value << *inPlace< uint32_t >();
            break;
        case Types::Number:
            emitter << YAML::Value << *inPlace< float >();
            break;

        // String types
        case Types::String:
        case Types::Filename:
            emitter << YAML::Value  << *ptr< std::string >();
            break;

        // Finite array type
        case Types::Vec2:
        case Types::Vec3:
        case Types::Vec4:
            return dumpArray< float >( emitter );

        case Types::IVec2:
        case Types::IVec3:
        case Types::IVec4:
            return dumpArray< int32_t >( emitter );

        case Types::UVec2:
        case Types::UVec3:
        case Types::UVec4:
            return dumpArray< uint32_t >( emitter );

        case Types::Vector:
            dumpVector< float >( emitter );
            break;
        case Types::VectorI:
            dumpVector< int32_t >( emitter );
            break;
        case Types::VectorU:
            dumpVector< uint32_t >( emitter );
            break;
        case Types::StrList:
            dumpVector< std::string >( emitter );
            break;
        case Types::Image:
        case Types::TensorCPU:
            emitter << YAML::Value  << "BIGTYPE";
            break;
        default:
            Global::err( "Serialization have failed. Unknown type." );
            break;
    }
    return;
}

const char AnyType::arrayIdxSymbol[4] = { 'x', 'y', 'z', 'w' };

const std::map< strID, AnyType::Types > AnyType::TypeNames = {
        { strID( "empty" ),    Types::Empty    },
        { strID( "int" ),      Types::Int      },
        { strID( "count" ),    Types::Count    },
        { strID( "string" ),   Types::String   },
        { strID( "filename" ), Types::Filename },
        { strID( "number" ),   Types::Number   },
        { strID( "vec2" ),     Types::Vec2     },
        { strID( "vec3" ),     Types::Vec3     },
        { strID( "vec4" ),     Types::Vec4     },
        { strID( "ivec2" ),    Types::IVec2    },
        { strID( "ivec3" ),    Types::IVec3    },
        { strID( "ivec4" ),    Types::IVec4    },
        { strID( "uvec2" ),    Types::UVec2    },
        { strID( "uvec3" ),    Types::UVec3    },
        { strID( "uvec4" ),    Types::UVec4    },
        { strID( "vector" ),   Types::Vector   },
        { strID( "ivector" ),  Types::VectorI  },
        { strID( "uvector" ),  Types::VectorU  },
        { strID( "strlist" ),  Types::StrList  },
        { strID( "image" ),    Types::Image    },
        { strID( "tensor" ),   Types::TensorCPU}
};

const std::array< strID, int( AnyType::Types::MAX ) > AnyType::TypeNamesRev = {
        strID( "empty" ),    // Types::Empty
        strID( "int" ),      // Types::Int
        strID( "count" ),    // Types::Count
        strID( "string" ),   // Types::String
        strID( "filename" ), // Types::Filename
        strID( "number" ),   // Types::Number
        strID( "vec2" ),     // Types::Vec2
        strID( "vec3" ),     // Types::Vec3
        strID( "vec4" ),     // Types::Vec4
        strID( "ivec2" ),    // Types::IVec2
        strID( "ivec3" ),    // Types::IVec3
        strID( "ivec4" ),    // Types::IVec4
        strID( "uvec2" ),    // Types::UVec2
        strID( "uvec3" ),    // Types::UVec3
        strID( "uvec4" ),    // Types::UVec4
        strID( "vector" ),   // Types::Vector
        strID( "ivector" ),  // Types::VectorI
        strID( "uvector" ),  // Types::VectorU
        strID( "strlist" ),  // Types::StrList
        strID( "image" ),    // Types::Image
        strID( "tensor" ),   // Types::TensorCPU
    };
