/*
 * CRC.cpp
 *
 *  Created on: Feb 24, 2021
 *      Author: vlad
 */

#include <map>
#include <assert.h>
#include <core/UID.h>

std::map< uint64_t, std::string > strID::DB;

strID::strID( const std::string& str )
{
    UID id( str );
    m_uid = id.uid;

    if ( DB.find( m_uid ) != DB.end() )
    {
        // there is an entry for this string in DB already.
        assert( DB[ m_uid ] == str );
    }
    else
        DB[ m_uid ] = str;
}
