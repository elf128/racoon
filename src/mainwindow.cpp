#include <QtWidgets>
#include <QToolBar>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "core/Compute.h"
#include "core/Global.h"
#include "ui/Form_ModelTypeSelector.h"
#include "ui/UI_Node.h"
#include "ui/UI_Scene.h"

#define STOP_ICON ":/icons/Stop.png"
#define STEP_ICON ":/icons/Step.png"
#define PLAY_ICON ":/icons/Play.png"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_viewer( nullptr )
{
    ui->setupUi(this);

    ads::CDockManager::setConfigFlag( ads::CDockManager::FocusHighlighting, true);

    m_stop = ui->toolBar->addAction( QIcon( STOP_ICON ), "Stop model execution" );
    m_step = ui->toolBar->addAction( QIcon( STEP_ICON ), "Single step execution" );
    m_play = ui->toolBar->addAction( QIcon( PLAY_ICON ), "Run model execution" );

    // Create the dock manager. Because the parent parameter is a QMainWindow
    // the dock manager registers itself as the central widget.
    m_DockManager = new ads::CDockManager(this);
    Global::__init__();
    NG_Compute::allocateWorkersPool( 4 );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_triggered()
{
    strID newType;
    ModelTypeSelector* selector = new ModelTypeSelector( &newType );

    int res = selector->exec();

    delete selector;

    if ( res == QDialog::Accepted && newType != strID() )
    {
        NG_Structure* model = new NG_Structure( newType );
        UI_NodeGraph *child = createMdiChild( model );
        child->setFilename("Model.yaml");
        //child->setModel( , model );
        child->show();
    }
}

void MainWindow::on_actionOpen_triggered()
{
    const QString filename = QFileDialog::getOpenFileName(this);
    openFile( filename );
}

void MainWindow::openFile( const QString& fileName )
{
    if (!fileName.isEmpty())
    {
        NG_Structure* model = new NG_Structure;
        if ( model->Load( fileName ) )
        {
            UI_NodeGraph *child = createMdiChild( model );
            child->setFilename( fileName );
            //child->setModel( fileName, model );
            child->show();
        }
        else
        {
            qDebug().noquote() << "Failed to load" << fileName;
            delete model;
        }
    }
    else
        qDebug( "Empty Filename" );
}

void MainWindow::on_actionSave_triggered()
{
    UI_NodeGraph* graph = getCurrentMdi();

    if ( graph )
    {
        if ( !graph->Save() )
        {
            const QString filename = QFileDialog::getSaveFileName(this);

            if ( !filename.isEmpty() )
                graph->SaveTo( filename );
            else
                qDebug( "Empty Filename" );
        }
    }
}

void MainWindow::on_actionSave_As_triggered()
{
    UI_NodeGraph* graph = getCurrentMdi();

    if ( graph )
    {
        const QString filename = QFileDialog::getSaveFileName(this);

        if ( !filename.isEmpty() )
            graph->SaveTo( filename );
        else
            qDebug( "Empty Filename" );
    }
}

void MainWindow::on_actionViewer_toggled( bool show )
{
    if ( show )
    {
        qDebug( "Show Viewer" );
        if ( m_viewer == nullptr )
            m_viewer = createViewerChild();

        m_viewer->show();
    }
    else
    {
        qDebug( "Remove Viewer" );
        if ( m_viewer != nullptr )
        {
            ads::CDockWidget* DockWidget = m_viewer->getContainer();
            DockWidget->closeDockWidget();
            delete DockWidget;
            m_viewer = nullptr;
        }
    }
}

void MainWindow::on_ViewerCloseRequested()
{
    qDebug( "Viewer close requested" );
    ui->actionViewer->setChecked( false );
}

UI_NodeGraph* MainWindow::createMdiChild( NG_Structure* model )
{
    ads::CDockWidget* DockWidget = new ads::CDockWidget( model->getName().c_str() );
    UI_NodeGraph* child = new UI_NodeGraph( model, DockWidget, m_viewer );

    DockWidget->setWidget( child );

    DockWidget->setFeature(ads::CDockWidget::DockWidgetFocusable, true );
    // Add the toggleViewAction of the dock widget to the menu to give
    // the user the possibility to show the dock widget if it has been closed
    //ui->menuView->addAction( DockWidget->toggleViewAction() );

    child->SetupToolbar( model->getType() );
    child->connect( ui->actionCut,    SIGNAL( triggered() ), SLOT( onCut() ) );
    child->connect( ui->actionCopy,   SIGNAL( triggered() ), SLOT( onCopy() ) );
    child->connect( ui->actionPaste,  SIGNAL( triggered() ), SLOT( onPaste() ) );
    child->connect( ui->actionDelete, SIGNAL( triggered() ), SLOT( onDelete() ) );
    child->connect( m_stop,           SIGNAL( triggered() ), SLOT( onStop() ) );
    child->connect( m_step,           SIGNAL( triggered() ), SLOT( onStep() ) );
    child->connect( m_play,           SIGNAL( triggered() ), SLOT( onPlay() ) );


    // Add the dock widget to the top dock widget area
    m_DockManager->addDockWidget( ads::TopDockWidgetArea, DockWidget );
    return child;
}

UI_NodeGraph* MainWindow::getCurrentMdi()
{
    ads::CDockWidget* DockWidget = m_DockManager->focusedDockWidget();//dockWidgetsMap()["NodeGraph"];

    if ( DockWidget )
    {
        UI_NodeGraph* graph = dynamic_cast<UI_NodeGraph*>( DockWidget->widget() );

        if ( graph )
            return graph;
        else
            qDebug( "node graph is null" );
    }
    else
        qDebug( "focusWidget returned null" );

    return nullptr;
}

UI_Viewer* MainWindow::createViewerChild()
{
    ads::CDockWidget* DockWidget = new ads::CDockWidget( "Viewer" );
    UI_Viewer* child = new UI_Viewer( DockWidget );
    DockWidget->setWidget( child );

    DockWidget->setFeature(ads::CDockWidget::CustomCloseHandling,          true);
    DockWidget->setFeature(ads::CDockWidget::DockWidgetFocusable,          false );
    DockWidget->setFeature(ads::CDockWidget::DockWidgetDeleteOnClose,      true);
    DockWidget->setFeature(ads::CDockWidget::DockWidgetForceCloseWithArea, true);

    // Add the toggleViewAction of the dock widget to the menu to give
    // the user the possibility to show the dock widget if it has been closed
    //ui->menuView->addAction( DockWidget->toggleViewAction() );

    //child->SetupToolbar();

    connect( DockWidget, SIGNAL( closeRequested() ), SLOT( on_ViewerCloseRequested()));

    UI_NodeGraph* graph = getCurrentMdi();

    if ( graph  )
    {
        UI_Scene* scene = graph->getScene();

        if ( scene )
            child->connect( scene, SIGNAL( veiwMe( UI_Node*, UI_Scene* ) ),
                                   SLOT(   veiwMe( UI_Node*, UI_Scene* ) ) );
    }
   //child->connect( ui->actionDelete, SIGNAL( triggered() ), SLOT( onDelete() ) );


    // Add the dock widget to the top dock widget area
    m_DockManager->addDockWidget( ads::TopDockWidgetArea, DockWidget );
    return child;
}
