QT += core opengl testlib
#QT += opengl testlib

CONFIG += c++11

#qtConfig(opengles.|angle|dynamicgl): error("This example requires Qt to be configured with -opengl desktop")

HEADERS += \
    #model/Node.h \
    #model/Structure.h \
    core/Global.h \
    core/UID.h \
    core/AnyType.h \
    #model/Registry.h \
    #reflection/Ref_FC.h \
    tests/test_AnyType.h

SOURCES += \
    #model/Node.cpp \
    #model/Structure.cpp \
    core/Global.cpp \
    core/UID.cpp \
    core/AnyType.cpp \
    #model/Registry.cpp \
    #reflection/Ref_FC.cpp \
    tests/tests.cpp \
    tests/test_AnyType.cpp


#TARGET = Tool360

#FORMS += \
#    UI_pc/ParamWidget.ui

unix:!macx {
QT += x11extras
LIBS += -lxcb -lyaml-cpp
INCLUDEPATH += /usr/include/python3.8
}

INCLUDEPATH += $${INCLUDE_EXT}
LIBS += -L$${INCLUDE_EXT} -lmicroNN
