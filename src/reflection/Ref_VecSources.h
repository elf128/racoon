/*
 * Ref_Img.h
 *
 *  Created on: Oct 14, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_REFLECTION_REF_VECSOURCES_H_
#define SRC_REFLECTION_REF_VECSOURCES_H_

#include "core/AnyType.h"
#include "model/Reflection.h"
#include "microNN/Layer.h"

class Ref_ConstVec: public Reflection
{
public:
    Ref_ConstVec( const NG_Node* source ) : Reflection( source ) {}
    virtual ~Ref_ConstVec() {};

    virtual strID GetName() const;
    virtual bool  preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute(NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );

private:
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};

////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////

class Ref_RandomVec: public Reflection
{
public:
    Ref_RandomVec( NG_Node* source );
    virtual ~Ref_RandomVec() {};

    virtual strID GetName() const;
    virtual bool  preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute(NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );

private:
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
    int outs;
};


#endif /* SRC_REFLECTION_REF_VECSOURCES_H_ */
