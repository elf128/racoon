/*
 * Ref_Img.cpp
 *
 *  Created on: Oct 14, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "Ref_VecSources.h"
#include "model/Node.h"

#include <random>

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

bool Ref_ConstVec::preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    const Global::Storage* value = src->storageLookup( strID( "value" ) );

    if ( value )
        value->data.clone( &slice->data[0] );

    (void) workerState;
    return true;
}

bool  Ref_ConstVec::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    (void) slice;
    (void) workerState;
    return true;
}

void  Ref_ConstVec::initCache( NG_Shadow::CacheSlice* slice )
{
    slice->initWith( { AnyType::Types::Empty } );
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

Ref_RandomVec::Ref_RandomVec( NG_Node* source )
    : Reflection( source )
{
    std::random_device rd;
    Global::Storage* seed = source->storageLookupModify( strID( "seed" ) );

    if ( seed )
    {
        if ( seed->data.unsafeValueUInt() == 0 )
        {
            seed->data.unsafeValueUInt() = rd();
        }
    }
    const AnyType::Types outType = getSrcOutputType( 0 );

    switch ( outType )
    {
        case AnyType::Types::Number:  outs =  1; break;
        case AnyType::Types::Vec2:    outs =  2; break;
        case AnyType::Types::Vec3:    outs =  3; break;
        case AnyType::Types::Vec4:    outs =  4; break;
        case AnyType::Types::Int:     outs = -1; break;
        case AnyType::Types::IVec2:   outs = -2; break;
        case AnyType::Types::IVec3:   outs = -3; break;
        case AnyType::Types::IVec4:   outs = -4; break;
        default:                      outs =  0; break;
    }
}

bool Ref_RandomVec::preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    const Global::Storage* seed = src->storageLookup( strID( "seed" ) );

    if ( seed )
    {
        seed->data.clone( &slice->data[1] );

        if ( outs < 0 )
        {
            const Global::Storage* valueMin = src->storageLookup( strID( "min" ) );
            const Global::Storage* valueMax = src->storageLookup( strID( "max" ) );

            if ( valueMin )
            {
                valueMin->data.clone( &slice->data[2] );
                if ( valueMax )
                {
                    valueMax->data.clone( &slice->data[0] );

                    if ( valueMin->data.getType() == valueMax->data.getType() )
                        return true;
                }
            }
        }
        else
        {
            return ( outs > 0 );
        }
    }

    (void) workerState;
    return false;
}

bool  Ref_RandomVec::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    AnyType&       out      = slice->data[0];
    const AnyType& seed     = slice->data[1];
    const uint     count    = abs( outs );

    std::mt19937 gen( seed.unsafeValueUInt() + static_cast< uint32_t >( slice->time ) );

    if ( outs < 0 )
    {
        const AnyType& valueMin = slice->data[2];

        if ( count == 1 )
        {
            std::uniform_int_distribution< int32_t > rnd( valueMin.unsafeValueInt(), out.unsafeValueInt() );
            out.unsafeValueInt() = rnd( gen );
        }
        else
        {
            for( uint i = 0; i < count; i++ )
            {
                std::uniform_int_distribution< int32_t > rnd( valueMin.unsafeArrayInt( i ), out.unsafeArrayInt( i ) );
                out.unsafeArrayInt( i ) = rnd( gen );
            }
        }
    }
    else
    {
        if ( count == 1 )
        {
            std::uniform_real_distribution< float > rnd( 0.0f, 1.0f );
            out.unsafeValueNumber() = rnd( gen );
        }
        else
        {
            for( uint i = 0; i < count; i++ )
            {
                std::uniform_real_distribution< float > rnd( 0.0f, 1.0f );
                out.unsafeArrayNumber( i ) = rnd( gen );
            }
        }
    }

    (void) workerState;
    return true;
}

void  Ref_RandomVec::initCache( NG_Shadow::CacheSlice* slice )
{
    switch( outs )
    {
        case 0: // THIS SHOULD NOT HAPPEN
            slice->initWith( {
                AnyType::Types::Empty,   // Result
                AnyType::Types::Count    // Seed
            } );
            break;
        case 1:
            slice->initWith( {
                AnyType::Types::Number,   // Result
                AnyType::Types::Count    // Seed
            } );
            break;
        case 2:
            slice->initWith( {
                AnyType::Types::Vec2,   // Result
                AnyType::Types::Count    // Seed
            } );
            break;
        case 3:
            slice->initWith( {
                AnyType::Types::Vec3,   // Result
                AnyType::Types::Count    // Seed
            } );
            break;
        case 4:
            slice->initWith( {
                AnyType::Types::Vec4,   // Result
                AnyType::Types::Count    // Seed
            } );
            break;        default:
            slice->initWith( {
                AnyType::Types::Empty,   // Result/Max
                AnyType::Types::Count,   // Seed
                AnyType::Types::Empty,   // Min
            } );
            break;
    }
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


REGISTER_REFLECTION( Ref_ConstVec );
REGISTER_REFLECTION( Ref_RandomVec );


