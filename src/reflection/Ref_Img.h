/*
 * Ref_Img.h
 *
 *  Created on: Oct 14, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_REFLECTION_REF_IMG_H_
#define SRC_REFLECTION_REF_IMG_H_

#include "core/AnyType.h"
#include "model/Reflection.h"
#include "microNN/Layer.h"

using TensorType = Tensor_cpu< float >;

class Ref_Img: public Reflection
{
public:
    Ref_Img( const NG_Node* source ) : Reflection( source ) {};
    virtual ~Ref_Img() {};

};

class Ref_ImgLoad: public Ref_Img
{
public:
    Ref_ImgLoad( const NG_Node* source ) : Ref_Img( source ) {}
    virtual ~Ref_ImgLoad() {}

    virtual strID GetName() const;
    virtual bool  preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute(NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );

private:
    //Reflection* srcNameRef;
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};

class Ref_ImgSave: public Ref_Img
{
public:
    Ref_ImgSave( const NG_Node* source ): Ref_Img( source ), srcImageRef( nullptr ), srcImageIdx( -1 ) {}
    virtual ~Ref_ImgSave() {}

    virtual strID GetName() const;
    virtual bool  preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute(NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual void  Reflect();

private:
    Reflection* srcImageRef;
    int         srcImageIdx;
    //Reflection* srcNameRef;
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};

////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////

class Ref_ImgCrop: public Ref_Img
{
public:
    Ref_ImgCrop( const NG_Node* source ) : Ref_Img( source ), srcImageRef( nullptr ), srcImageIdx( -1 ) {}
    virtual ~Ref_ImgCrop() {}

    virtual strID GetName() const;
    virtual bool  preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute(NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual void  Reflect();

private:
    Reflection* srcImageRef;
    int         srcImageIdx;
    //Reflection* srcOffsetRef;
    //Reflection* srcSizeRef;
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};
#endif /* SRC_REFLECTION_REF_IMG_H_ */
