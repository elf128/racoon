/*
 * Ref_FC.cpp
 *
 *  Created on: Apr 3, 2021
 *      Author: vlad
 */

#include "Ref_FC.h"
#include "model/Node.h"
#include <QDebug>

void Ref_Layer::initCache( NG_Shadow::CacheSlice* slice )
{
}
/*
NG_Shadow::CacheSlice* Ref_Layer::getCacheSlice( const int time )
{
    return shadow.getCacheSlice( time );
}
*/

///////////////////////////////////////////////////////////

Ref_FC::Ref_FC( const NG_Node* source ) : Ref_Layer( source )
{
    layerFC = nullptr;
    qDebug() << "Ref_FC reflection has been made for node: " << source->getName().c_str();
}

Ref_FC::~Ref_FC()
{
    if ( layerFC )
        delete layerFC;
}
/*
void  Ref_FC::Reflect()
{
    if ( layerFC )
        delete layerFC;

    //layerFC = new Layer< TensorType >( )
}*/

bool  Ref_FC::preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    return false;
}

bool  Ref_FC::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    return false;
}

Layer< TensorType >* Ref_FC::getOutputLayer()
{
    return layerFC;
}

REGISTER_REFLECTION( Ref_FC );

Ref_Layer::Ref_Layer( const NG_Node* source ) : Reflection( source ) {}
