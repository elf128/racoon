/*
 * M_Basic.cpp
 *
 *  Created on: Nov 5, 2021
 *      Author: vlad
 */

#include "M_Basic.h"
#include "model/Structure.h"
#include <QDebug>
#include "model/Structure.h"
#include "model/Node.h"

M_Basic::M_Basic( const NG_Structure* source )
    : Mirror( source )
{
    qDebug() << "M_Basic mirror has been made for structure: " << source->getName().c_str();
}

REGISTER_MIRROR( M_Basic );

void  M_Basic::Reflect()
{
    dependency.clear();

    for ( auto i = src->m_nodes.cbegin(); i != src->m_nodes.cend(); ++i )
    {
        if ( (*i)->m_outputs.size() == 0 )
            dependency.push_back( (*i)->m_reflection );
    }
    qDebug() << src->getName().c_str() << " have been reflected with M_Basic. ";
    shadow.dirtyCache();
}

bool  M_Basic::preCompute(  NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )  { return true; }
bool  M_Basic::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState ) { return true; }
void  M_Basic::initCache( NG_Shadow::CacheSlice* slice ) { (void) slice; }
