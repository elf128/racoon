/*
 * Ref_Img.cpp
 *
 *  Created on: Oct 14, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "Ref_Img.h"
#include "model/Node.h"
#include <QImage>

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

bool  Ref_ImgLoad::preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    const Global::Storage* file = src->storageLookup( strID( "file" ) );
    if ( file )
        file->data.clone( &slice->data[ 1 ] );

    (void) workerState;
    return true;
}

bool  Ref_ImgLoad::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    AnyType::ImageInit init;
    init.load_from_file = true;
    init.filename       = slice->data[1].unsafeString();

    slice->data[0].initAs( AnyType::Types::Image, &init );

    (void) workerState;
    return true;
}

void  Ref_ImgLoad::initCache( NG_Shadow::CacheSlice* slice )
{
    slice->initWith( { AnyType::Types::Image, AnyType::Types::Filename } );
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

void  Ref_ImgSave::Reflect()
{
    Reflection::Reflect();
    srcImageRef = getInput( strID( "in0" ), srcImageIdx );
}

bool  Ref_ImgSave::preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    const Global::Storage* file = src->storageLookup( strID( "file" ) );
    if ( file )
        file->data.clone( &slice->data[ 0 ] );

    (void) workerState;
    return true;
}

bool  Ref_ImgSave::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    if ( srcImageRef )
    {
        NG_Shadow::CacheSlice* inputCache = srcImageRef->getCacheSlice( slice->time );

        const AnyType& input = inputCache->data[ srcImageIdx ];

        if ( input.getType() == AnyType::Types::Image )
        {
            const QImage* image = input.unsafeImage();
            if ( image )
            {
                const QString filename = QString( slice->data[ 0 ].unsafeString().c_str() );
                image->save( filename );
            }
        }
    }
    (void) workerState;
    return true;
}

void  Ref_ImgSave::initCache( NG_Shadow::CacheSlice* slice )
{
    slice->initWith( { AnyType::Types::Filename } );
}


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void  Ref_ImgCrop::Reflect()
{
    Reflection::Reflect();
    srcImageRef = getInput( strID( "in0" ), srcImageIdx );
}


bool Ref_ImgCrop::preCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    const Global::Storage* offset = src->storageLookup( strID( "offset" ) );
    const Global::Storage* size   = src->storageLookup( strID( "size" ) );

    if ( offset && size )
    {
        slice->data[1].unsafe_iX() = offset->data.unsafe_iX();
        slice->data[1].unsafe_iY() = offset->data.unsafe_iY();
        slice->data[1].unsafe_iZ() = size->data.unsafe_uX();
        slice->data[1].unsafe_iW() = size->data.unsafe_uY();
    }
    else
    {
        slice->data[1].unsafe_iX() = 0;
        slice->data[1].unsafe_iY() = 0;
        slice->data[1].unsafe_iZ() = 0;
        slice->data[1].unsafe_iW() = 0;
    }

    (void) workerState;
    return true;
}

bool  Ref_ImgCrop::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{
    if ( srcImageRef )
    {
        NG_Shadow::CacheSlice* inputCache = srcImageRef->getCacheSlice( slice->time );
        const AnyType& input = inputCache->data[ srcImageIdx ];

        if ( input.getType() == AnyType::Types::Image )
        {
            const QImage* image = input.unsafeImage();
            if ( image )
            {

                slice->data[ 0 ].unsafeImage()[0] = std::move(
                        image->copy(
                                    slice->data[1].unsafe_iX(),
                                    slice->data[1].unsafe_iY(),
                                    slice->data[1].unsafe_iZ(),
                                    slice->data[1].unsafe_iW()
                                    )
                                );
            }
        }
    }

    (void) workerState;
    return true;
}

void  Ref_ImgCrop::initCache( NG_Shadow::CacheSlice* slice )
{
    slice->initWith( { AnyType::Types::Image, AnyType::Types::IVec4 } );
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


REGISTER_REFLECTION( Ref_ImgLoad );
REGISTER_REFLECTION( Ref_ImgSave );
REGISTER_REFLECTION( Ref_ImgCrop );


