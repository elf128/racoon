/*
 * M_Basic.h
 *
 *  Created on: Nov 5, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_REFLECTION_M_BASIC_H_
#define SRC_REFLECTION_M_BASIC_H_

#include "model/Reflection.h"

class M_Basic: public Mirror
{
public:
    M_Basic( const NG_Structure* source );
    virtual ~M_Basic() {}

    virtual strID GetName() const;
    virtual void  Reflect();
    virtual bool  preCompute(  NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );

private:
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};

#endif /* SRC_REFLECTION_M_BASIC_H_ */
