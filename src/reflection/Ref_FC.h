/*
 * Ref_FC.h
 *
 *  Created on: Apr 3, 2021
 *      Author: vlad
 */

#ifndef SRC_REFLECTION_REF_FC_H_
#define SRC_REFLECTION_REF_FC_H_

#include "model/Reflection.h"
#include "microNN/Layer.h"

using TensorType = Tensor_cpu< float >;

class Ref_Layer: public Reflection
{
public:
    Ref_Layer( const NG_Node* source );
    virtual ~Ref_Layer() {};

    virtual Layer< TensorType >* getOutputLayer() = 0;
private:
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};

class Ref_FC: public Ref_Layer
{
    Layer_FC< TensorType >* layerFC;
public:
    Ref_FC( const NG_Node* source );
    virtual ~Ref_FC();

    virtual strID GetName() const;
    //virtual void  Reflect();
    virtual bool  preCompute(  NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );
    virtual bool  postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState );

    virtual Layer< TensorType >* getOutputLayer();
};

#endif /* SRC_REFLECTION_REF_FC_H_ */
