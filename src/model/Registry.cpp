/*
 * Registry.cpp
 *
 *  Created on: Apr 3, 2021
 *      Author: vlad
 */

#include "Registry.h"
#include <QDebug>

Smoke::Smoke()
    : shadow( this )
{

}

Smoke::~Smoke()
{

}

NG_Shadow::CacheSlice* Smoke::getCacheSlice( const int time )
{
    return shadow.getCacheSlice( time );
}

const std::vector< Smoke* >& Smoke::getDependency()
{
    return dependency;
}

const std::vector< Smoke* >& Smoke::getTargets()
{
    return targets;
}

void Registry::RegisterMirror( const strID name, MirrorMaker maker )
{
    mirrors[ name ] = maker;
    qDebug() << "RegisterMirror: " << name.c_str();
}

void Registry::RegisterReflection( const strID name, ReflectionMaker maker )
{
    reflections[ name ] = maker;
    qDebug() << "RegisterReflection: " << name.c_str();
}

Mirror* Registry::makeMirror(  const strID name, NG_Structure* source )
{
    auto F = mirrors.find( name );
    if ( F == mirrors.end() )
        return nullptr;

    return F->second( source );
}

Reflection* Registry::makeReflection( const strID name, NG_Node* source   )
{
    auto F = reflections.find( name );
    if ( F == reflections.end() )
        return nullptr;

    return F->second( source );
}

void Registry::kickSmoke( Smoke* smoke )
{
    //smokeStack.push_back( smoke );
    NG_Compute::Eliminate( smoke );
    delete smoke;
}

/*
void Registry::flushSmoke()
{
    for ( auto i = smokeStack.begin(); i != smokeStack.end(); ++i )
        delete *i;

    smokeStack.clear();
}*/

MirrorReg::MirrorReg( const strID name, MirrorMaker maker )
{
    Registry::RegisterMirror( name, maker );
}

ReflectionReg::ReflectionReg( const strID name, ReflectionMaker maker )
{
    Registry::RegisterReflection( name, maker );
}


std::map< strID, MirrorMaker >     Registry::mirrors;
std::map< strID, ReflectionMaker > Registry::reflections;
//std::vector< Smoke* >              Registry::smokeStack;



