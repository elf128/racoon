/*
 * Shadow.cpp
 *
 *  Created on: Oct 19, 2021
 *      Author: vlad
 */

#include "Shadow.h"

#include "core/Global.h"
#include "Registry.h"

NG_Shadow::NG_Shadow( Smoke* p_smoke )
{
    parent = p_smoke;
}

NG_Shadow::~NG_Shadow()
{
    // TODO Auto-generated destructor stub
}

/**************************/
NG_Shadow::CacheSlice::CacheSlice( NG_Shadow::CacheSlice&& other )
    : data(      std::move( other.data      ))
    , time(  other.time )
{
    bool d   = other.dirty;
    dirty    = d;
    State s  = other.state;
    state    = s;
    depCount = 0;
}

NG_Shadow::CacheSlice::CacheSlice( const int p_time )
    : time( p_time )
    , dirty( true )
    , state( State::Idle )
{}

void NG_Shadow::CacheSlice::initWith( const std::vector< AnyType::Types > additional )
{
    data.clear();
    int idx = 0;
    for( auto it = additional.cbegin(); it != additional.cend(); ++it, idx++ )
    {
        data.push_back( AnyType() );
        data[ idx ].initAs(*it );
    }
}

NG_Shadow::CacheSlice* NG_Shadow::getCacheSlice( const int time )
{
    std::lock_guard< std::recursive_mutex > Lock( lock );

    for( auto i = cache.begin(); i != cache.end(); ++i )
        if ( i->time == time )
            return &(*i);

    Global::verbQ( QString( "There's no record of timeslice %1. Making new one" ).arg( time ) );
    initCache( time );
    return &( cache[ cache.size() - 1 ] );
}

void NG_Shadow::initCache( const int time )
{
    CacheSlice slice( time );

    cache.push_back( std::move( slice ) );

    parent->initCache( &( cache[ cache.size() - 1 ] ) );
}

void NG_Shadow::nukeCache()
{
    cache.clear();
}

void NG_Shadow::dirtyCache()
{
    for( auto i = cache.begin(); i != cache.end(); ++i )
        i->setDirty();
}
