/*
 * Node.h
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#ifndef SRC_MODEL_NODE_H_
#define SRC_MODEL_NODE_H_

#include "core/Compute.h"
#include "core/Global.h"
#include "core/UID.h"

class NG_Node;
class Reflection;
class NG_Structure;

namespace YAML { class Emitter; }

struct NG_SlotIdxPair
{
    uint32_t src;
    uint32_t dst;
    NG_SlotIdxPair() : src( 0xFFFFFFFF ), dst( 0xFFFFFFFF ) {}
    NG_SlotIdxPair( const uint32_t from, const uint32_t to ) : src( from ), dst( to ) {}
    NG_SlotIdxPair( const NG_SlotIdxPair& other ) : src( other.src ), dst( other.dst ) {}
    const NG_SlotIdxPair& operator=( const NG_SlotIdxPair& other ) { src = other.src; dst = other.dst; return *this; }

    bool operator == ( const NG_SlotIdxPair& other ) const { return ( src == other.src) && ( dst == other.dst ); }
};

struct NG_SlotPair
{
    strID src;
    strID dst;
    NG_SlotPair() : src(), dst() {}
    NG_SlotPair( const strID from, const strID to ) : src( from ), dst( to ) {}
    NG_SlotPair( const NG_SlotPair& other ) : src( other.src ), dst( other.dst ) {}
    const NG_SlotPair& operator=( const NG_SlotPair& other ) { src = other.src; dst = other.dst; return *this; }

    bool operator == ( const NG_SlotPair& other ) const { return ( src == other.src) && ( dst == other.dst ); }
};
struct NG_Source
{
    strID          Name;
    strID          Type;
};

struct NG_Target
{
    NG_Node*       Node;
    NG_SlotPair    Slots;
    //NG_SlotIdxPair Idx;
    strID          Type;
    NG_Target() : Node( nullptr ) {}
    NG_Target( NG_Node* p_node, const NG_SlotPair& p_slots ) : Node( p_node ), Slots( p_slots ), Type() {}
    NG_Target( NG_Node* p_node, const NG_SlotPair& p_slots/*, const NG_SlotIdxPair& p_idx*/, const strID p_type ) : Node( p_node ), Slots( p_slots ),/* Idx( p_idx ),*/ Type( p_type ) {}

    bool operator ==( const NG_Target& other ) const { return ( Node == other.Node ) && ( Slots == other.Slots ); }
    operator bool() const { return Node != nullptr; }
};

class NG_Node
{
public:
    NG_Node( const strID& p_name,
             const int p_x, const int p_y,
             const Global::uiColor& color,
             const Global::StrList& p_Ins,
             const Global::StrList& p_Outs,
             const NG_Structure*  p_structure );

    ~NG_Node();


    strID                  m_name;
    bool                   m_saved;
    bool                   m_touched;
    int                    m_x;
    int                    m_y;
    Global::uiColor        m_color;
    NG_ExecMode            m_execMode;


    uint  getTotalInputCount() const;
    uint  getTotalOutputCount() const;

    uint  getNativeInputCount() const;
    uint  getNativeOutputCount() const;

    const NG_Target* getSource( const strID slot ) const { int idx; if ( ( idx = getInputIndex( slot ) ) >= 0 ) return &( m_inputs[ idx ] ); else return nullptr; }
    const strID      getName() const { return m_name; }
    const strID      getType() const { return m_type; }

    static strID     genInputName(    const uint idx );
    static strID     genOutputName(   const uint idx );

    bool             emitYaml( YAML::Emitter& doc );
    bool             LoadParams( const Global::typeMap& params, const YAML::Node nodeData );
    void             Touch();
    void             ExposeParam( const strID param );
    void             CollapseParam( const strID param );

    const std::vector< NG_Target > getTargets() const;
    const Global::Storage*         storageLookup( const strID id ) const
    {
        for( auto it = m_storage.cbegin(); it != m_storage.cend(); ++it )
            if ( it->name == id )
                return static_cast< const Global::Storage* >( &(*it) );

        return nullptr;
    }

    Global::Storage*               storageLookupModify( const strID id )
    {
        for( auto it = m_storage.begin(); it != m_storage.end(); ++it )
            if ( it->name == id )
                return static_cast< Global::Storage* >( &(*it) );

        return nullptr;
    }

private:
    std::vector< Global::Storage > m_storage;
    strID                          m_type;

    std::vector< NG_Target >       m_inputs;
    std::vector< NG_Source >       m_outputs;
    uint                           m_nativeInputCount;
    uint                           m_nativeOutputCount;

    bool  ConnectFrom(     const NG_SlotPair p_slots, NG_Node* source );
    bool  isLinkedFrom(    const NG_SlotPair p_slots, NG_Node* source );
    bool  isLinkedTo(      const NG_SlotPair p_slots, NG_Node* target );
    int   getInputIndex(   const strID p_slot ) const;
    int   getOutputIndex(  const strID p_slot ) const;
    bool  DisconnectInput( const strID p_slot );
    bool  makeReflection(  const strID& typeName, const strID& refName );
    void  TouchOutputs();

    void Reflect();  // Reflection removes touch.

protected:
    std::vector< NG_Target >  m_outputLinks;
    Reflection*               m_reflection;
    const NG_Structure*       m_structure;
    bool registerTarget( const NG_Target& target );
    bool removeTarget(   const NG_Target& target );

    friend class NG_Structure;
    friend class Reflection;
    friend class UI_Node;
    friend class UI_Scene;
    friend class UI_Props;
    friend class UI_Viewer;
    friend class Mirror;
    friend class M_Basic;

};

#endif /* SRC_MODEL_NODE_H_ */
