/*
 * Registry.h
 *
 *  Created on: Apr 3, 2021
 *      Author: Vlad A. < elf128@gmail.com >
 */

#ifndef SRC_MODEL_REGISTRY_H_
#define SRC_MODEL_REGISTRY_H_

#include "Shadow.h"
#include <map>

#include "core/Compute.h"
#include "core/UID.h"
//#include <atomic>

class Mirror;
class Reflection;
class NG_Node;
class NG_Structure;

class Smoke
{
protected:
    NG_Shadow              shadow;
    std::vector< Smoke* >  dependency;
    std::vector< Smoke* >  targets;
public:
    Smoke();
    virtual ~Smoke() = 0;

    virtual strID GetName() const = 0;
    virtual void  Reflect() = 0;
    virtual bool  preCompute(  NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& ) = 0;
    virtual bool  postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& ) = 0;

    virtual NG_Shadow::CacheSlice* getCacheSlice( const int time );
    virtual const std::vector< Smoke* >& getDependency();
    virtual const std::vector< Smoke* >& getTargets();

private:
    virtual void  initCache( NG_Shadow::CacheSlice* slice ) = 0;

    friend class NG_Shadow;
};

template < class T > Mirror*     MirrorFactory( NG_Structure* source ) { return new T( source ); }
template < class T > Reflection* ReflectionFactory( NG_Node* source )  { return new T( source ); }

typedef Mirror*     (*MirrorMaker)( NG_Structure* source );
typedef Reflection* (*ReflectionMaker)( NG_Node* source );


class Registry
{
    static std::map< strID, MirrorMaker >     mirrors;
    static std::map< strID, ReflectionMaker > reflections;
    //static std::vector< Smoke* >              smokeStack;
public:
    static void RegisterMirror(     const strID name, MirrorMaker maker );
    static void RegisterReflection( const strID name, ReflectionMaker maker );

    static Mirror*     makeMirror(     const strID name, NG_Structure* source  );
    static Reflection* makeReflection( const strID name, NG_Node*      source  );
    static void        kickSmoke( Smoke* smoke );
    //static void        flushSmoke();
};

struct MirrorReg
{
    MirrorReg() = delete;
    MirrorReg( const MirrorReg& ) = delete;
    MirrorReg( const strID name, MirrorMaker );
};

struct ReflectionReg
{
    ReflectionReg() = delete;
    ReflectionReg( const ReflectionReg& ) = delete;
    ReflectionReg( const strID name, ReflectionMaker );
};

#define makeStr(S) #S

#define SMOKE_REGISTER( Name ) \
        strID staticName_##Name( makeStr(Name) ); \
        strID Name::GetName() const { return staticName_##Name; } \

#define REGISTER_MIRROR( Name ) \
        SMOKE_REGISTER( Name ) \
        MirrorReg _mirror_reg_##Name( strID( makeStr(Name) ), MirrorFactory< Name >);

#define REGISTER_REFLECTION( Name ) \
        SMOKE_REGISTER( Name ) \
        ReflectionReg _reflection_reg_##Name( strID( makeStr(Name) ), ReflectionFactory< Name > );

#endif /* SRC_MODEL_REGISTRY_H_ */
