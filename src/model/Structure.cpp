/*
 * Structure.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#include "core/Global.h"
#include "Structure.h"
#include "Node.h"
#include "Reflection.h"

#include <fstream>
#include <regex>
#include <yaml-cpp/yaml.h>
#include <QDebug>


NG_Structure::NG_Structure() : m_name( "" ), m_type( "" ), m_mirror( nullptr ) {}

NG_Structure::NG_Structure( const strID p_type )
    : m_name( "" )
    , m_type( p_type )
    , m_mirror( nullptr )
{
    const Global::SysDef *const def = Global::findSysDef( m_type );
    if ( def )
    {
        makeMirror( m_type, def->mirror );
        useMirror();
    }
}

NG_Structure::~NG_Structure()
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );

    while( m_nodes.size() )
    {
        NG_Node* N = m_nodes[ m_nodes.size()-1 ];
        m_nodes.pop_back();

        Registry::kickSmoke( N->m_reflection );
        N->m_reflection = nullptr;

        delete N;
    }

    Registry::kickSmoke( m_mirror );
}

void NG_Structure::AddNode( NG_Node* p_node )
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );
    Q_ASSERT( std::find( m_nodes.begin(), m_nodes.end(), p_node ) == m_nodes.end() );

    this->m_nodes.push_back( p_node );

}

void NG_Structure::DelNode( NG_Node* p_node )
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );

    auto it = std::find( m_nodes.begin(), m_nodes.end(), p_node );
    Q_ASSERT( it != m_nodes.end() );

    //for( int i=0; i < p_node->getInputCount(); i++)
    //    p_node->ConnectFrom( 0, i );

    this->m_nodes.erase( it );

    Registry::kickSmoke( p_node->m_reflection );
    p_node->m_reflection = nullptr;

    delete p_node;
}

bool NG_Structure::Rename( NG_Node* p_node, const strID newName )
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );

    Global::verb( "NG_Structure::Rename" );
    auto it = std::find( m_nodes.begin(), m_nodes.end(), p_node );
    if( it == m_nodes.end() )
        // Node is not in the structure.
        return false;

    if ( FindNode( newName ) )
        // New name is not unique
        return false;

    p_node->m_name = newName;
    return true;
}

bool NG_Structure::Rename( const strID newName )
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );

    m_name = newName;
    return true;
}

bool NG_Structure::Link( NG_Node* p_src, NG_Node* p_dst, const strID p_srcSlot, const strID p_dstSlot )
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );

    if ( p_src && p_dst )
        return p_dst->ConnectFrom( NG_SlotPair( p_srcSlot, p_dstSlot ), p_src );
    else
        return false;
}


bool NG_Structure::CheckLink( NG_Node* p_src, NG_Node* p_dst, const strID p_srcSlot, const strID p_dstSlot ) const
{
    if ( p_src && p_dst )
        return p_dst->isLinkedFrom( NG_SlotPair( p_srcSlot, p_dstSlot ), p_src );
    else
        return false;
}

bool NG_Structure::Unlink( NG_Node* p_dst, const strID p_dstSlot )
{
    std::lock_guard< std::recursive_mutex > lock( m_lock );

    if ( p_dst )
        return p_dst->DisconnectInput( p_dstSlot );

    return false;
}

bool NG_Structure::Save( const QString p_filename ) const
{
    YAML::Emitter doc;

    //doc << YAML::BeginDoc;
    doc << YAML::BeginMap;
    doc << YAML::Key << m_type.c_str();

    doc << YAML::Value << YAML::BeginMap;

    emitPropsToYaml( doc );

    doc << YAML::Key   << "nodes";
    doc << YAML::Value << YAML::BeginSeq;

    for( auto n = m_nodes.begin(); n != m_nodes.end(); n++ )
        (*n)->m_saved = false;

    bool notDone = true;

    do
    {
        uint savedThisTime = 0;

        for ( auto n = m_nodes.begin(); n != m_nodes.end(); n++ )
        {
            NG_Node* node = *n;

            if ( node->m_saved == false && node->emitYaml( doc ) )
            {
                node->m_saved = true;
                savedThisTime++;
            }
        }

        if ( savedThisTime == 0 )
        {
            Global::err( "Unresolved cyclic dependency in the graph\n SAVE IS UNFINISHED.\nRemove cyclic dependency and try again" );
            return false;
        }

        notDone = false;

        foreach( NG_Node* n, m_nodes )
            if (n->m_saved == false )
            {
                notDone = true;
                break;
            }

    }
    while( notDone );

    doc << YAML::EndSeq;
    doc << YAML::EndMap;

    //doc << YAML::EndDoc;

    std::ofstream file( p_filename.toStdString() );
    file << doc.c_str();

    Global::verb( "Yaml" );
    Global::verb( doc.c_str() );
    Global::verb( "Yaml end" );

    return true;
}

bool NG_Structure::emitPropsToYaml( YAML::Emitter& doc ) const
{
    doc << YAML::Key   << "name" << YAML::Value << m_name;
    return true;
}

bool NG_Structure::Load( const QString p_filename )
{
    try
    {
        YAML::Node doc = YAML::LoadFile( p_filename.toStdString() );

        if ( !doc )
        {
            Global::err( "Failed to find root 'network'" );
            Global::err( p_filename.toStdString() );
            Global::err( doc.as< std::string >() );
            return false;
        }

        m_type = doc.begin()->first.as<std::string>();
        YAML::Node props = doc.begin()->second;

        if ( !props || props.IsMap() == false )
        {
            Global::err( "Failed to parse root of a 'network'" );
            Global::err( p_filename.toStdString() );
            Global::err( doc.as< std::string >() );
            return false;
        }

        const Global::SysDef *const def = Global::findSysDef( m_type );

        if ( def == nullptr )
        {
            Global::errQ( QString( "NodeGraph type '%1' is unknown. Cannot load." ).arg( m_type.c_str() ) );
            Global::err( doc );
            return false;
        }

        YAML::Node name = props["name"];
        if ( name && name.IsScalar() )
            m_name = name.as< std::string >();
        else
            m_name = std::string( "Network" );

        YAML::Node net = props["nodes"];
        if ( !net || net.IsSequence() == false )
        {
            Global::err( "Failed to find nodes of the 'network'" );
            Global::err( p_filename.toStdString() );
            Global::err( doc.as< std::string >() );
            return false;
        }

        Global::logQ( QString( "Found network %1 with %2 nodes").arg( m_type.c_str() ).arg( net.size() ) );

        {
            std::lock_guard< std::recursive_mutex > lock( m_lock );

            for( uint idx = 0; idx  < net.size(); idx++ )
            {
                NG_Node* node = ConstructNode( net[ idx ] );
                if ( node == nullptr )
                {
                    Global::err( "Node wasn't constructed. Exiting" );
                    return false;
                }
                AddNode( node );
            }

            makeMirror( m_type, def->mirror );
            useMirror();
        }
    }
    catch( const std::exception& e )
    {
        Global::err( e.what() );

        return false;
    }
    return true;
}

NG_Node* NG_Structure::FindNode( const strID& p_name )
{
    for( uint i=0;i<this->m_nodes.size();i++)
        if ( this->m_nodes[i]->m_name == p_name )
            return this->m_nodes[i];

    return 0;
}

const NG_Node* NG_Structure::FindNode( const strID& p_name ) const
{
    for( uint i=0;i<this->m_nodes.size();i++)
        if ( this->m_nodes[i]->m_name == p_name )
            return this->m_nodes[i];

    return 0;
}

NG_Node* NG_Structure::ConstructNode( YAML::Node yaml )
{
    NG_Node* node = nullptr;

    try
    {
        Global::verb( "------\nConstructNode:" );
        Global::verb( yaml );

        const std::string nodeTypeName = yaml.begin()->first.as<std::string>();
        const YAML::Node  nodeData     = yaml.begin()->second;

        const Global::NodeDef *const def = Global::findLayerDef( m_type, nodeTypeName );

        Global::verbQ( QString( "Node type '%1'" ).arg( nodeTypeName.c_str() ) );

        if ( def == nullptr )
        {
            Global::errQ( QString( "Node type '%1' is unknown" ).arg( nodeTypeName.c_str() ) );
            Global::err( yaml );
            return nullptr;
        }

        Global::verb( "got descriptions" );

        if ( nodeData.IsMap() == false )
        {
            Global::errQ( QString( "Node data has to be YAML Map." ));
            Global::err( yaml );
            return nullptr;
        }

        const std::string& name = nodeData[ "name" ].as<std::string>();
        const int posX          = nodeData[ "pos" ][0].as<int>();
        const int posY          = nodeData[ "pos" ][1].as<int>();

        const Global::StrList& Ins    = def->inputTypes;
        const Global::StrList& Outs   = def->outputTypes;
        const Global::uiColor& color  = def->color;
        const Global::typeMap& params = def->params;

        {
            std::lock_guard< std::recursive_mutex > lock( m_lock );

            node = new NG_Node( name, posX, posY, color, Ins, Outs, this );

            Global::verb( "Linking: " );

            node->LoadParams( params, nodeData );
            LinkByYaml( node, nodeData );
            node->makeReflection( nodeTypeName, def->reflection );
        }
        return node;
    }
    catch( const std::exception& e )
    {
        Global::err( e.what() );
        Global::err( yaml );

        if ( node != nullptr )
            delete node;

        return nullptr;
    }
}

bool NG_Structure::LinkByYaml( NG_Node* node, const YAML::Node nodeData )
{
    std::regex linkExp("^\\s*<-(.+)\\.(.+)->\\s*$");

    for( auto i = nodeData.begin(); i != nodeData.end(); ++i )
    {
        const strID key = strID(  i->first.as<std::string>() );
        if ( ( key != strID( "pos" ) ) && ( key != strID("name") ) )
        {
            int slotIdx = node->getInputIndex( key );

            if ( slotIdx >= 0 )
            {
                const std::string link = i->second.as< std::string >();
                std::smatch match;

                if ( link == std::string( "<-->" ) )
                    continue;

                if ( std::regex_search( link, match, linkExp ) && match.size() > 2)
                {
                    const std::string nodeName = match.str(1);
                    const std::string outName  = match.str(2);

                    Global::verbQ( QString( "Link to %1[ %2 ]" ).arg( nodeName.c_str() ).arg( outName.c_str() ) );

                    NG_Node* src = FindNode( nodeName );
                    if ( src != nullptr )
                    {
                        if ( Link( src, node, outName, key ) == false )
                            Global::err( "Link fail." );
                    }
                    else
                        Global::errQ( QString( "cannot find the node %1" ).arg( nodeName.c_str() ));
                }
                else
                    Global::errQ( QString( "unknown format for link %1 or not a link" ).arg( link.c_str() ) );
            }
            else
                Global::verbQ( QString( "no slot for %1" ).arg( key.c_str() ) );
        }
    }
}

const strID NG_Structure::generateNodeName( const strID id ) const
{
    std::string Rbase = id.c_str();
    int idx = 0;

    do
    {
        strID R = strID( Rbase + "_" + std::to_string( idx ) );
        const NG_Node* n = FindNode( R );
        if ( n == nullptr )
            return R;

        idx++;
    }while(1);
}


NG_Node* NG_Structure::ConstructNode( const strID nodeType, const int p_x, const int p_y ) const
{
    NG_Node* node = nullptr;

    Global::verb( "------\nConstructNode:" );

    const Global::NodeDef *const def = Global::findLayerDef( m_type, nodeType );

    Global::verbQ( QString( "Node type '%1'" ).arg( nodeType.c_str() ) );

    if ( def == nullptr )
    {
        Global::errQ( QString( "Node type '%1' is unknown" ).arg( nodeType.c_str() ) );
        return nullptr;
    }

    Global::verb( "got descriptions" );

    const strID name              = generateNodeName( nodeType );
    const Global::StrList& Ins    = def->inputTypes;
    const Global::StrList& Outs   = def->outputTypes;
    const Global::uiColor& color  = def->color;
    const Global::typeMap& params = def->params;

    node = new NG_Node( name, p_x, p_y, color, Ins, Outs, this );

    node->LoadParams( params, YAML::Load("") );
    node->makeReflection( nodeType, def->reflection );

    return node;
}

void NG_Structure::Rebuild()
{
    std::vector< NG_Node* > touched;
    bool done;

    do
    {
        done = true;
        for( auto i = m_nodes.begin(); i != m_nodes.end(); ++i )
        {
            if ( (*i)->m_touched )
            {
                (*i)->m_touched = false;
                (*i)->TouchOutputs();
                touched.push_back( *i );
                done = false;
            }
        }
    }
    while( !done );

    for( auto i = m_nodes.begin(); i != m_nodes.end(); ++i )
        (*i)->Reflect();

    useMirror();
}

void NG_Structure::computeStop()
{
    Global::logQ( QString( "Stop compute for structure %1").arg( m_name.c_str() ) );
}

void NG_Structure::computeStep()
{
    Global::logQ( QString( "=====================\nStep in compute for structure %1").arg( m_name.c_str() ) );
    NG_Compute::PushCompute( m_mirror, 0 );
}

void NG_Structure::computePlay()
{
    Global::logQ( QString( "Run compute for structure %1").arg( m_name.c_str() ) );
}

bool NG_Structure::makeMirror( const strID& typeName, const strID& mirrorName )
{
    if ( m_mirror )
    {
        Global::err( "Reflection already exist. Something went wrong. Troubles ahead, please save your work." );
        return false;
    }

    m_type       = typeName;
    m_mirror = Registry::makeMirror( mirrorName, this );

    return ( m_mirror != nullptr );
}

void NG_Structure::useMirror()
{
    if ( m_mirror )
        m_mirror->Reflect();
}

