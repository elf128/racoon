/*
 * Registry.cpp
 *
 *  Created on: Apr 3, 2021
 *      Author: vlad
 */

#include "Reflection.h"
#include <QDebug>

#include "model/Structure.h"

/**************************/

REGISTER_REFLECTION( Ref_None );

Ref_None::Ref_None( const NG_Node* source ) : Reflection( source )
{
    qDebug() << "Dummy reflection has been made for node: " << source->getName().c_str();
}

Ref_None::~Ref_None()     {}
bool  Ref_None::preCompute(  NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{ (void) workerState; (void) slice; return true; }

bool  Ref_None::postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& workerState )
{ (void) workerState; (void) slice; return true; }
void  Ref_None::initCache( NG_Shadow::CacheSlice* slice ) { (void) slice; }

/*****************************/

Reflection* Reflection::getInput( const strID slotName, int& srcOutputNum )
{
    if ( src == nullptr )
        return nullptr;

    int inputIdx = src->getInputIndex( slotName );

    if ( inputIdx < 0 )
        return nullptr;

    const NG_Target& in = src->m_inputs[ inputIdx ];
    if ( in.Node && in.Node->m_reflection )
    {
        srcOutputNum = in.Node->getOutputIndex( in.Slots.src );
        return ( srcOutputNum >= 0 ) ? in.Node->m_reflection : nullptr;
    }
    else
        return nullptr;
}

void  Reflection::Reflect()
{
    shadow.dirtyCache();
    dependency.clear();
    targets.clear();

    for ( auto i = src->m_inputs.cbegin(); i != src->m_inputs.cend(); ++i )
    {
        const NG_Target& target = *i;

        if ( target.Node )
            dependency.push_back( target.Node->m_reflection );
    }

    if ( src->m_outputLinks.size() )
        for ( auto i = src->m_outputLinks.cbegin(); i != src->m_outputLinks.cend(); ++i )
        {
            const NG_Target& target = *i;

            if ( target.Node )
                targets.push_back( target.Node->m_reflection );
        }
    else
    {
        if ( src->m_structure && src->m_structure->m_mirror )
            targets.push_back( static_cast< Smoke* >( src->m_structure->m_mirror ) );
    }

    qDebug() << src->getName().c_str() << " have been reflected with " << GetName().c_str();
}
