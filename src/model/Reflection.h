/*
 * Registry.h
 *
 *  Created on: Apr 3, 2021
 *      Author: Vlad A. < elf128@gmail.com >
 */

#ifndef SRC_MODEL_REFLECTION_H_
#define SRC_MODEL_REFLECTION_H_

#include "Registry.h"
#include "model/Node.h"

class NG_Structure;

class Mirror : public Smoke
{
public:
    const NG_Structure* src;
    Mirror( const NG_Structure* source )
        : Smoke()
        , src( source )
    {}
    virtual ~Mirror() override {}
};

class Reflection : public Smoke
{
public:
    const NG_Node* src;
    Reflection( const NG_Node* source )
        : Smoke()
        , src( source ){}
    virtual ~Reflection() override {}

    virtual void  Reflect();
    virtual Reflection* getInput( const strID slotNum, int& srcOutputName ); // SLOW! DO NOT USE INSIDE COMPUTE.

    inline AnyType::Types getSrcOutputType( const uint slot ) const { return AnyType::resolveType( src->m_outputs[ slot ].Type ); }
};

class Ref_None: public Reflection
{
public:
    Ref_None( const NG_Node* source );
    virtual ~Ref_None();

    virtual strID GetName() const;
    virtual bool  preCompute(  NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& );
    virtual bool  postCompute( NG_Shadow::CacheSlice* slice, std::atomic< NG_Compute::Worker::State >& );

private:
    virtual void  initCache( NG_Shadow::CacheSlice* slice );
};

#endif /* SRC_MODEL_REFLECTION_H_ */
