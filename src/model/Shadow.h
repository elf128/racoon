/*
 * Shadow.h
 *
 *  Created on: Oct 19, 2021
 *      Author: vlad
 */

#ifndef SRC_MODEL_SHADOW_H_
#define SRC_MODEL_SHADOW_H_

#include <vector>
#include <mutex>
#include <atomic>

#include "core/AnyType.h"

class strID;
class Smoke;

class NG_Shadow
{
    enum class State
    {
        Idle,
        In_Pre,
        In_Post,
        Ready,
    };
public:
    struct CacheSlice
    {
        std::recursive_mutex   lock;
        //std::vector< strID >   datanames;
        std::vector< AnyType > data;
        int                    time;

        std::atomic< bool >    dirty;
        std::atomic< State >   state;
        std::atomic< int >     depCount;

        CacheSlice( const int time );
        CacheSlice( CacheSlice&& other );

        bool  isDirty() const { return dirty; }
        void  setDirty() { dirty = true; }
        void  setClean() { dirty = false; };
        void  incDepCount() { depCount++; }

        void  initWith( const std::vector< AnyType::Types > additional );
    };

    NG_Shadow( Smoke* p_smoke );
    ~NG_Shadow();

    CacheSlice* getCacheSlice( const int time );
    void        nukeCache();
    void        dirtyCache();

private:
    std::vector< CacheSlice > cache;
    std::recursive_mutex      lock;
    Smoke*                    parent;

    void initCache( const int time );
};

#endif /* SRC_MODEL_SHADOW_H_ */
