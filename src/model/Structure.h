/*
 * Structure.h
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#ifndef SRC_MODEL_STRUCTURE_H_
#define SRC_MODEL_STRUCTURE_H_

#include <vector>
#include <mutex>

#include "core/Global.h"

class NG_Node;
class Mirror;

namespace YAML { class Node; class Emitter; }

class NG_Structure
{
    strID                              m_name;
    strID                              m_type;
    std::vector< NG_Node* >            m_nodes;
    std::vector< Global::Storage >     m_storage;
    std::recursive_mutex               m_lock;

public:
    NG_Structure();
    NG_Structure( const strID p_type );
    ~NG_Structure();

    void AddNode( NG_Node* p_node );
    void DelNode( NG_Node* p_node );

    bool Link(      NG_Node* p_src, NG_Node* p_dst, const strID p_srcSlot, const strID p_dstSlot );
    bool CheckLink( NG_Node* p_src, NG_Node* p_dst, const strID p_srcSlot, const strID p_dstSlot ) const;
    bool Unlink(    NG_Node* p_dst,                 const strID p_dstSlot );
    void Rebuild();

    bool Rename(    NG_Node* p_node, const strID newName );
    bool Rename(    const strID newName );

    bool Save( const QString p_filename ) const;
    bool Load( const QString p_filename );

    void computeStop();
    void computeStep();
    void computePlay();

    NG_Node* FindNode( const strID& p_name );
    const NG_Node* FindNode( const strID& p_name ) const;

    //NG_Node* FindNode( uint64_t uid );
    NG_Node* ConstructNode( YAML::Node yaml );
    NG_Node* ConstructNode( const strID nodeType, const int p_x = 0, const int p_y = 0 ) const;
    bool     LinkByYaml( NG_Node* node, const YAML::Node nodeData );

    bool        emitPropsToYaml( YAML::Emitter& doc ) const;

    const strID getName() const { return m_name; }
    const strID getType() const { return m_type; }
    const strID generateNodeName( const strID id ) const;

    const Global::Storage*         storageLookup( const strID id ) const
    {
        for( auto it = m_storage.cbegin(); it != m_storage.cend(); ++it )
            if ( it->name == id )
                return static_cast< const Global::Storage* >( &(*it) );

        return nullptr;
    }

    Global::Storage*               storageLookupModify( const strID id )
    {
        for( auto it = m_storage.begin(); it != m_storage.end(); ++it )
            if ( it->name == id )
                return static_cast< Global::Storage* >( &(*it) );

        return nullptr;
    }

private:
    Mirror*               m_mirror;

    bool makeMirror( const strID& typeName, const strID& mirrorName );
    void useMirror();

    friend class UI_Scene;
    friend class UI_Props;
    friend class UI_Viewer;
    friend class Mirror;
    friend class Reflection;
    friend class M_Basic;
};

#endif /* SRC_MODEL_STRUCTURE_H_ */
