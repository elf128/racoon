/*
 * Node.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "Node.h"
#include "Reflection.h"
#include "Structure.h"

#include <yaml-cpp/yaml.h>
#include <regex>

NG_Node::NG_Node( const strID& p_name,
                  const int p_x, const int p_y,
                  const Global::uiColor& color,
                  const Global::StrList& p_nativeIns,
                  const Global::StrList& p_nativeOuts,
                  const NG_Structure*  p_structure )
    : m_name(        p_name )
{
    m_x        = p_x;
    m_y        = p_y;
    m_color    = color;
    m_saved    = false;
    m_touched  = true;
    m_execMode = NG_ExecMode::Normal;

    m_reflection        = nullptr;
    m_structure         = p_structure;
    m_nativeInputCount  = p_nativeIns.size();
    m_nativeOutputCount = p_nativeOuts.size();

    m_inputs.resize(      p_nativeIns.size() );
    m_outputs.resize(     p_nativeOuts.size() );

    for( uint i = 0; i < m_nativeInputCount; i++ )
    {
        //m_inputs[ i ].Idx.dst   = i;
        m_inputs[ i ].Slots.dst = genInputName( i );
        m_inputs[ i ].Type      = p_nativeIns[ i ];
    }

    for( uint i = 0; i < m_nativeOutputCount; i++ )
    {
        m_outputs[ i ].Name = genOutputName( i );
        m_outputs[ i ].Type = p_nativeOuts[ i ];
    }
}

NG_Node::~NG_Node()
{
    // ToDo: UI is cleaning links, but in case of direct deletion of the node we can have invalid links.
    //       Make sure we clean up links, just in case.
    if ( m_reflection )
        Global::err( "Zombie reflection detected. Troubles ahead. Save your work." );
}

uint NG_Node::getTotalInputCount() const
{
    return m_inputs.size();
}

uint NG_Node::getTotalOutputCount() const
{
    return m_outputs.size();
}

uint NG_Node::getNativeInputCount() const
{
    return m_nativeInputCount;
}

uint NG_Node::getNativeOutputCount() const
{
    return m_nativeOutputCount;
}

strID NG_Node::genInputName( const uint idx )
{
    std::string S = "in";
    S += std::to_string( idx );
    return strID( S );
}

strID NG_Node::genOutputName( const uint idx )
{
    //char slotName[8];
    //std::sprintf( slotName, "in%d", i );
    std::string S = "out";
    S += std::to_string( idx );
    return strID( S );
}

int NG_Node::getInputIndex(   const strID p_slot ) const
{
    for( uint i = 0; i < m_inputs.size(); i++ )
        if ( m_inputs[ i ].Slots.dst == p_slot )
            return static_cast< int >( i );

    return -1;
}

int NG_Node::getOutputIndex( const strID p_slot ) const
{
    for( uint i = 0; i < m_outputs.size(); i++ )
        if ( m_outputs[ i ].Name == p_slot )
            return static_cast< int >( i );

    return -1;
}
bool NG_Node::ConnectFrom( const NG_SlotPair p_slots, NG_Node* source )
{
    int dst = getInputIndex(  p_slots.dst );
    int src = source->getOutputIndex( p_slots.src );

    if ( dst < 0 )
        return false;

    if ( m_inputs[ dst ].Type != source->m_outputs[ src ].Type )
        return false;

    if ( m_inputs[ dst ].Node )
        if ( DisconnectInput( p_slots.dst ) == false )
            return false;

    NG_Target target( this, p_slots, m_inputs[ dst ].Type );

    if ( source->registerTarget( target ) == false )
        return false;

    m_inputs[ dst ].Node  = source;
    m_inputs[ dst ].Slots = p_slots;

    m_touched = true;

    return true;
}

bool NG_Node::isLinkedFrom( const NG_SlotPair p_slots, NG_Node* source )
{
    int dst = getInputIndex(  p_slots.dst );
    if ( dst < 0 )
        return false;

    const NG_Target& link = m_inputs[ dst ];

    if ( link.Node == source && link.Slots == p_slots )
        return link.Node->isLinkedTo( p_slots, this );
    else
        return false;

}

bool NG_Node::isLinkedTo( const NG_SlotPair p_slots, NG_Node* target )
{
    auto it = std::find( m_outputLinks.begin(), m_outputLinks.end(), NG_Target( target, p_slots ) );
    return it != m_outputLinks.end();
}

bool NG_Node::DisconnectInput( const strID slot )
{
    int idx = getInputIndex( slot );

    if ( idx < 0 )
        return false;

    if ( m_inputs[ idx ].Node == nullptr || m_inputs[ idx ].Slots.dst != slot )
        return false;

    bool res = m_inputs[ idx ].Node->removeTarget( NG_Target( this, m_inputs[ idx ].Slots ) );

    if ( res )
        m_inputs[ idx ].Node = nullptr;

    return res;
}

bool NG_Node::registerTarget( const NG_Target& target )
{
    auto it = std::find( m_outputLinks.begin(), m_outputLinks.end(), target );
    if ( it != m_outputLinks.end() )
        return false;

    m_outputLinks.push_back( target );
    return true;
}

bool NG_Node::removeTarget( const NG_Target& target )
{
    auto it = std::find( m_outputLinks.begin(), m_outputLinks.end(), target );
    if ( it == m_outputLinks.end() )
        return false;

    m_outputLinks.erase( it );

    return true;
}

const std::vector< NG_Target > NG_Node::getTargets() const
{
    return m_outputLinks;
}

bool NG_Node::LoadParams( const Global::typeMap& params, const YAML::Node nodeData )
{
    std::regex linkExp("^\\s*<-(.*)->\\s*$");

    Global::verb( "--\nLoadParams:" );
    for( auto i = params.cbegin(); i != params.cend(); ++i )
    {
        const YAML::Node P = nodeData[ i->name.c_str() ];

        m_storage.push_back( i->name );

        bool load_data = true;
        if ( P && P.IsScalar() )
        {
            const std::string S = P.as< std::string >();
            std::smatch match;

            if ( std::regex_search( S, match, linkExp ) && match.size() > 1 )
            {
            // It's exposed!
                AnyType& data  = m_storage[ m_storage.size() - 1 ].data;
                data.fromYAML( i->type, YAML::Node() );

                ExposeParam( i->name );
                load_data = false;
            }
        }

        if ( load_data )
        {
            CollapseParam( i->name );
            AnyType& data  = m_storage[ m_storage.size() - 1 ].data;
            data.fromYAML( i->type, P );

            Global::verbQ( QString("%1 = %2( %3 )")
                    .arg( i->name.c_str() )
                    .arg( AnyType::getTypeAsStr( data.getType() ).c_str() )
                    .arg( std::string( data ).c_str() ) );
        }
    }

    return true;
}

void NG_Node::ExposeParam( const strID param )
{
    Global::Storage* store = storageLookupModify( param );

    if ( store )
    {
        store->exposed = true;
        int slot = getInputIndex( param );
        if ( slot < 0 )
            m_inputs.push_back( NG_Target( nullptr, NG_SlotPair( strID(), param ), AnyType::getTypeAsStr( store->data.getType() ) ) );

        Touch();
    }
}

void NG_Node::CollapseParam( const strID param )
{
    Global::Storage* store = storageLookupModify( param );

    if ( store )
    {
        store->exposed = false;

        for( auto it = m_inputs.begin(); it != m_inputs.end(); )
            if ( it->Slots.dst == param )
            {
                if ( it->Node )
                    DisconnectInput( param );

                it = m_inputs.erase( it );
            }
            else
                ++it;

        Touch();
    }
}

bool NG_Node::emitYaml( YAML::Emitter& doc )
{
    for( uint in = 0; in < m_inputs.size(); in++ )
        if ( m_inputs[ in ].Node != nullptr && m_inputs[ in ].Node->m_saved == false )
            //dependency, skip this node.
            return false;

    doc << YAML::BeginMap;
    if ( !m_type.isNull() )
        doc << YAML::Key << m_type;
    else
    {
        Global::err( "Trying to serialise node with unknown type. Cannot save!" );
        Global::err( m_name );
        return true;  // Return true, to indicate, that we're done with this node.
    }

    doc << YAML::Value << YAML::BeginMap;
    doc << YAML::Key << "name" << YAML::Value <<  m_name;

    Global::StrList connected;
    for( uint i = 0; i < m_inputs.size(); i++ )
    {
        const NG_Target& link = m_inputs[ i ];
        if ( link.Node )
        {
            char in[128];
            char out[128];
            std::sprintf( in,  "%s", link.Slots.dst.c_str() );
            std::sprintf( out, "<-%s.%s->", link.Node->m_name.c_str(), link.Slots.src.c_str() );
            doc << YAML::Key << in << YAML::Value << out;
            connected.push_back( link.Slots.dst );
        }
    }
    doc << YAML::Key << "pos"  << YAML::Value;
    doc << YAML::Flow << YAML::BeginSeq << m_x << m_y << YAML::EndSeq;

    for( auto i = m_storage.cbegin(); i != m_storage.cend(); ++i )
    {
        auto it = std::find( connected.begin(), connected.end(), i->name );
        if ( it != connected.end() )
            continue;

        doc << YAML::Key << i->name;

        if ( i->exposed )
            doc << YAML::Value << "<-->";
        else
            i->data.toYAML( doc );
    }

    doc << YAML::EndMap;
    doc << YAML::EndMap;

    return true;
}

bool NG_Node::makeReflection( const strID& typeName, const strID& refName )
{
    if ( m_reflection )
    {
        Global::err( "Reflection already exist. Something went wrong. Troubles ahead, please save your work." );
        return false;
    }

    m_type       = typeName;
    m_reflection = Registry::makeReflection( refName, this );

    return ( m_reflection != nullptr );
}

void NG_Node::Reflect()
{
    if ( m_reflection )
        m_reflection->Reflect();

    m_touched = false;
}

void NG_Node::Touch()
{
    m_touched = true;
}


void NG_Node::TouchOutputs()
{
    for( auto i = m_outputLinks.begin(); i != m_outputLinks.end(); ++i )
        i->Node->Touch();
}
