/*
 * UI_NodeGraph.cpp
 *
 *  Created on: Feb 21, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "UI_NodeGraph.h"
#include "ui_NodeGraph.h"
#include "UI_Scene.h"

#include <QToolBar>
#include "DockManager.h"

#define PROP_ICON ":/icons/Props.png"


UI_NodeGraph::UI_NodeGraph( NG_Structure* p_model, ads::CDockWidget* widget, QWidget* p_viewer, QWidget *parent )
    : QWidget( parent )
    , ui(      new Ui::UI_NodeGraph )
    , m_model( p_model )
{
    ui->setupUi( this );
    container = widget;
    m_scene   = ui->canvas->SetModel( m_model, ui->props, this, p_viewer );
}

UI_NodeGraph::~UI_NodeGraph()
{
    delete ui;
    delete m_model;
}

bool UI_NodeGraph::SaveTo( const QString filename )
{
    m_model->Save( filename );
    m_filename = filename;

    return true;
}

bool UI_NodeGraph::Save()
{
    if ( m_filename.isEmpty() )
        return false;

    m_model->Save( m_filename );
    return true;
}

void UI_NodeGraph::SetupToolbar( const strID& type )
{
    if ( container )
    {
        QToolBar* bar = container->createDefaultToolBar();
        QAction* showProps = bar->addAction( QIcon( PROP_ICON ), "Show model's properties." );

        bar->addSeparator();

        const Global::StrList groups = Global::listGroups( type );
        for ( auto i = groups.cbegin(); i != groups.cend(); ++i )
        {
            QAction* action = bar->addAction( QString( i->c_str() ) );
            action->setEnabled( false );

            const Global::StrList layers = Global::listLayers( type, *i );

            for ( auto j = layers.cbegin(); j != layers.cend(); ++j )
            {
                QAction* action = bar->addAction( QString( j->c_str() ) );
                m_actionMap[ action ] = *j;
            }

            bar->addSeparator();
        }
        this->connect( showProps, SIGNAL( triggered() ), SLOT( ShowProps() ) );
        this->connect( bar,       SIGNAL( actionTriggered(QAction*) ), SLOT( onToolBar(QAction*) ) );

    }
}


void UI_NodeGraph::ShowProps()
{
    Global::verb( "Show props for root" );
    emit clickTripleDot();
}

void UI_NodeGraph::Rename( const QString& name )
{
    //Global::verb( "UI_NodeGraph::Rename" );
    container->setWindowTitle( name );
}

void UI_NodeGraph::onToolBar( QAction* action )
{

    auto a = m_actionMap.find( action );

    if ( a == m_actionMap.end() )
        return;

    const strID nodeType = a->second;

    Global::verbQ( QString( "onToolBar( %1 )") .arg( nodeType.c_str() ) );


    QPoint pos = ui->canvas->visibleRegion().boundingRect().center();

    int newX = pos.x();
    int newY = pos.y();

    NG_Node* node = m_model->ConstructNode( nodeType, newX, newY );

    if ( node && m_scene )
        m_scene->AddNodeToModel( node );
}

void UI_NodeGraph::onCut()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_scene->onCut();
}

void UI_NodeGraph::onCopy()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_scene->onCopy();
}

void UI_NodeGraph::onPaste()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_scene->onPaste();
}

void UI_NodeGraph::onDelete()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_scene->onDelete();
}

void UI_NodeGraph::onStop()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_model->computeStop();
}

void UI_NodeGraph::onStep()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_model->computeStep();
}

void UI_NodeGraph::onPlay()
{
    if ( container->property( "focused" ).toBool() == false )
        return;

    m_model->computePlay();
}

