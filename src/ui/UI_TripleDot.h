/*
 * UI_TripleDot.h
 *
 *  Created on: May 11, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_UI_UI_TRIPLEDOT_H_
#define SRC_UI_UI_TRIPLEDOT_H_

#include <QGraphicsItem>
#include <qgraphicsitem.h>
#include "core/Global.h"

class UI_Node;

class UI_TripleDot : public QObject, public QGraphicsItem
{
    Q_OBJECT

    static const uint c_hovered  = 0x00FF40;
    static const uint c_selected = 0xFF8000;
    static const uint c_normal   = 0x000000;

public:
    UI_TripleDot( UI_Node *p_parent, QPointF p_pos );
    virtual ~UI_TripleDot() {}

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget );

    bool getIsSelected() const { return isSelected; }
    void setIsSelected( const bool set ) { isSelected = set; }

signals:
    void clickTripleDot( UI_TripleDot* node );

protected:
    void mousePressEvent(   QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(    QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event);

    bool isSelected;

};
#endif /* SRC_UI_UI_TRIPLEDOT_H_ */
