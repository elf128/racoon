/*
 * UI_Scene.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "core/Global.h"
#include "UI_Scene.h"
#include <QtGui>
#include <QDebug>

#include <QGraphicsSceneMouseEvent>
#include "model/Structure.h"
#include "model/Node.h"

#include "ui/UI_Node.h"
#include "ui/UI_Link.h"
#include "ui/UI_Socket.h"
#include "ui/UI_LinkDrag.h"
#include "ui/UI_TripleDot.h"
#include "ui/UI_ViewSign.h"


void UI_Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
    if ( m_linkDrag )
    {
        if ( QGraphicsItem* item = this->itemAt( e->scenePos(), QTransform() ) )
        {
            if ( item != m_linkDrag )
            {
                UI_Socket* socket = dynamic_cast< UI_Socket* >( item );

                m_linkDrag->setTarget( nullptr );
                bool lockeOnInput = m_linkDrag->isLockedOnDst();

                if ( socket && socket->isDataType( m_linkDrag->getDataType() ) )
                    if ( ( !lockeOnInput && socket->isInput() == true  ) ||
                         (  lockeOnInput && socket->isInput() == false ) )
                    {
                        m_linkDrag->setSnap(   socket->scenePos() );
                        m_linkDrag->setTarget( socket );
                    }
                    else
                        m_linkDrag->unSnap();
                else
                    m_linkDrag->unSnap();
            }
            else
                m_linkDrag->unSnap();
        }
        else
            m_linkDrag->unSnap();

        m_linkDrag->OnDrag( e );
    }
    QGraphicsScene::mouseMoveEvent( e );
}

void UI_Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
    QGraphicsScene::mouseReleaseEvent( e );
    if ( m_linkDrag )
    {
        qDebug() << "UI_Scene::DROP!!!";

        const bool lockeOnDst    = m_linkDrag->isLockedOnDst();
        const bool newLink       = m_linkDrag->isNewLink();

        UI_Socket* origin = m_linkDrag->getOrigin();
        UI_Link*   link   = m_linkDrag->getOldLink();
        UI_Socket* target = m_linkDrag->getTarget();

        removeItem( m_linkDrag );
        delete m_linkDrag;
        m_linkDrag = nullptr;

        if ( target )
        {
            if ( newLink && origin == nullptr )
            {
                Global::verb( "UI_Scene::mouseReleaseEvent: Sanity check failed. Origin is not defined" );
                return;
            }

            if ( target->isInput() && lockeOnDst )
            {
                Global::verb( "UI_Scene::mouseReleaseEvent: Cannon connect two inputs" );
                return;
            }

            if ( !target->isInput() && !lockeOnDst )
            {
                Global::verb( "UI_Scene::mouseReleaseEvent: Cannon connect two outputs" );
                return;
            }

            UI_Node* srcNode;
            UI_Node* dstNode;
            strID srcSlot, dstSlot;

            if ( lockeOnDst )
            {
                Global::verb( "UI_Scene::mouseReleaseEvent: destination is output" );
                srcNode = static_cast< UI_Node* >( target->parentItem() );
                srcSlot = target->slot.slotName;

                if ( newLink )
                {
                    dstNode = static_cast< UI_Node* >( origin->parentItem() );
                    dstSlot = origin->slot.slotName;
                }
                else
                {
                    dstNode = link->m_to;
                    dstSlot = link->m_slotTo->slot.slotName;
                }
            }
            else
            {
                Global::verb( "UI_Scene::mouseReleaseEvent: destination is input");
                dstNode = static_cast< UI_Node* >( target->parentItem() );
                dstSlot =  target->slot.slotName;

                if ( newLink )
                {
                    srcNode = static_cast< UI_Node* >( origin->parentItem() );
                    srcSlot = origin->slot.slotName;
                }
                else
                {
                    srcNode = link->m_from;
                    srcSlot = link->m_slotFrom->slot.slotName;
                }
            }

            Global::verbQ( QString( "UI_Scene::mouseReleaseEvent: %1.%2 -> %3.%4 ")
                    .arg( srcNode->node->m_name.c_str() )
                    .arg( srcSlot.c_str() )
                    .arg( dstNode->node->m_name.c_str() )
                    .arg( dstSlot.c_str() ) );

            if ( AddLinkToModel( srcNode, dstNode, srcSlot, dstSlot ) && newLink == false )
                DeleteLinkFromModel( link->m_to->node, link->m_slotTo->slot.slotName );
        }
        else
        {
            if ( newLink )
                Global::verb( "UI_Scene::mouseReleaseEvent: drop target is not defined for new link. Linking is not done.");
            else
            {
                Global::verb( "UI_Scene::mouseReleaseEvent: old link is dropped out. Disconnecting!!");
                DeleteLinkFromModel( link->m_to->node, link->m_slotTo->slot.slotName );
            }
        }

        update();
        m_model->Rebuild();

    }

}

void UI_Scene::startDrag( UI_Socket *p_gui_socket, const strID p_type )
{
    if ( m_linkDrag )
        return;

    if ( p_gui_socket == nullptr )
        return;

    m_linkDrag = new UI_LinkDrag(
            p_gui_socket->mapToScene( 0, 0 ),
            ( p_gui_socket->isInput() == false ) ? UI_LinkDrag::LinkType::NewOutToIn : UI_LinkDrag::LinkType::NewInToOut,
            p_type );

    m_linkDrag->setOrigin( p_gui_socket );
    addItem( m_linkDrag );

    //qDebug() << "UI_Scene::startDrag( UI_Socket )" << p_type.c_str();;
}

void UI_Scene::startDrag( UI_Link *p_gui_link, bool dragEnd, const strID p_type  )
{
    if ( m_linkDrag )
        return;

    if ( p_gui_link == nullptr )
        return;

    if ( dragEnd )
        m_linkDrag = new UI_LinkDrag( p_gui_link->mapToScene( p_gui_link->getLinkOriginPos() ), UI_LinkDrag::LinkType::OldDragEnd, p_type );
    else
        m_linkDrag = new UI_LinkDrag( p_gui_link->mapToScene( 0, 0 ), UI_LinkDrag::LinkType::OldDragStart, p_type );

    m_linkDrag->setOldLink( p_gui_link );
    addItem( m_linkDrag );
    qDebug() << "UI_Scene::startDrag( UI_Link )" << p_type.c_str();
}

void UI_Scene::clickTripleDot( UI_TripleDot* p_nodeDots )
{
    qDebug().noquote().nospace() << QString( "TripleDot clicked" );
    if ( p_nodeDots == nullptr )
    {
        qDebug().noquote().nospace() << QString( "TripleDot click handler in UI_Scene got nullptr. Abort." );
        return;
    }

    UI_Node* P = dynamic_cast< UI_Node* > ( p_nodeDots->parentItem() );
    if ( P == nullptr )
    {
        qDebug().noquote().nospace() << QString( "TripleDot click handler in UI_Scene cannot querry parent item. Abort." );
        return;
    }

    emit showProps( P, this );
}

void UI_Scene::clickView( UI_ViewSign* p_nodeDots )
{
    qDebug().noquote().nospace() << QString( "View clicked" );

    if ( p_nodeDots == nullptr )
    {
        qDebug().noquote().nospace() << QString( "View click handler in UI_Scene got nullptr. Abort." );
        return;
    }

    UI_Node* P = dynamic_cast< UI_Node* > ( p_nodeDots->parentItem() );
    if ( P == nullptr )
    {
        qDebug().noquote().nospace() << QString( "View click handler in UI_Scene cannot querry parent item. Abort." );
        return;
    }

    emit veiwMe( P, this );
}

void UI_Scene::clickTripleDot()
{
    qDebug().noquote().nospace() << QString( "Root's TripleDot clicked" );

    emit showProps( nullptr, this );
}

void UI_Scene::changeProp( const strID& prop, const QString& newValue  )
{
    // This function only takes care of model renaming.
    if ( m_model )
    {
        if ( prop == strID( "Name" ) )
        {
            if ( m_model->Rename( newValue.toStdString() ) )
            {
                emit Rename( newValue );
            }
        }
    }
}

void UI_Scene::SetModel( NG_Structure *p_model )
{
    m_model        = p_model;
    m_linkDrag     = 0;


    for( uint i=0; i < p_model->m_nodes.size(); i++)
    {
        UI_Node *item = new UI_Node( p_model->m_nodes[i], this );
        addItem( item );
    }

    for( auto nodeIt = p_model->m_nodes.cbegin(); nodeIt != p_model->m_nodes.cend(); ++nodeIt )
    {
        UI_Node* ui_node = this->FindUI( *nodeIt );

        for( auto slotIt = (*nodeIt)->m_inputs.cbegin(); slotIt != (*nodeIt)->m_inputs.cend(); ++slotIt )
        {
            if ( slotIt->Node )
            {
                const strID srcSlot = slotIt->Slots.src;
                const strID dstSlot = slotIt->Slots.dst;

                UI_Node* ui_srcNode = this->FindUI( slotIt->Node );

                UI_Socket* srcSocket = ui_srcNode->getOutputSocket( srcSlot );
                UI_Socket* dstSocket = ui_node->getInputSocket(  dstSlot );

                if ( srcSocket == nullptr )
                    Global::errQ( QString( "Could not resolve indexes of source socket %1.%2")
                            .arg( slotIt->Node->getName().c_str())
                            .arg( srcSlot.c_str() )
                            );
                if ( dstSocket == nullptr )
                    Global::errQ( QString( "Could not resolve indexes of destination sockets %1.%2")
                            .arg( (*nodeIt)->getName().c_str())
                            .arg( dstSlot.c_str() )
                            );

                if ( srcSocket && dstSocket )
                {
                    UI_Link* item = new UI_Link(
                            ui_srcNode, ui_node,
                            srcSocket,  dstSocket,
                            slotIt->Type );

                    addItem( item );

                    QObject::connect( item, SIGNAL( startDrag( UI_Link*, bool, const strID ) ),
                                      this, SLOT(   startDrag( UI_Link*, bool, const strID ) ) );
                }
            }
        }
    }
    m_model->Rebuild();
}

UI_Node* UI_Scene::AddNodeToModel( NG_Node* p_node )
{
    m_model->AddNode( p_node );
    UI_Node *item = new UI_Node( p_node, this );
    addItem( item );

    m_model->Rebuild();

    return item;
}

UI_Link* UI_Scene::AddLinkToModel( UI_Node *p_from, UI_Node *p_to, strID p_slotFrom, strID p_slotTo )
{
    // Check, if same link already exists.
    if ( m_model->CheckLink( p_from->node, p_to->node, p_slotFrom, p_slotTo ) )
        return nullptr;

    // Check, if this input exist;
    const NG_Target* InputTo = p_to->node->getSource( p_slotTo );
    if ( InputTo && p_from->node->getOutputIndex( p_slotFrom ) >= 0 )
    {
        // Check, if there is another link to this input
        if ( InputTo->Node )
        {
            DeleteLinkItem( p_to->node, p_slotTo );
            //p_s->node->ConnectFrom( 0, input );
        }

        if ( m_model->Link( p_from->node, p_to->node, p_slotFrom, p_slotTo ) )
        {
            UI_Socket* srcSocket = p_from->getOutputSocket( p_slotFrom );
            UI_Socket* dstSocket = p_to->getInputSocket(    p_slotTo );

            UI_Link* item = new UI_Link( p_from, p_to, srcSocket, dstSocket, InputTo->Type );

            addItem( item );

            QObject::connect( item, SIGNAL( startDrag( UI_Link*, bool, const strID ) ),
                              this, SLOT(   startDrag( UI_Link*, bool, const strID ) ) );

            m_model->Rebuild();

            return item;
        }
    }
    return nullptr;
}

bool UI_Scene::DeleteLinkFromModel( NG_Node* p_to, strID slotTo )
{
    if ( p_to )
    {
        if ( DeleteLinkItem( p_to, slotTo ) == false )
            return false;

        return m_model->Unlink( p_to, slotTo );
    }
    else
        Global::err( "UI_Scene::DeleteLinkFromModel: got nullptr instead of Node.\nSomething is busted. Save your work. Report bug.");

    return false;
}

void UI_Scene::UnLinkInModel( NG_Node* p_node )
{
    if ( p_node )
    {
        auto targets = p_node->getTargets();
        for( auto it = targets.cbegin(); it != targets.cend(); ++it )
            DeleteLinkFromModel( it->Node, it->Slots.dst );

        for( auto it = p_node->m_inputs.begin(); it != p_node->m_inputs.end(); ++it )
            if ( it->Node )
                DeleteLinkFromModel( p_node, it->Slots.dst );
    }
}

bool UI_Scene::DeleteLinkItem( NG_Node* p_to, strID slotTo, const LinkMatch endpoints )
{
    QList<QGraphicsItem*> list = items();

    if ( p_to == nullptr )
    {
        Global::err( "UI_Scene::DeleteLinkItem: got nullptr instead of Node.\nSomething is busted. Save your work. Report bug.");
        return false;
    }

    const NG_Target* source = ( endpoints == LinkMatch::ByModel ) ? p_to->getSource( slotTo ) : nullptr;

    if ( ( endpoints == LinkMatch::ByModel ) && ( source == nullptr ) )
    {
        Global::err( "UI_Scene::DeleteLinkItem: Deleting link to slot which model doesn't know about\nSomething is busted. Save your work. Report bug.");
        return false;
    }

    if ( ( endpoints == LinkMatch::ByModel ) && (source->Node == nullptr ) )
    {
        Global::err( "UI_Scene::DeleteLinkItem: The model doesn't have any links registered in the slot\nSomething is busted. Save your work. Report bug.");
        return false;
    }

    if ( endpoints == LinkMatch::ByModel )
        for( int i=0; i<list.size(); i++ )
        {
            UI_Link* link = dynamic_cast< UI_Link*>( list[i] );

            if ( link )
            {
                if ( link->m_to->node                  == p_to
                  && link->m_slotTo->slot.slotName     == slotTo
                  && link->m_from->node                == source->Node
                  && link->m_slotFrom->slot.slotName   == source->Slots.src
                   )
                {
                    removeItem( link );
                    delete link;
                    return true;
                }
            }
        }

    if ( endpoints == LinkMatch::ByTarget )
        for( int i=0; i<list.size(); i++ )
        {
            UI_Link* link = dynamic_cast< UI_Link*>( list[i] );

            if ( link )
            {
                if ( link->m_to->node                  == p_to
                  && link->m_slotTo->slot.slotName     == slotTo
                   )
                {
                    removeItem( link );
                    delete link;
                    return true;
                }
            }
        }


    if ( endpoints == LinkMatch::BySource )
        for( int i=0; i<list.size(); i++ )
        {
            UI_Link* link = dynamic_cast< UI_Link*>( list[i] );

            if ( link )
            {
                if ( link->m_from->node                  == p_to
                  && link->m_slotFrom->slot.slotName     == slotTo
                   )
                {
                    removeItem( link );
                    delete link;
                }
        }
        return true;
    }

    return false;
}

QList<QGraphicsItem *> UI_Scene::getSelected()
{
    return selectedItems();
}

void UI_Scene::onCut()
{
    Global::verbQ( QString( "Cut is not implemented yet" ) );
}

void UI_Scene::onCopy()
{
    Global::verbQ( QString( "Copy is not implemented yet" ) );
}

void UI_Scene::onPaste()
{
    Global::verbQ( QString( "Paste is not implemented yet" ) );
    m_model->Rebuild();
}

void UI_Scene::onDelete()
{
    const QList<QGraphicsItem*> items = selectedItems();

    std::vector< UI_Node* > nodes;
    for( int i=0; i<items.size(); i++ )
    {
        QGraphicsItem* item = items[ i ];
        UI_Node* ui_node = dynamic_cast< UI_Node* >( item );
        if ( ui_node )
        {
            nodes.push_back( ui_node );
        }
    }

    for( auto it = nodes.begin(); it != nodes.end(); ++it )
    {
        NG_Node* ng_node = (*it)->node;
        if ( ng_node )
        {
            UnLinkInModel( ng_node );
            removeItem( *it );
            m_model->DelNode( ng_node );
        }
    }
    m_model->Rebuild();
}

UI_Node* UI_Scene::FindUI( NG_Node* p_node)
{
    QList<QGraphicsItem*> list = items();

    for( int i=0; i<list.size(); i++ )
    {
        UI_Node* node = dynamic_cast< UI_Node* >( list[i] );
        if ( node == 0 )
            continue;
        if ( node->node == p_node )
            return node;
    }
    return 0;
}
