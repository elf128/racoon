/*
 * UI_Socket.cpp
 *
 *  Created on: May 11, 2021
 *      Author: Vlad A.
 */

#include "UI_TripleDot.h"
#include "UI_Node.h"

#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QApplication>

UI_TripleDot::UI_TripleDot( UI_Node *p_parent, QPointF p_pos) : QGraphicsItem( p_parent )
{
    //this->setFlags( );

    isSelected = false;

    setAcceptHoverEvents( true );

    setPos( p_pos );
}

QRectF UI_TripleDot::boundingRect() const
{
    return QRectF(-6, -6, 13, 13 );
}

QPainterPath UI_TripleDot::shape() const
{
    QPainterPath Path;
    Path.addRect( -6, -6, 13, 13 );

    return Path;

}

void UI_TripleDot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED( widget );

//    QBrush b = painter->brush();
    QPen   p = painter->pen();

    //painter->setPen( QColor( 0, 0, 0) );

    Global::uiColor curent = Global::uiColor( (option->state & QStyle::State_MouseOver) ? c_hovered : c_normal );

    if ( isSelected )
        curent = curent * 0.5f + Global::uiColor( c_selected );

    painter->setPen( curent );

    QPainterPath trianglePath;
    if ( isSelected )
    {
        trianglePath.moveTo( 0.0, 4.0 );
        trianglePath.lineTo( 4.0,-4.0 );
        trianglePath.lineTo(-4.0,-4.0 );
        trianglePath.closeSubpath();
    }
    else
    {
        trianglePath.addEllipse( -1.0, -5.0, 2.0, 2.0 );
        trianglePath.addEllipse(  1.0, -1.0, 2.0, 2.0 );
        trianglePath.addEllipse( -1.0,  3.0, 2.0, 2.0 );
    }

    painter->drawPath( trianglePath );

    painter->setPen( p );
    //painter->setBrush( b );
}

void UI_TripleDot::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    //QGraphicsItem::mousePressEvent(event);
    //update();
    //this->setCursor(Qt::ClosedHandCursor);
}

void UI_TripleDot::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
    /*
    if ( reportedDrag )
        return;

    const QPoint& eventPos = event->screenPos();
    const QPoint& mousePos = event->buttonDownScreenPos(Qt::LeftButton);

    if ( QLineF( eventPos, mousePos ).length() < QApplication::startDragDistance() )
        return;

    emit startDrag( this );
    qDebug( "UI_Socket: emit startDrag" );
    reportedDrag = true;
*/
}

void UI_TripleDot::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    Q_UNUSED(event);
    //QGraphicsItem::mouseReleaseEvent(event);
    //setCursor( Qt::ArrowCursor );
    emit clickTripleDot( this );
}
