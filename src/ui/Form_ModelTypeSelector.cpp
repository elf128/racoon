/*
 * ModelTypeSelector.cpp
 *
 *  Created on: Oct 18, 2021
 *      Author: vlad
 */

#include "Form_ModelTypeSelector.h"

#include "core/Global.h"

ModelTypeSelector::ModelTypeSelector( strID* p_res, QDialog* parent )
    : QDialog(parent)
{
    res = p_res;

    ui.setupUi( this );

    Global::StrList list = Global::listSystems();

    for( auto i = list.cbegin(); i != list.cend(); ++i )
        ui.types->addItem( QString( i->c_str() ) );

    if ( list.cbegin() != list.cend() )
    {
        ui.types->setCurrentItem( ui.types->item( 0 ) );
        *res = strID( ui.types->item( 0 )->text().toStdString() );
    }

    connect( ui.buttonBox, SIGNAL( accepted() ), this, SLOT( accept() ) );
    connect( ui.buttonBox, SIGNAL( rejected() ), this, SLOT( reject() ) );
    connect( ui.types,     SIGNAL( itemSelectionChanged() ), this, SLOT( onItemSelected() ) );

}

void ModelTypeSelector::onItemSelected()
{
    QListWidgetItem* item = ui.types->currentItem();
    if ( item )
    {
        QString name = item->text();
        *res = strID( name.toStdString() );
    }
}
