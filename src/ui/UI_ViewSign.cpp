/*
 * UI_Socket.cpp
 *
 *  Created on: Oct 12, 2021
 *      Author: Vlad A.
 */

#include "UI_ViewSign.h"
#include "UI_Node.h"

#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QApplication>

UI_ViewSign::UI_ViewSign( UI_Node *p_parent, QPointF p_pos) : QGraphicsItem( p_parent )
{
    //this->setFlags( );

    isSelected = false;

    setAcceptHoverEvents( true );

    setPos( p_pos );
}

QRectF UI_ViewSign::boundingRect() const
{
    return QRectF(-6, -6, 13, 13 );
}

QPainterPath UI_ViewSign::shape() const
{
    QPainterPath Path;
    Path.addRect( -6, -6, 13, 13 );

    return Path;

}

void UI_ViewSign::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED( widget );

//    QBrush b = painter->brush();
    QPen   p = painter->pen();

    //painter->setPen( QColor( 0, 0, 0) );

    Global::uiColor curent = Global::uiColor( (option->state & QStyle::State_MouseOver) ? c_hovered : c_normal );

    if ( isSelected )
        curent = curent * 0.5f + Global::uiColor( c_selected );

    painter->setPen( curent );

    QPainterPath eyePath;
    if ( isSelected )
    {
        eyePath.moveTo( 0.0, 4.0 );
        eyePath.lineTo( 4.0,-4.0 );
        eyePath.lineTo(-4.0,-4.0 );
        eyePath.closeSubpath();
    }
    else
    {
        eyePath.moveTo( 7.0, 2.0 );
        eyePath.arcTo( -28.0 + 7.0, -3.0, 28.0, 10.0,  0.0, 90.0);

        //eyePath.moveTo(-7.0,  1.5 );
        eyePath.arcTo( -7.0, -1.0 - 5.0, 28.0, 10.0, 180.0, 90.0);

        eyePath.addEllipse( -3.0, -2.0, 6.0, 6.0 );
        //eyePath.addEllipse(  1.0, -1.0, 2.0, 2.0 );
        //eyePath.addEllipse( -1.0,  3.0, 2.0, 2.0 );
    }

    painter->drawPath( eyePath );

    painter->setPen( p );
    //painter->setBrush( b );
}

void UI_ViewSign::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    //QGraphicsItem::mousePressEvent(event);
    //update();
    //this->setCursor(Qt::ClosedHandCursor);
}

void UI_ViewSign::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);

}

void UI_ViewSign::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    Q_UNUSED(event);
    //QGraphicsItem::mouseReleaseEvent(event);
    //setCursor( Qt::ArrowCursor );
    emit clickView( this );
}
