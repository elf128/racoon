/*
 * UI_Scene.h
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#ifndef SRC_UI_UI_SCENE_H_
#define SRC_UI_UI_SCENE_H_

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QObject>
#include "core/UID.h"

class UI_Node;
class UI_Link;
class UI_Socket;
class UI_LinkDrag;
class UI_TripleDot;
class UI_ViewSign;

class NG_Node;
class NG_Structure;

class UI_Scene : public QGraphicsScene
{
    Q_OBJECT
    NG_Structure *m_model;

    UI_LinkDrag* m_linkDrag;

public:
    enum class LinkMatch
    {
        ByModel,
        ByTarget,
        BySource
    };

    void          SetModel( NG_Structure *p_model );
    NG_Structure* GetModel() { return m_model; }

    UI_Node*      AddNodeToModel(      NG_Node* p_node );
    UI_Link*      AddLinkToModel(      UI_Node* p_from, UI_Node* p_to, strID slotFrom, strID slotTo );
    bool          DeleteLinkFromModel( NG_Node* p_to, strID slotTo );
    bool          DeleteLinkItem(      NG_Node* p_to, strID slotTo, const LinkMatch endpoints = LinkMatch::ByModel );
    void          UnLinkInModel(       NG_Node* p_node );
    UI_Node*      FindUI(              NG_Node* p_node );

    QList<QGraphicsItem*> getSelected();

signals:
    void showProps( UI_Node*  p_node, UI_Scene* p_model );
    void veiwMe( UI_Node* p_node, UI_Scene* p_scene );
    void Updated();
    void Rename( const QString& );

public slots:
    void startDrag(      UI_Socket *p_gui_socket, const strID p_type );
    void startDrag(      UI_Link   *p_gui_link, bool dragEnd, const strID p_type );
    void clickTripleDot( UI_TripleDot* p_node );
    void clickView(      UI_ViewSign* p_node );
    void clickTripleDot();

    void changeProp( const strID& prop, const QString& newValue );

    void onCut();
    void onCopy();
    void onPaste();
    void onDelete();

protected:
    void mouseMoveEvent(    QGraphicsSceneMouseEvent* e);
    void mouseReleaseEvent( QGraphicsSceneMouseEvent* e);
};

#endif /* SRC_UI_UI_SCENE_H_ */
