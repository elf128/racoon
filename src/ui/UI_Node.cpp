/*
 * NGNode.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#include "UI_Node.h"
#include "UI_Socket.h"
#include "UI_TripleDot.h"
#include "UI_ViewSign.h"
#include "UI_Scene.h"
#include "model/Node.h"

#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>

UI_Node::UI_Node( NG_Node *p_node, UI_Scene* p_scene )
{
    node = p_node;

    setFlags( ItemIsSelectable | ItemIsMovable );
    setAcceptHoverEvents( true );

    setPos( QPointF( node->m_x,
                     node->m_y ) );

    QPointF rightMarkerPos ( (  ( UI_Node::c_width - UI_Node::c_height )/ 2 ), UI_Node::c_height / 2 );
    QPointF leftMarkerPos  ( (- ( UI_Node::c_width - UI_Node::c_height )/ 2 ), UI_Node::c_height / 2 );

    propMarker = new UI_TripleDot(  this, rightMarkerPos );
    viewMarker = new UI_ViewSign(   this, leftMarkerPos );

    QObject::connect( propMarker, SIGNAL( clickTripleDot( UI_TripleDot* )),
                      p_scene,    SLOT(   clickTripleDot( UI_TripleDot* ) ));

    QObject::connect( viewMarker, SIGNAL( clickView( UI_ViewSign* )),
                      p_scene,    SLOT(   clickView( UI_ViewSign* ) ));

    SyncSockets( p_scene );
}

UI_Node::~UI_Node()
{
    const uint inSlotCount =  this->inputs.size();
    for( uint i = 0; i<= inSlotCount; i++)
        delete this->inputs[ i ];

    const uint outSlotCount =  this->outputs.size();
    for( uint i = 0; i<= outSlotCount; i++)
        delete this->outputs[ i ];

    delete viewMarker;
    delete propMarker;
}

int UI_Node::getTotalInputCount() const
{
    if ( node )
        return node->getTotalInputCount();
    else
        return 0;
}

int UI_Node::getTotalOutputCount() const
{
    if ( node )
        return node->getTotalOutputCount();
    else
        return 0;
}

void UI_Node::SyncSockets( UI_Scene* p_scene )
{
    if ( p_scene == nullptr )
    {
        Global::err( "UI_Node cannot find the scene it belong to. Cannot synchronize sockets." );
        Global::err( "Chances are, what you seen is not what stored in memory.");
        Global::err( "Save your work into separate file. Crashing ahead! Report to devs.");
        return;
    }
    const uint inputCountNative  = node->getNativeInputCount();
    const uint outputCountNative = node->getNativeOutputCount();
    const uint inputCountTotal   = node->getTotalInputCount();
    const uint outputCountTotal  = node->getTotalOutputCount();

    // Cleaning phase.
    std::vector<UI_Socket* >::iterator it;

    it = inputs.begin();
    while( it != inputs.end() )
    {
        const strID slotName = (*it)->slot.slotName;
        const int idx = node->getInputIndex( slotName );
        if ( idx < 0 || ( (*it)->slot.dataType != node->m_inputs[ idx ].Type ) )
        {
            // Socket is missing. Gotta clean it up.
            p_scene->DeleteLinkItem( node, slotName, UI_Scene::LinkMatch::ByTarget );
            inputs.erase( it );
        }
        else
        {
            (*it)->slot.cachedIdx = idx;
            ++it;
        }
    }

    it = outputs.begin();
    while( it != outputs.end() )
    {
        const strID slotName = (*it)->slot.slotName;
        const int idx = node->getOutputIndex( slotName );
        if ( idx < 0 || ( (*it)->slot.dataType != node->m_outputs[ idx ].Type ) )
        {
            // Socket is missing. Gotta clean it up.
            p_scene->DeleteLinkItem( node, slotName, UI_Scene::LinkMatch::BySource );
            outputs.erase( it );
        }
        else
        {
            (*it)->slot.cachedIdx = idx;
            ++it;
        }
    }

    // Sorting sockets, in case node have reordered them
    std::sort( inputs.begin(), inputs.end(), [](const UI_Socket* d1, const UI_Socket* d2)
            {
                return ( d1->slot.cachedIdx < d2->slot.cachedIdx );
            } );

    std::sort( outputs.begin(), outputs.end(), [](const UI_Socket* d1, const UI_Socket* d2)
            {
                return ( d1->slot.cachedIdx < d2->slot.cachedIdx );
            } );

    // Adding phase
    it = inputs.begin();
    for( uint i = 0; i < inputCountTotal; i++)
    {
        const strID slotName = node->m_inputs[ i ].Slots.dst;
        const strID slotType = node->m_inputs[ i ].Type;

        if ( it == inputs.end() || (*it)->slot.slotName != slotName )
        {
            UI_Socket::SlotId slot;
            slot.slotName  = slotName;
            slot.dataType  = slotType;
            slot.cachedIdx = i;
            slot.type      = ( i < inputCountNative ) ? UI_Socket::SlotType::Input : UI_Socket::SlotType::Param;

            UI_Socket* socket = new UI_Socket( this, slot );

            it = inputs.insert( it, socket );

            QObject::connect( socket,  SIGNAL( startDrag( UI_Socket*, const strID ) ),
                              p_scene, SLOT(   startDrag( UI_Socket*, const strID ) ) );
        }
        else
            (*it)->resolveCachedIdx();

        ++it;
    }

    it = outputs.begin();
    for( uint i = 0; i < outputCountTotal; i++)
    {
        const strID slotName = node->m_outputs[ i ].Name;
        const strID slotType = node->m_outputs[ i ].Type;

        if (  it == outputs.end() || (*it)->slot.slotName != slotName )
        {
            UI_Socket::SlotId slot;
            slot.slotName  = slotName;
            slot.dataType  = slotType;
            slot.cachedIdx = i;
            slot.type = ( i < outputCountNative ) ? UI_Socket::SlotType::Output : UI_Socket::SlotType::Prop;

            UI_Socket* socket = new UI_Socket( this, slot );

            it = outputs.insert( it, socket );

            QObject::connect( socket,  SIGNAL( startDrag( UI_Socket*, const strID ) ),
                              p_scene, SLOT(   startDrag( UI_Socket*, const strID ) ) );
        }
        else
            (*it)->resolveCachedIdx();

        ++it;
    }
}

UI_Socket* UI_Node::getInputSocket( const strID slot )
{
    for( auto it = inputs.begin(); it != inputs.end(); ++it )
        if ( (*it)->slot.slotName == slot )
            return (*it);

    return nullptr;
}

UI_Socket* UI_Node::getOutputSocket( const strID slot )
{
    for( auto it = outputs.begin(); it != outputs.end(); ++it )
        if ( (*it)->slot.slotName == slot )
            return (*it);

    return nullptr;
}

QRectF UI_Node::boundingRect() const
{
    return QRectF( -( UI_Node::c_width / 2 ), 0,
                      UI_Node::c_width,       UI_Node::c_height );
}

QPainterPath UI_Node::shape() const
{
    QPainterPath path;
    path.addRect( -( UI_Node::c_width / 2 ), 0,
                     UI_Node::c_width,       UI_Node::c_height );
    return path;
}

void UI_Node::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget )
{
    Q_UNUSED(widget);

    QBrush b = painter->brush();

    painter->setPen( QColor( 0, 0, 0) );

    QColor fillColor     = node->m_color;
    QColor selectedColor = node->m_color * 1.2f + Global::uiColor( 0x003F00 );
    QColor hoveredColor  = node->m_color * 1.1f + Global::uiColor( 0x0F0F0F );

    Global::uiColor outlineColor     = Global::uiColor( UI_Node::c_normOut  );
    Global::uiColor outlineHighlite  = Global::uiColor( UI_Node::c_highOut  );

    if (option->state & QStyle::State_MouseOver)
    {
        fillColor = hoveredColor;
        outlineColor = outlineHighlite;
    }
    else
        if (option->state & QStyle::State_Selected)
            fillColor = selectedColor;

    const float c_left     = -UI_Node::c_width  / 2;
    const float c_right    =  UI_Node::c_width  / 2;
    const float c_top      =  0.0f;
    const float c_bottom   =  UI_Node::c_height;
    const float c_half     =  UI_Node::c_height / 2;

    const float c_leftside  = c_left  + 8.0;
    const float c_rightside = c_right - 8.0;


    QPainterPath roundRectPath;
    roundRectPath.moveTo( c_leftside, c_top);
    roundRectPath.arcTo(  c_left, c_top, c_half, c_bottom, 90.0, 180.0);
    //roundRectPath.lineTo( 0.0,  8.0);
    roundRectPath.arcTo(  c_rightside, c_top, c_half, c_bottom, 270.0, 180.0);
    roundRectPath.lineTo( c_leftside, 0.0);
    roundRectPath.closeSubpath();

    QPainterPath trianglePath;
    trianglePath.moveTo( 0.0, 4.0 );
    trianglePath.lineTo( 4.0,-4.0 );
    trianglePath.lineTo(-4.0,-4.0 );
    trianglePath.closeSubpath();

    painter->setPen( outlineColor );
    painter->setBrush( fillColor );
    painter->drawPath( roundRectPath );

    painter->setPen( QColor( 255, 255, 255 ) );
    QFont font( "Times", 10 );
    font.setStyleStrategy( QFont::ForceOutline );
    painter->setFont( font );
    painter->drawText( c_leftside + UI_Node::c_leftIconOffset, 12.0, QString( this->node->m_name.c_str() ) );

    painter->setBrush(b);
}

void UI_Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    update();
}

void UI_Node::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
/*    if (event->modifiers() & Qt::ShiftModifier) {
        stuff << event->pos();
        update();
        return;
    }*/
    QGraphicsItem::mouseMoveEvent(event);
    const QPointF pos = this->pos();
    if ( this->node->m_x != pos.x() || this->node->m_y != pos.y() )
    {
        this->node->m_x = this->x();
        this->node->m_y = this->y();
        emit Moved( this );
    }

}

void UI_Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);

    //const QPointF pos = this->pos();

    // ToDo: Probably old code.

    if (this->isSelected())
    {
        if ( event->modifiers() & Qt::ShiftModifier )
            emit MonitorClicked( this->node );
        else
            emit ViewClicked( this->node );
    }
    update();
}

void UI_Node::selectedNode( UI_Node* p_node )
{
    if ( propMarker )
        propMarker->setIsSelected( p_node == this );

    update();

    if ( p_node )
        Global::verbQ( QString( "Node %1 got selectedNode %2" )
                .arg( node->getName().c_str() )
                .arg( p_node->node->getName().c_str() ) );
    else
        Global::verbQ( QString( "Node %1 got selectedNode nullptr" )
                .arg( node->getName().c_str() ) );

}


void UI_Node::viewedNode( UI_Node* p_node )
{
    if ( viewMarker )
        viewMarker->setIsSelected( p_node == this );

    update();

    if ( p_node )
        Global::verbQ( QString( "Node %1 got viewedNode %2" )
                .arg( node->getName().c_str() )
                .arg( p_node->node->getName().c_str() ) );
    else
        Global::verbQ( QString( "Node %1 got viewedNode nullptr" )
                .arg( node->getName().c_str() ) );

}

void UI_Node::expose( const strID slot )
{
    Global::verbQ( QString( "Node %1 should expose %2" )
                    .arg( node->getName().c_str() )
                    .arg( slot.c_str() ) );

    if ( node )
    {
        node->ExposeParam( slot );
        SyncSockets( dynamic_cast< UI_Scene* >( scene() ) );
    }
}

void UI_Node::colapse( const strID slot )
{
    Global::verbQ( QString( "Node %1 should collapse %2" )
                    .arg( node->getName().c_str() )
                    .arg( slot.c_str() ) );
    if ( node )
    {
        node->CollapseParam( slot );
        SyncSockets( dynamic_cast< UI_Scene* >( scene() ) );
    }
}
