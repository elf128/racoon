/*
 * UI_Socket.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#include "UI_Socket.h"
#include "UI_Node.h"
#include "core/Global.h"

#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QApplication>

UI_Socket::UI_Socket( UI_Node *p_parent, SlotId p_slot/*, QPointF p_pos*/ ) : QGraphicsItem( p_parent )
{
    //this->setFlags( );

    setAcceptHoverEvents( true );

    slot = p_slot;

    resolveCachedIdx();

    reportedDrag = false;
}

QRectF UI_Socket::boundingRect() const
{
    return QRectF(-6, -6, 13, 13 );
}

QPainterPath UI_Socket::shape() const
{
    QPainterPath Path;
    Path.addRect( -6, -6, 13, 13 );

    return Path;

}

void UI_Socket::resolveCachedIdx()
{
    float posX = calcX();
    float posY = calcY();
    setPos( QPointF(posX, posY ) );
}

float UI_Socket::calcX() const
{
    UI_Node* node = dynamic_cast< UI_Node* >( parentItem() );
    if ( node )
    {
        int slotCount = (( slot.type == SlotType::Input ) || ( slot.type == SlotType::Param ) )
                ? node->getTotalInputCount()
                : node->getTotalOutputCount();

        float step = UI_Node::c_width / ( float( slotCount ) + 1.0f );

        return -( UI_Node::c_width / 2.0 ) + step * ( slot.cachedIdx + 1 );
    }
    else
        return 0;

}

float UI_Socket::calcY() const
{
    return (( slot.type == SlotType::Input ) || ( slot.type == SlotType::Param ) ) ? -UI_Node::c_slotOffset : UI_Node::c_height + UI_Node::c_slotOffset;
}

void UI_Socket::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED( widget );

    QBrush b = painter->brush();
    QPen   p = painter->pen();

    painter->setPen( QColor( 0, 0, 0) );

    Global::uiColor fillColor = ( slot.type == UI_Socket::SlotType::Input )  ? Global::uiColor( UI_Node::c_inColor  )  : (
                                ( slot.type == UI_Socket::SlotType::Output ) ? Global::uiColor( UI_Node::c_outColor  ) : (
                                ( slot.type == UI_Socket::SlotType::Param )  ? Global::uiColor( UI_Node::c_paramColor  ) : (
                                ( slot.type == UI_Socket::SlotType::Prop )   ? Global::uiColor( UI_Node::c_proptColor  ) : (
                                     Global::uiColor( 0x0F0F0F )       ))));

    Global::uiColor selectedColor = fillColor * 1.2f + Global::uiColor( 0x003F00 );
    Global::uiColor hoveredColor  = fillColor * 1.1f + Global::uiColor( 0x0F0F0F );
    Global::uiColor outlineColor     = Global::uiColor( UI_Node::c_normOut  );
    Global::uiColor outlineHighlite  = Global::uiColor( UI_Node::c_highOut  );

    if (option->state & QStyle::State_MouseOver)
    {
        outlineColor = outlineHighlite;
        fillColor = hoveredColor;
    }

    QPainterPath trianglePath;
    trianglePath.moveTo( 0.0, 4.0 );
    trianglePath.lineTo( 4.0,-4.0 );
    trianglePath.lineTo(-4.0,-4.0 );
    trianglePath.closeSubpath();

    painter->setPen( QColor( outlineColor ) );
    painter->setBrush( QColor( fillColor ) );
    painter->drawPath( trianglePath );

    painter->setPen( p );
    painter->setBrush( b );
}

void UI_Socket::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    //QGraphicsItem::mousePressEvent(event);
    //update();
    this->setCursor(Qt::ClosedHandCursor);
}

void UI_Socket::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    //QGraphicsItem::mouseMoveEvent(event);
    if ( reportedDrag )
        return;

    const QPoint& eventPos = event->screenPos();
    const QPoint& mousePos = event->buttonDownScreenPos(Qt::LeftButton);

    if ( QLineF( eventPos, mousePos ).length() < QApplication::startDragDistance() )
        return;

    emit startDrag( this, slot.dataType );
    qDebug( "UI_Socket: emit startDrag" );
    reportedDrag = true;

}

void UI_Socket::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    //QGraphicsItem::mouseReleaseEvent(event);
    setCursor( Qt::ArrowCursor );
    reportedDrag = false;
}
