/*
 * ModelTypeSelector.h
 *
 *  Created on: Oct 18, 2021
 *      Author: vlad
 */

#ifndef SRC_UI_FORM_MODELTYPESELECTOR_H_
#define SRC_UI_FORM_MODELTYPESELECTOR_H_

#include <QDialog>
#include "ui_ModelTypeSelector.h"

class strID;

class ModelTypeSelector : public QDialog
{
    Q_OBJECT
public:
    explicit ModelTypeSelector( strID* p_res, QDialog* parent = nullptr );

private slots:
    void onItemSelected();

private:
    Ui::ModelTypeSelector ui;
    strID*                res;
};

#endif /* SRC_UI_FORM_MODELTYPESELECTOR_H_ */
