/*
 * UI_Canvas.h
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_UI_UI_CANVAS_H_
#define SRC_UI_UI_CANVAS_H_

//#include <QGLWidget>
#include <QtGui>
#include <QGraphicsView>
#include <QMap>

#include "ui/UI_Node.h"
#include "ui/UI_Link.h"
#include "ui/UI_Socket.h"

class UI_Scene;
class UI_NodeGraph;
class NG_Structure;

class UI_Canvas : public QGraphicsView
{
    Q_OBJECT

    UI_Scene     *scene;
public:
    UI_Canvas(QWidget *parent = 0);
    virtual ~UI_Canvas() {}

    UI_Scene* SetModel( NG_Structure* p_model, QWidget* p_props, UI_NodeGraph* widget, QWidget* p_viewer );

protected:
//    void paintEvent(QPaintEvent *event);
    void wheelEvent(QWheelEvent *e);
    void mouseReleaseEvent( QMouseEvent* e );

signals:
    void NodeAnimated( NG_Node* p_node );

public slots:
    void zoomIn(int level = 1);
    void zoomOut(int level = 1);

private slots:
    void resetView();
    void setupMatrix();
    void togglePointerMode();

};

#endif /* SRC_UI_UI_CANVAS_H_ */
