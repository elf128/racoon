/*
 * UI_NodeGraph.h
 *
 *  Created on: Feb 21, 2021
 *      Author: vlad
 */

#ifndef SRC_UI_UI_NODEGRAPH_H_
#define SRC_UI_UI_NODEGRAPH_H_

#include <QWidget>
#include "model/Structure.h"

QT_BEGIN_NAMESPACE
namespace Ui { class UI_NodeGraph; }
QT_END_NAMESPACE

namespace ads { class CDockWidget; }

class QToolBar;
class QAction;
class UI_Scene;

class UI_NodeGraph: public QWidget
{
    Q_OBJECT
public:
    UI_NodeGraph( NG_Structure* p_model, ads::CDockWidget* widget, QWidget* p_viewer, QWidget* parent = nullptr );
    virtual ~UI_NodeGraph();

    //void setModel( const QString filename, NG_Structure* model );
    void setFilename( const QString& filename ) { m_filename = filename; }
    UI_Scene* getScene() { return m_scene; }

    bool SaveTo( const QString filename );
    bool Save();

    void SetupToolbar( const strID& type );

signals:
    void clickTripleDot();

public slots:
    void ShowProps();
    void Rename( const QString& name );
    void onToolBar( QAction* );
    void onCut();
    void onCopy();
    void onPaste();
    void onDelete();

    void onStop();
    void onStep();
    void onPlay();

private:
    Ui::UI_NodeGraph* ui;
    NG_Structure*     m_model;
    UI_Scene*         m_scene;
    QString           m_filename;
    ads::CDockWidget* container;

    std::map<QAction*, strID > m_actionMap;
};

#endif /* SRC_UI_UI_NODEGRAPH_H_ */
