/*
 * NGLink.h
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#ifndef SRC_UI_UI_LINKDRAG_H_
#define SRC_UI_UI_LINKDRAG_H_

#include <qgraphicsitem.h>
#include "core/UID.h"

class UI_Node;
class UI_Socket;
class UI_Link;

class UI_LinkDrag : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:

    enum class LinkType
    {
        NewOutToIn,
        NewInToOut,
        OldDragEnd,
        OldDragStart,
    };

    UI_LinkDrag( const QPointF& p_origin, const LinkType type, const strID p_dataType );

    QRectF boundingRect() const;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setSnap( const QPointF& p_pos );
    void unSnap();

    void       setOrigin(  UI_Socket* origin ) { m_drag_origin = origin; }
    void       setTarget(  UI_Socket* target ) { m_drag_target = target; }
    void       setOldLink( UI_Link* link )     { m_oldLink = link; }

    UI_Socket* getOrigin()   const { return m_drag_origin; }
    UI_Socket* getTarget()   const { return m_drag_target; }
    UI_Link*   getOldLink()  const { return m_oldLink; }
    LinkType   getLinkType() const { return m_type; }
    bool       isLockedOnDst() const { return ( m_type == LinkType::NewInToOut) || ( m_type == LinkType::OldDragStart ); }
    bool       isNewLink() const     { return ( m_type == LinkType::NewInToOut) || ( m_type == LinkType::NewOutToIn ); }
    inline const strID getDataType() const { return m_dataType; }

private:
    LinkType     m_type;
    const strID  m_dataType;
    QPointF      m_origin;
    bool         m_isSnapped;
    QPointF      m_snapPoint;
    UI_Socket*   m_drag_origin;
    UI_Socket*   m_drag_target;
    UI_Link*     m_oldLink;

public slots:
    void OnDrag( QGraphicsSceneMouseEvent *event );

signals:

};

#endif /* SRC_UI_UI_LINKDRAG_H_ */
