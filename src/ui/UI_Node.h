/*
 * NGNode.h
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_UI_NODE_H_
#define SRC_UI_NODE_H_

#include <QGraphicsItem>
#include <vector>
#include <qgraphicsitem.h>

class strID;
class NG_Node;
class UI_Scene;
class UI_Socket;
class UI_TripleDot;
class UI_ViewSign;

class UI_Node: public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    static const int c_width          = 100;
    static const int c_height         = 16;
    static const int c_slotOffset     = 2;
    static const int c_leftIconOffset = 10;
    static const uint c_inColor       = 0x208020;
    static const uint c_outColor      = 0x802020;
    static const uint c_paramColor    = 0x205030;
    static const uint c_proptColor    = 0x502030;
    static const uint c_normOut       = 0x000000;
    static const uint c_highOut       = 0x00FF40;

    struct Settings
    {
        int32_t viewOutputIdx = 0;
        int32_t viewModeIdx   = -1;
    };

    UI_Node( NG_Node *p_node, UI_Scene* p_scene );
    virtual ~UI_Node();

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;

protected:
    void mousePressEvent(   QGraphicsSceneMouseEvent *event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) override;
    void mouseMoveEvent(    QGraphicsSceneMouseEvent *event ) override;

    void expose( const strID slot );
    void colapse( const strID slot );

    int getTotalInputCount() const;
    int getTotalOutputCount() const;
    UI_Socket* getInputSocket( const strID slot );
    UI_Socket* getOutputSocket( const strID slot );

    void SyncSockets( UI_Scene* p_scene );
//private:
    std::vector<UI_Socket* > inputs;
    std::vector<UI_Socket* > outputs;
    UI_TripleDot*            propMarker;
    UI_ViewSign*             viewMarker;
    NG_Node*                 node;
    Settings                 settings;

public slots:
    void selectedNode( UI_Node* p_node );
    void viewedNode( UI_Node* p_node );

signals:
    void MonitorClicked( NG_Node* p_node );
    void ViewClicked( NG_Node* p_node );
    void Moved( UI_Node* p_node );
    void Updated();

    friend class UI_Link;
    friend class UI_Socket;
    friend class UI_Scene;
    friend class UI_Props;
    friend class UI_Viewer;
};

#endif /* SRC_UI_NODE_H_ */
