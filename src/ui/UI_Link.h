/*
 * NGLink.h
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#ifndef SRC_UI_UI_LINK_H_
#define SRC_UI_UI_LINK_H_

#include "core/UID.h"
#include <QGraphicsItem>

class UI_Node;
class UI_Socket;

class UI_Link: public QObject, public QGraphicsItem
{
    Q_OBJECT
    bool    reportedDrag;
    float   m_dirX;
    float   m_dirY;
public:
    UI_Link( UI_Node *p_from, UI_Node *p_to, UI_Socket* p_slotFrom, UI_Socket* p_slotTo, const strID p_type );

    virtual ~UI_Link() {}

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    void mousePressEvent(   QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(    QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event);

public:
    UI_Node*    m_from;
    UI_Node*    m_to;
    UI_Socket*  m_slotFrom;
    UI_Socket*  m_slotTo;
    const strID m_dataType;

    const QPointF getLinkOriginPos() const;

public slots:
    void OnNodeMove( UI_Node* p_node );

signals:
    void startDrag( UI_Link* self, bool dragStart, const strID type );

    friend class UI_Node;
};

#endif /* SRC_UI_UI_LINK_H_ */
