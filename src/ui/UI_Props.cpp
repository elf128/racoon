/*
 * UI_Props.cpp
 *
 *  Created on: May 11, 2021
 *      Author: vlad
 */

#include "UI_Props.h"
#include "model/Node.h"
#include "UI_Node.h"
#include "UI_Scene.h"
#include "model/Structure.h"

#include <QDebug>
#include <QValidator>

QLineEdit* FileEditFactoryPrivate::createEditor( QtProperty* property, QWidget*parent )
{
    QLineEdit* editor = new QLineEdit( parent );
    initializeEditor( property, editor );
    return editor;
}

void FileEditFactoryPrivate::initializeEditor( QtProperty* property, QLineEdit* editor)
{
    typename PropertyToEditorListMap::iterator it = m_createdEditors.find(property);

    if (it == m_createdEditors.end())
        it = m_createdEditors.insert(property, EditorList());

    it.value().append(editor);
    m_editorToProperty.insert(editor, property);
}

void FileEditFactoryPrivate::slotEditorDestroyed(QObject *object)
{
    const typename EditorToPropertyMap::iterator ecend = m_editorToProperty.end();

    for (typename EditorToPropertyMap::iterator itEditor = m_editorToProperty.begin(); itEditor !=  ecend; ++itEditor)
    {
        if (itEditor.key() == object)
        {
            QLineEdit* editor = itEditor.key();
            QtProperty* property = itEditor.value();
            const typename PropertyToEditorListMap::iterator pit = m_createdEditors.find( property );
            if ( pit != m_createdEditors.end() )
            {
                pit.value().removeAll(editor);
                if (pit.value().empty())
                    m_createdEditors.erase(pit);
            }
            m_editorToProperty.erase(itEditor);
            return;
        }
    }
}


void FileEditFactoryPrivate::slotPropertyChanged(QtProperty *property,
                const QString &value)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QLineEdit *editor : it.value()) {
        if (editor->text() != value)
            editor->setText(value);
    }
}

void FileEditFactoryPrivate::slotRegExpChanged(QtProperty *property,
            const QRegExp &regExp)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    QtStringPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QLineEdit *editor : it.value()) {
        editor->blockSignals(true);
        const QValidator *oldValidator = editor->validator();
        QValidator *newValidator = 0;
        if (regExp.isValid()) {
            newValidator = new QRegExpValidator(regExp, editor);
        }
        editor->setValidator(newValidator);
        if (oldValidator)
            delete oldValidator;
        editor->blockSignals(false);
    }
}

void FileEditFactoryPrivate::slotSetValue(const QString &value)
{
    QObject *object = q_ptr->sender();

    const QMap<QLineEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QLineEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object)
        {
            QtProperty *property = itEditor.value();
            QtStringPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

FileEditFactory::FileEditFactory(QObject *parent)
    : QtAbstractEditorFactory<QtStringPropertyManager>(parent), d_ptr( new FileEditFactoryPrivate() )
{
    d_ptr->q_ptr = this;

}

FileEditFactory::~FileEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
}

void FileEditFactory::connectPropertyManager(QtStringPropertyManager *manager)
{
    connect( manager, SIGNAL( valueChanged(       QtProperty*,QString ) ),
                this, SLOT(   slotPropertyChanged(QtProperty*,QString ) ) );
    connect( manager, SIGNAL( regExpChanged(      QtProperty*,QRegExp ) ),
                this, SLOT(   slotRegExpChanged(  QtProperty*,QRegExp ) ) );
}

QWidget *FileEditFactory::createEditor(QtStringPropertyManager *manager,
        QtProperty *property, QWidget *parent)
{

    QLineEdit *editor = d_ptr->createEditor(property, parent);
    QRegExp regExp = manager->regExp(property);
    if (regExp.isValid()) {
        QValidator *validator = new QRegExpValidator(regExp, editor);
        editor->setValidator(validator);
    }
    editor->setText(manager->value(property));

    connect(editor, SIGNAL(textEdited(QString)),
                this, SLOT(slotSetValue(QString)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

void FileEditFactory::disconnectPropertyManager(QtStringPropertyManager *manager)
{
    disconnect( manager, SIGNAL( valueChanged(        QtProperty*,QString ) ),
                this,    SLOT(   slotPropertyChanged( QtProperty*,QString ) ) );
    disconnect( manager, SIGNAL( regExpChanged(       QtProperty*,QRegExp ) ),
                this,    SLOT(   slotRegExpChanged(   QtProperty*,QRegExp ) ) );
}

QtStructPropertyManager::QtStructPropertyManager(QObject *parent)
    : QtAbstractPropertyManager(parent) {}

QtStructPropertyManager::~QtStructPropertyManager() {}

void QtStructPropertyManager::initializeProperty(QtProperty *property)
{
    (void)property;
}

void QtStructPropertyManager::uninitializeProperty(QtProperty *property)
{
    (void)property;
}

QString QtStructPropertyManager::valueText(const QtProperty *property) const
{
    (void)property;
    return QString("[...]");
}


UI_Props::UI_Props( QWidget* parent  )
    : QtTreePropertyBrowser( parent )
{
    m_selectedNode = nullptr;
    m_scene        = nullptr;
    m_modelMode    = false;
    setFactoryForManager( &m_stringManager, &m_lineeditFactory );
    setFactoryForManager( &m_stringManager, &m_fileeditFactory );
    setFactoryForManager( &m_intManager,    &m_spinboxFactory );
    setFactoryForManager( &m_doubleManager, &m_doubleSpinboxFactory );
    setFactoryForManager( &m_boolManager,   &m_checkFactory );

    m_managerTypes[ int(  AnyType::Types::Empty     ) ] = ManagerType::None;
    m_managerTypes[ int(  AnyType::Types::Int       ) ] = ManagerType::Int;
    m_managerTypes[ int(  AnyType::Types::Count     ) ] = ManagerType::UInt;
    m_managerTypes[ int(  AnyType::Types::String    ) ] = ManagerType::String;
    m_managerTypes[ int(  AnyType::Types::Filename  ) ] = ManagerType::String;
    m_managerTypes[ int(  AnyType::Types::Number    ) ] = ManagerType::Double;
    m_managerTypes[ int(  AnyType::Types::Vec2      ) ] = ManagerType::Double;
    m_managerTypes[ int(  AnyType::Types::Vec3      ) ] = ManagerType::Double;
    m_managerTypes[ int(  AnyType::Types::Vec4      ) ] = ManagerType::Double;
    m_managerTypes[ int(  AnyType::Types::IVec2     ) ] = ManagerType::Int;
    m_managerTypes[ int(  AnyType::Types::IVec3     ) ] = ManagerType::Int;
    m_managerTypes[ int(  AnyType::Types::IVec4     ) ] = ManagerType::Int;
    m_managerTypes[ int(  AnyType::Types::UVec2     ) ] = ManagerType::UInt;
    m_managerTypes[ int(  AnyType::Types::UVec3     ) ] = ManagerType::UInt;
    m_managerTypes[ int(  AnyType::Types::UVec4     ) ] = ManagerType::UInt;
    m_managerTypes[ int(  AnyType::Types::Vector    ) ] = ManagerType::Double;
    m_managerTypes[ int(  AnyType::Types::VectorI   ) ] = ManagerType::Int;
    m_managerTypes[ int(  AnyType::Types::VectorU   ) ] = ManagerType::UInt;
    m_managerTypes[ int(  AnyType::Types::StrList   ) ] = ManagerType::String;
    m_managerTypes[ int(  AnyType::Types::Image     ) ] = ManagerType::None;
    m_managerTypes[ int(  AnyType::Types::TensorCPU ) ] = ManagerType::None;

}

void UI_Props::showProps( UI_Node* p_node, UI_Scene* p_scene )
{
    if ( p_node )
        Global::verbQ( QString( "showProps( %1 )" ).arg( p_node->node->m_name.c_str() ) );

    else if ( p_scene )
        Global::verbQ( QString( "showProps( %1 )" ).arg( p_scene->GetModel()->m_name.c_str() ) );
    else
        Global::verbQ( QString( "hideProps()" ) );

    if ( p_node )
        QObject::connect( this,   SIGNAL( selectedNode( UI_Node* ) ),
                          p_node, SLOT(   selectedNode( UI_Node* ) ));

    emit selectedNode( p_node );

    if ( m_selectedNode )
        disconnect( m_selectedNode );

    if ( m_scene )
        QObject::disconnect( m_scene,        SIGNAL( Updated() ),
                             this,           SLOT(   objectUpdated()  ));

    m_selectedNode = p_node;
    m_scene        = p_scene;
    m_modelMode    = ( p_node == nullptr );

    clear();
    m_groupManager.clear();
    m_structManager.clear();
    m_stringManager.clear();
    m_intManager.clear();
    m_doubleManager.clear();
    m_boolManager.clear();

    m_properties.clear();

    NG_Node*      node  = ( p_node != nullptr ) ? p_node->node : nullptr;
    NG_Structure* model = ( p_scene ) ? p_scene->GetModel() : nullptr;

    if ( node || model )
    {
        QtProperty* type = m_stringManager.addProperty( "Type" );
        QtProperty* name = m_stringManager.addProperty( "Name" );

        type->setEnabled( false );
        name->setEnabled( true );

        m_properties.push_back( PropID( strID("Type"), type, 0 ) );
        m_properties.push_back( PropID( strID("Name"), name, 0 ) );

        addProperty( type );
        addProperty( name );

        const std::vector< Global::Storage >& storage = ( node ) ? node->m_storage : model->m_storage;

        QtProperty* params = m_groupManager.addProperty( QString ( "Parameters" ) );
        QtProperty* props  = m_groupManager.addProperty( QString ( "Properties" ) );

        setPropertiesWithoutValueMarked( true );

        _addParamProperties( params, storage );

        addProperty( params );
        addProperty( props );

        QtBrowserItem *item = topLevelItem( params );
        setBackgroundColor( item, QColor( 192, 240, 192 ) );
        if ( node )
            QObject::connect( m_selectedNode, SIGNAL( Updated() ),
                              this,           SLOT(   objectUpdated()  ));

        if ( model )
            QObject::connect( m_scene,        SIGNAL( Updated() ),
                              this,           SLOT(   objectUpdated()  ));
    }

    objectUpdated();
}

void UI_Props::valueChanged( QtProperty *property, bool value )    { _valueChanged( property, value, 0.0f, QString() ); }
void UI_Props::valueChanged( QtProperty *property, int value )     { _valueChanged( property, value, 0.0f, QString() ); }
void UI_Props::valueChanged( QtProperty *property, double value )  { _valueChanged( property, 0, value, QString() ); }
void UI_Props::valueChanged( QtProperty *property, const QString &value ) { _valueChanged( property, 0, 0.0f, value ); }

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void UI_Props::_valueChanged( QtProperty* property, const int intVal, const double doubleVal, const QString& strVal )
{
//    Global::verbQ( QString( "UI_Props::_valueChanged( %1, %2, %3, %4 )" )
//                        .arg( ulong((void*)property ) )
//                        .arg( intVal )
//                        .arg( doubleVal )
//                        .arg( strVal ) );

    const PropID* propID = nullptr;
    for( auto it = m_properties.cbegin(); it != m_properties.cend(); ++it )
        if ( it->prop == property )
        {
            propID = &(*it);
            break;
        }

    if ( propID == nullptr )
        return;

//    Global::verbQ( QString( "    .%1 [ %2 ];" )
//                        .arg( propID->name.c_str() )
//                        .arg( propID->idx ));

    if ( !m_modelMode )
    {
        if ( m_scene && m_scene->GetModel() && m_selectedNode && m_selectedNode->node )
        {
            if ( propID->name == strID( "Name" ) )
            {
                if ( m_scene->GetModel()->Rename( m_selectedNode->node, strVal.toStdString() ) )
                    m_selectedNode->update();
            }
            else
            {
                if ( propID->idx >= 0 )
                {
                    std::lock_guard< std::recursive_mutex > lock( m_scene->GetModel()->m_lock );
                    Global::Storage* propStorage = m_selectedNode->node->storageLookupModify( propID->name );
                    if ( propStorage )
                    {
                        AnyType& prop = propStorage->data;

                        ManagerType manager = m_managerTypes[ int( prop.getType() ) ];

                        if ( manager != ManagerType::None )
                        {
                            const int dataSize  = AnyType::getSize( prop.getType() );

                            if ( dataSize == 1 )
                                switch( manager )
                                {
                                    case ManagerType::Int:    prop.unsafeValueInt()    = int32_t( intVal );    break;
                                    case ManagerType::UInt:   prop.unsafeValueUInt()   = uint32_t( ( intVal > 0 )? intVal : 0 );   break;
                                    case ManagerType::Double: prop.unsafeValueNumber() = float( doubleVal );   break;
                                    case ManagerType::String: prop.unsafeString()      = strVal.toStdString(); break;
                                    default: break;
                                }
                            if ( dataSize > 1 )
                                switch( manager )
                                {
                                    case ManagerType::Int:    prop.unsafeArrayInt( propID->idx )    = int32_t( intVal );  break;
                                    case ManagerType::UInt:   prop.unsafeArrayUInt( propID->idx )   = uint32_t( ( intVal > 0 )? intVal : 0 ); break;
                                    case ManagerType::Double: prop.unsafeArrayNumber( propID->idx ) = float( doubleVal ); break;
                                    default: break;
                                }
                        }
                    }
                }
                else
                {
                    if ( propID->idx == -2 )
                    {
                        // Expose flag have been changed.
                        if ( intVal )
                            m_selectedNode->expose( propID->name );
                        else
                            m_selectedNode->colapse( propID->name );
                    }
                }
                m_selectedNode->node->Touch();
                m_scene->GetModel()->Rebuild();
            }
        }
    }
    else
    {
        if ( m_scene && m_scene->GetModel() )
        {
            if ( propID->name == strID( "Name" ) )
            {
                emit changeProp( propID->name, strVal );
            }
            else
            {
                // Populate me......
            }
        }
    }
    objectUpdated();
}

void UI_Props::_addParamProperties( QtProperty* params, const std::vector< Global::Storage >& storage )
{
    for( auto i = storage.cbegin(); i != storage.cend(); ++i )
    {
        const AnyType& data = i->data;
        const int dataSize  = AnyType::getSize( data.getType() );
        const ManagerType manager = m_managerTypes[ int( data.getType() ) ];

        //Global::verbQ( QString( "    .%1;" ).arg( i->name.c_str() ) );
        if ( manager != ManagerType::None )
        {
            QtProperty* prop = nullptr;
            QtProperty* grp  = nullptr;
            QtProperty* ext  = nullptr;

            if ( dataSize > 1 )
            {
                grp = m_structManager.addProperty( QString ( i->name.c_str() ) );
                m_properties.push_back( PropID( i->name, grp ) );
            }

            for( int idx = 0; idx < dataSize; idx++ )
            {
                QString Label;
                if ( dataSize > 1 )
                    Label = QString( "%1.%2" ).arg( i->name.c_str() ).arg( AnyType::arrayIdxSymbol[ idx ] );
                else
                    Label = i->name.c_str();

                switch( manager )
                {
                    case ManagerType::Int:
                    case ManagerType::UInt:   prop = m_intManager.   addProperty( Label ); break;
                    case ManagerType::Double: prop = m_doubleManager.addProperty( Label ); break;
                    case ManagerType::String: prop = m_stringManager.addProperty( Label ); break;
                    default: break;
                }

                if ( prop )
                {
                    prop->setEnabled( true );
                    m_properties.push_back( PropID( i->name, prop, idx ) );

                    if ( grp )
                        grp->addSubProperty( prop );
                    else
                        params->addSubProperty( prop );
                }
            }

            ext = m_boolManager.addProperty( QString( "external" ) );
            ext->setEnabled( true );
            m_properties.push_back( PropID( i->name, ext, -2 ) );

            if ( grp )
                grp->addSubProperty( ext );
            else
                prop->addSubProperty( ext );

            if ( grp )
                params->addSubProperty( grp );
        }
    }
}

void UI_Props::objectUpdated()
{
    QObject::disconnect( &m_intManager,    SIGNAL(valueChanged( QtProperty*, int ) ),
                          this,            SLOT(  valueChanged( QtProperty*, int ) ) );
    QObject::disconnect( &m_doubleManager, SIGNAL(valueChanged( QtProperty*, double ) ),
                          this,            SLOT(  valueChanged( QtProperty*, double ) ) );
    QObject::disconnect( &m_stringManager, SIGNAL(valueChanged( QtProperty*, const QString& ) ),
                          this,            SLOT(  valueChanged( QtProperty*, const QString& ) ) );
    QObject::disconnect( &m_boolManager,   SIGNAL(valueChanged( QtProperty*, bool ) ),
                          this,            SLOT(  valueChanged( QtProperty*, bool ) ) );

    if (  ( !m_modelMode && m_selectedNode && m_selectedNode->node ) ||
          (  m_modelMode && m_scene && m_scene->GetModel() ) )
    {
        for( auto propID = m_properties.begin(); propID != m_properties.end(); ++propID )
        {
            //Global::verbQ( QString( "    .%1 [ %2 ] @%3;" )
            //                    .arg( propID->name.c_str() )
            //                    .arg( propID->idx )
            //                    .arg( ulong( (void*)propID->prop ) ) );

            if ( propID->name == strID("Name") )
                m_stringManager.setValue( propID->prop, ( m_modelMode ) ?
                        m_scene->GetModel()->getName().c_str() :
                        m_selectedNode->node->getName().c_str() );
            else if ( propID->name == strID("Type") )
                m_stringManager.setValue( propID->prop, ( m_modelMode ) ?
                        m_scene->GetModel()->getType().c_str() :
                        m_selectedNode->node->getType().c_str() );
            else
            {
                const Global::Storage* propStorage = ( m_modelMode )
                        ? m_scene->GetModel()->storageLookup(  propID->name )
                        : m_selectedNode->node->storageLookup( propID->name );
                if ( propStorage )
                {
                    const AnyType& prop = propStorage->data;

                    ManagerType manager = m_managerTypes[ int( prop.getType() ) ];

                    if ( propID->idx >= 0 )
                    {
                        if ( manager != ManagerType::None )
                        {
                            const int dataSize  = AnyType::getSize( prop.getType() );

                            if ( dataSize == 1 )
                                switch( manager )
                                {
                                    case ManagerType::Int:    m_intManager.   setValue( propID->prop, prop.unsafeValueInt() );  break;
                                    case ManagerType::UInt:   m_intManager.   setValue( propID->prop, prop.unsafeValueUInt() ); break;
                                    case ManagerType::Double: m_doubleManager.setValue( propID->prop, prop.unsafeValueNumber() );  break;
                                    case ManagerType::String: m_stringManager.setValue( propID->prop, QString( prop.unsafeString().c_str() ) ); break;
                                    default: break;
                                }
                            if ( dataSize > 1 )
                                switch( manager )
                                {
                                    case ManagerType::Int:    m_intManager.   setValue( propID->prop, prop.unsafeArrayInt(    propID->idx ) );  break;
                                    case ManagerType::UInt:   m_intManager.   setValue( propID->prop, prop.unsafeArrayUInt(   propID->idx ) ); break;
                                    case ManagerType::Double: m_doubleManager.setValue( propID->prop, prop.unsafeArrayNumber( propID->idx ) );  break;
                                    default: break;
                                }
                        }
                    }
                    else
                    {
                        //m_structManager.setValue( i->first, QString( "Test" ) );
                        if ( propID->idx == -2 )
                        {
                            // This is the property for being exposed externally.
                            m_boolManager.setValue( propID->prop, propStorage->exposed );
                        }
                    }
                }
            }
        }

    }

    QObject::connect( &m_intManager,     SIGNAL( valueChanged( QtProperty*, int ) ),
                       this,             SLOT(   valueChanged( QtProperty*, int ) ) );
    QObject::connect( &m_doubleManager,  SIGNAL( valueChanged( QtProperty*, double ) ),
                       this,             SLOT(   valueChanged( QtProperty*, double ) ) );
    QObject::connect( &m_stringManager,  SIGNAL( valueChanged( QtProperty*, const QString& ) ),
                       this,             SLOT(   valueChanged( QtProperty*, const QString& ) ) );
    QObject::connect( &m_boolManager,    SIGNAL( valueChanged( QtProperty*, bool ) ),
                       this,             SLOT(   valueChanged( QtProperty*, bool ) ) );
}


