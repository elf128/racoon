/*
 * NG_LinkDrag.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#include "core/UID.h"
#include "UI_LinkDrag.h"
#include <math.h>
#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QApplication>


UI_LinkDrag::UI_LinkDrag( const QPointF& p_origin, const UI_LinkDrag::LinkType type, const strID p_dataType ) : QGraphicsItem( 0 ) , m_dataType( p_dataType )
{
    m_type        = type;
    m_origin      = p_origin;
    m_isSnapped   = false;
    m_drag_origin = nullptr;
    m_drag_target = nullptr;
    m_oldLink     = nullptr;

    setPos( p_origin );
}

QRectF UI_LinkDrag::boundingRect() const
{
    QPointF from_coord = mapFromScene( this->m_origin );

    const int x  = int ( from_coord.x() );
    const int y  = int ( from_coord.y() );

    const int x1 = ( ( x > 0 ) ? 0 : x ) - 1;
    const int y1 = ( ( y > 0 ) ? 0 : y ) - 1;

    const int w = fabs( x ) + 2;
    const int h = fabs( y ) + 2;

    return QRect( x1, y1, w, h );
}

void UI_LinkDrag::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen p = painter->pen();

    QPointF pos = mapFromScene( m_origin );
    if ( m_isSnapped )
        painter->setPen( QColor( 255, 255,  0) );
    else
        painter->setPen( QColor( 192, 192,  64) );

    painter->drawLine( 0, 0, pos.x()/2, pos.y()/2 );

    painter->setPen( QColor( 64, 64, 64) );
    painter->drawLine( pos.x()/2, pos.y()/2, pos.x(), pos.y() );

    painter->setPen( p );
}

void UI_LinkDrag::setSnap( const QPointF& p_pos)
{
    m_isSnapped = true;
    m_snapPoint = p_pos;
    update();
}

void UI_LinkDrag::unSnap()
{
    m_isSnapped = false;
    m_drag_target = nullptr;
    update();
}

void UI_LinkDrag::OnDrag( QGraphicsSceneMouseEvent *e )
{
    if ( m_isSnapped )
        setPos( m_snapPoint );
    else
        setPos( e->scenePos() );

    prepareGeometryChange();
    update();
}
