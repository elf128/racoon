/*
 * UI_Socket.h
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#ifndef SRC_UI_UI_SOCKET_H_
#define SRC_UI_UI_SOCKET_H_

#include <QGraphicsItem>
#include <qgraphicsitem.h>
#include "core/UID.h"

class UI_Node;

class UI_Socket : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    enum class SlotType
    {
        Input,
        Output,
        Param,
        Prop,
    };

    struct SlotId
    {
        strID    slotName;
        strID    dataType;
        SlotType type;
        int      cachedIdx;
    };

    UI_Socket( UI_Node *p_parent, SlotId p_slot/*, QPointF p_pos */ );
    virtual ~UI_Socket() {}

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget );

    void resolveCachedIdx();

    inline bool isDataType( const strID p_type ) const { return ( slot.dataType == p_type ); }
    inline bool isInput() const { return ( slot.type == SlotType::Input || slot.type == SlotType::Param ); }

protected:
    void mousePressEvent(   QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(    QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event);

    float calcX() const;
    float calcY() const;

    bool reportedDrag;
public:
    SlotId slot;

signals:
    void startDrag( UI_Socket* socket, const strID p_type );
};
#endif /* SRC_UI_UI_SOCKET_H_ */
