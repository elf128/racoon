/*
 * UI_Viewver.cpp
 *
 *  Created on: Nov 15, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "UI_Viewer.h"
#include "UI_Scene.h"
#include "UI_Node.h"

#include "model/Node.h"
#include "model/Structure.h"
#include "model/Reflection.h"
#include "model/Shadow.h"

#include <QToolBar>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QOpenGLWidget>
//#include <QtWidgets/QScrollArea>

#include "DockManager.h"
#include "core/Global.h"

//#include "ui_ViewData.h"
//#include "ui_View2D.h"
//#include "ui_View3D.h"

//#define PROP_ICON ":/icons/Props.png"


UI_Viewer::UI_Viewer(  ads::CDockWidget* widget, QWidget *parent )
    : QWidget( parent )
//    , ui(      new Ui::UI_Viewer )
{
//    ui->setupUi( this );
    container    = widget;
    m_scene      = nullptr;
    m_viewedNode = nullptr;
    m_mode       = Mode::None;
    m_ui         = UI::None;

    ui_horizontalLayout = nullptr;
    ui_tableWidget      = nullptr;
    ui_scrollArea       = nullptr;
    ui_image            = nullptr;
    ui_openGLWidget     = nullptr;

    m_time       = 0;
    m_outIdx     = 0;
    m_outType    = AnyType::Types::Empty;

    m_dimentionality0 = -1;
    m_dimentionality1 = -1;
    m_dimIdx0         = -1;
    m_dimIdx1         = -1;
}

UI_Viewer::~UI_Viewer()
{
    emit viewedNode( nullptr );

    deleteTableWidgets();
    deleteImageWidgets();
    deleteGLWidgets();
    if ( ui_horizontalLayout ) delete ui_horizontalLayout;

    qDebug( "~UI_Viewer()" );
//    delete m_model;
}

void UI_Viewer::setupTableWidgets()
{
    if ( ui_tableWidget == nullptr )
    {
        ui_tableWidget = new QTableWidget( this );

        ui_tableWidget->setObjectName( QString::fromUtf8("tableWidget") );

        ui_tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui_tableWidget->setProperty( "showDropIndicator", QVariant(false) );
        ui_tableWidget->setAlternatingRowColors(true);
        ui_horizontalLayout->addWidget( ui_tableWidget );
    }
}

void UI_Viewer::setupImageWidgets()
{
    if ( ui_image == nullptr )
    {
        ui_image = new QLabel();
        ui_image->setBackgroundRole( QPalette::Base );
        ui_image->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
        ui_image->setScaledContents( true );
    }

    if ( ui_scrollArea == nullptr )
    {
        ui_scrollArea = new QScrollArea();
        ui_scrollArea->setBackgroundRole( QPalette::Dark );
        ui_scrollArea->setWidget( ui_image );
        ui_scrollArea->setVisible( false );

        ui_horizontalLayout->addWidget( ui_scrollArea );
    }
}

void UI_Viewer::setupGLWidgets()
{
    if ( ui_openGLWidget == nullptr )
    {
        ui_openGLWidget = new QOpenGLWidget( this );
        ui_openGLWidget->setObjectName( QString::fromUtf8("openGLWidget") );
        ui_horizontalLayout->addWidget( ui_openGLWidget );
    }
}

void UI_Viewer::deleteTableWidgets()
{
    if ( ui_tableWidget )
    {
        ui_horizontalLayout->removeWidget( ui_tableWidget );
        delete ui_tableWidget;
        ui_tableWidget = nullptr;
    }
}

void UI_Viewer::deleteImageWidgets()
{
    if ( ui_image )
    {
        delete ui_image;
        ui_image = nullptr;
    }

    if ( ui_scrollArea )
    {
        ui_horizontalLayout->removeWidget( ui_scrollArea );
        delete ui_scrollArea;
        ui_scrollArea = nullptr;
    }
}

void UI_Viewer::deleteGLWidgets()
{
    if ( ui_openGLWidget )
    {
        ui_horizontalLayout->removeWidget( ui_openGLWidget );
        delete ui_openGLWidget;
        ui_openGLWidget = nullptr;
    }
}

void UI_Viewer::changeMode( const Mode p_mode )
{
    if ( m_mode == p_mode )
        return;

    if ( p_mode != Mode::None )
    {
        const UI newUI = s_ui[ int ( p_mode ) ];

        if ( m_ui != newUI )
        {
            if ( ui_horizontalLayout == nullptr )
            {
                ui_horizontalLayout = new QHBoxLayout( this );
                ui_horizontalLayout->setSpacing(0);
                ui_horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
                ui_horizontalLayout->setContentsMargins(0, 0, 0, 0);
            }

            switch( newUI )
            {
                case UI::Data:
                    deleteImageWidgets();
                    deleteGLWidgets();
                    setupTableWidgets();
                    break;
                case UI::Graph2D:
                    deleteTableWidgets();
                    deleteGLWidgets();
                    setupImageWidgets();
                    break;
                case UI::Graph3D:
                    deleteImageWidgets();
                    deleteTableWidgets();
                    setupGLWidgets();
                    break;
                default:
                    break;
            }
            m_ui = newUI;
        }
    }
    else
    {
        if ( m_ui != UI::None )
        {
            // delete sub_ui here
            if ( ui_horizontalLayout )
            {
                deleteTableWidgets();
                deleteImageWidgets();
                deleteGLWidgets();

                delete ui_horizontalLayout;
                ui_horizontalLayout = nullptr;
            }

            m_ui = UI::None;
        }
    }

    m_mode = p_mode;
    SetupToolbar();
}

void UI_Viewer::RenderToTable( const AnyType& data )
{
    if ( m_ui == UI_Viewer::UI::Data )
    {
        const ConfigSet& config = s_config[ int( m_outType) ];

        if ( config.dimentionality1 > 0 )
        {
            // Essentially always one, unless it's -1.
            ui_tableWidget->setRowCount( config.dimentionality1 );
        }
        if ( config.dimentionality1 == -1 )
        {
            // ToDo: Special case. Only valid for tensors so far. Ignore it for now.
            // ToDo: StrList also require support right here.
            // ToDo: Ohh, and Vectors as well.
        }

        if ( config.dimentionality0 > 0 )
        {
            ui_tableWidget->setColumnCount( config.dimentionality0 );
            QStringList header;
            for( int i = 0; i < config.dimentionality0; i++ )
                header.append( QString( AnyType::arrayIdxSymbol[ i ] ) );

            ui_tableWidget->setHorizontalHeaderLabels( header );
        }

        if ( config.dimentionality0 == -1 )
        {
            // ToDo: Add suport for arrays here.
        }

        for( int y = 0; y <  config.dimentionality1; y++ )
        {
            for( int x = 0; x <  config.dimentionality0; x++ )
            {
                // we will pretty much ignore Y, since it's only relevant for tensors and strList both are not implemented yet.
                // ToDo: Add bunch of support here
                QTableWidgetItem* item = ui_tableWidget->item( y, x );
                if ( item == nullptr )
                {
                    item = new QTableWidgetItem();
                    ui_tableWidget->setItem( y, x, item );
                }

                if ( ( data.isInPlace() || data.getType() == AnyType::Types::String || data.getType() == AnyType::Types::Filename ) && x == 0 && y == 0 )
                    item->setText( QString("%1").arg( std::string( data ).c_str() ) );
                if ( data.isIArray() && y == 0 )
                    item->setText( QString("%1").arg( data.unsafeArrayInt( x ) ) );
                if ( data.isUArray() && y == 0 )
                    item->setText( QString("%1").arg( data.unsafeArrayUInt( x ) ) );
                if ( data.isNArray() && y == 0 )
                    item->setText( QString("%1").arg( data.unsafeArrayNumber( x ) ) );

            }
        }
    }
}

void UI_Viewer::RenderTo2D( const AnyType& data )
{
    if ( m_ui == UI_Viewer::UI::Graph2D )
    {
        if ( data.getType() == AnyType::Types::Image )
        {
            ui_image->setPixmap(QPixmap::fromImage( *data.unsafeImage() ) );
            ui_scrollArea->setVisible(true);
            ui_image->adjustSize();
        }
    }
}

void UI_Viewer::SetupToolbar()
{
    /*
    if ( container )
    {
        QToolBar* bar = container->createDefaultToolBar();
        QAction* showProps = bar->addAction( QIcon( PROP_ICON ), "Show model's properties." );

        bar->addSeparator();

        const Global::StrList groups = Global::listGroups( type );
        for ( auto i = groups.cbegin(); i != groups.cend(); ++i )
        {
            QAction* action = bar->addAction( QString( i->c_str() ) );
            action->setEnabled( false );

            const Global::StrList layers = Global::listLayers( type, *i );

            for ( auto j = layers.cbegin(); j != layers.cend(); ++j )
            {
                QAction* action = bar->addAction( QString( j->c_str() ) );
                m_actionMap[ action ] = *j;
            }

            bar->addSeparator();
        }
        this->connect( showProps, SIGNAL( triggered() ), SLOT( ShowProps() ) );
        this->connect( bar,       SIGNAL( actionTriggered(QAction*) ), SLOT( onToolBar(QAction*) ) );

    }
    */
}

void UI_Viewer::onToolBar( QAction* action )
{
/*
    auto a = m_actionMap.find( action );

    if ( a == m_actionMap.end() )
        return;

    const strID nodeType = a->second;

    Global::verbQ( QString( "onToolBar( %1 )") .arg( nodeType.c_str() ) );


    QPoint pos = ui->canvas->visibleRegion().boundingRect().center();

    int newX = pos.x();
    int newY = pos.y();

    NG_Node* node = m_model->ConstructNode( nodeType, newX, newY );

    if ( node && m_scene )
        m_scene->AddNodeToModel( node );
*/
}

void UI_Viewer::veiwMe( UI_Node* p_node, UI_Scene* p_scene )
{
    if ( p_node )
        Global::verbQ( QString( "veiwMe.node = %1" ).arg( p_node->node->m_name.c_str() ) );
    if ( p_scene )
        Global::verbQ( QString( "veiwMe.struct = %1" ).arg( p_scene->GetModel()->m_name.c_str() ) );

    if ( p_node )
        QObject::connect( this,   SIGNAL( viewedNode( UI_Node* ) ),
                          p_node, SLOT(   viewedNode( UI_Node* ) ));

    emit viewedNode( p_node );

    if ( m_viewedNode )
        disconnect( m_viewedNode );

    if ( m_scene )
        QObject::disconnect( m_scene,        SIGNAL( Updated() ),
                             this,           SLOT(   objectUpdated()  ));

    m_viewedNode = p_node;
    m_scene      = p_scene;

    if ( m_viewedNode && m_viewedNode->node )
    {
        NG_Node*           node     = m_viewedNode->node;
        UI_Node::Settings& settings = m_viewedNode->settings;
        const int          outNum   = node->getTotalOutputCount();

        m_nodeOutputTypes.resize( outNum );

        size_t i = 0;
        for( auto it = node->m_outputs.cbegin(); it != node->m_outputs.cend(); ++it, i++ )
            m_nodeOutputTypes[ i ] = AnyType::resolveType( it->Type );

        m_outIdx  = settings.viewOutputIdx;
        if ( m_outIdx >= outNum )
            settings.viewOutputIdx = m_outIdx = outNum - 1;

        m_outType = m_nodeOutputTypes[ m_outIdx ];

        if ( settings.viewModeIdx < 0 ||
             settings.viewModeIdx >= int32_t( Mode::MAX ) ||
             ( ( s_config[ int( m_outType) ].supported & ( 1 << settings.viewModeIdx ) ) == 0 ) )
        {
            // reset view mode.
            settings.viewModeIdx = int( getDefaultMode( m_outType ) );
        }

        changeMode( Mode( settings.viewModeIdx ) );
    }
    else
        changeMode( Mode::None );

    updateView();

}

void UI_Viewer::updateView()
{
    if ( m_viewedNode && m_viewedNode->node )
    {
        NG_Node*               node     = m_viewedNode->node;
        UI_Node::Settings&     settings = m_viewedNode->settings;

        Reflection*            ref      = node->m_reflection;
        NG_Shadow::CacheSlice* cache    = ref->getCacheSlice( m_time );

        if ( cache )
        {
            // Data exist
            std::lock_guard< std::recursive_mutex > lock( cache->lock );
            if ( cache->data.size() > m_outIdx )
            {
                const AnyType& data = cache->data[ m_outIdx ];
                if ( data.getType() == m_outType )
                {
                    Global::verbQ( QString( "Viewer got data: %1").arg( std::string( data ).c_str() ) );
                    switch( m_ui )
                    {
                        case UI::Data:
                            RenderToTable( data );
                            break;
                        case UI::Graph2D:
                            RenderTo2D( data );
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Global::errQ( QString( "Viewer. Types doesn't match. Expected type: %1, while cache has %2" )
                            .arg( AnyType::getTypeAsStr( m_outType ).c_str() )
                            .arg( AnyType::getTypeAsStr( data.getType() ).c_str() ) );
                }
            }
            else
            {
                Global::verbQ( QString( "Viewer got back cache, with %1 data, while it needs at least %2" )
                        .arg( cache->data.size() )
                        .arg( m_outIdx+1 )
                        );
            }
        }
        else
        {
            // Data doesn't exit
            Global::verb( "Viewer didn't get back cache" );
        }
    }
}


const std::array< UI_Viewer::ConfigSet, int( AnyType::Types::MAX ) > UI_Viewer::s_config = {
        // Mode, UI, d0, d1, di0, di1
        UI_Viewer::ConfigSet{ 0b000000, Mode::None,    0,  0,  0,  0  }, // Types::Empty
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1,  1,  0,  0  }, // Types::Int
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1,  1,  0,  0  }, // Types::Count
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1,  1,  0,  0  }, // Types::String
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1,  1,  0,  0  }, // Types::Filename
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1,  1,  0,  0  }, // Types::Number
        UI_Viewer::ConfigSet{ 0b000011, Mode::Tables,  2,  1,  0,  0  }, // Types::Vec2
        UI_Viewer::ConfigSet{ 0b000111, Mode::Tables,  3,  1,  0,  0  }, // Types::Vec3
        UI_Viewer::ConfigSet{ 0b000111, Mode::Tables,  4,  1,  0,  0  }, // Types::Vec4
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  2,  1,  0,  0  }, // Types::IVec2
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  3,  1,  0,  0  }, // Types::IVec3
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  4,  1,  0,  0  }, // Types::IVec4
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  2,  1,  0,  0  }, // Types::UVec2
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  3,  1,  0,  0  }, // Types::UVec3
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  4,  1,  0,  0  }, // Types::UVec4
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1, -1,  0,  0  }, // Types::Vector
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1, -1,  0,  0  }, // Types::VectorI
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1, -1,  0,  0  }, // Types::VectorU
        UI_Viewer::ConfigSet{ 0b000001, Mode::Tables,  1, -1,  0,  0  }, // Types::StrList
        UI_Viewer::ConfigSet{ 0b001000, Mode::Image,  -1, -1,  0,  1  }, // Types::Image
        UI_Viewer::ConfigSet{ 0b001001, Mode::Tables, -1, -1, -1, -1  }  // Types::TensorCPU
};

const std::array< UI_Viewer::UI, int( UI_Viewer::Mode::MAX ) > UI_Viewer::s_ui = {
        UI_Viewer::UI::None, // None
        UI_Viewer::UI::Data, // Tables
        UI_Viewer::UI::Graph3D, // Vector
        UI_Viewer::UI::Graph2D, // Color
        UI_Viewer::UI::Graph2D, // Image
        //UI_Viewer::UI::Graph3D, // Graph
};
