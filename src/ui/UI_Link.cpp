/*
 * NG_Link.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: Vlad A.
 */

#include "core/Global.h"

#include "UI_Link.h"
#include "UI_Node.h"
#include "UI_Socket.h"
#include <math.h>
#include <QtGui>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QApplication>


UI_Link::UI_Link( UI_Node *p_from, UI_Node *p_to, UI_Socket* p_slotFrom, UI_Socket* p_slotTo, const strID p_type ) : QGraphicsItem( 0 ) , m_dataType( p_type )
{
    reportedDrag = false;
    m_from       = p_from;
    m_to         = p_to;
    m_slotFrom   = p_slotFrom;
    m_slotTo     = p_slotTo;

    setFlags( ItemIsSelectable );
    setAcceptHoverEvents( true );

    const QPointF posFrom = p_slotFrom->scenePos();
    const QPointF posTo   = p_slotTo->  scenePos();

    setPos( posTo );

    const float dx = posFrom.x() - posTo.x();
    const float dy = posFrom.y() - posTo.y();
    const float l  = 1 / sqrt( dx * dx + dy * dy );

    m_dirX = dx * l;
    m_dirY = dy * l;

    QObject::connect( p_from,   SIGNAL( Moved(      UI_Node* ) ),
                      this,     SLOT(   OnNodeMove( UI_Node* ) ) );
    QObject::connect( p_to,     SIGNAL( Moved(      UI_Node* ) ),
                      this,     SLOT(   OnNodeMove( UI_Node* ) ) );
}

QRectF UI_Link::boundingRect() const
{
    QPointF from_coord = getLinkOriginPos();

    return QRect( ( (from_coord.x() > 0 ) ? 0 : from_coord.x() ) - 1.0f,
                  ( (from_coord.y() > -2.0f ) ? -2.0f : from_coord.y() )  - 1.0f,
                  fabs( from_coord.x() ) + 2.0f,
                  fabs( from_coord.y() + 2 ) + 2.0f );

}

const QPointF UI_Link::getLinkOriginPos() const
{
    return mapFromScene( m_from->pos().x(), m_from->pos().y() + 18 );
}


QPainterPath UI_Link::shape() const
{
    const float offset = 5.0f;
    const float dist   = 10.0f;
    QPointF from_coord = getLinkOriginPos();

    const float startX =  0.0f;
    const float startY = -2.0f;
    const float endX   = from_coord.x();
    const float endY   = from_coord.y();

    QPainterPath path;
    path.moveTo( startX - m_dirY * offset + m_dirX * dist, startY + m_dirX * offset + m_dirY * dist);
    path.lineTo( endX   - m_dirY * offset - m_dirX * dist, endY   + m_dirX * offset - m_dirY * dist );
    path.lineTo( endX   + m_dirY * offset - m_dirX * dist, endY   - m_dirX * offset - m_dirY * dist );
    path.lineTo( startX + m_dirY * offset + m_dirX * dist, startY - m_dirX * offset + m_dirY * dist );

    path.closeSubpath();
    return path;
}

void UI_Link::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    QPen p = painter->pen();

    if (option->state & QStyle::State_MouseOver)
    {
        painter->setPen( QColor( 255, 255, 0) );
    }
    else
    {
        if (option->state & QStyle::State_Selected)
            painter->setPen( QColor( 0, 255, 0) );
        else
            painter->setPen( QColor( 128, 128, 128) );
    }

    //painter->setPen( QColor( 0, 0, 0) );

    QPointF from_coord = getLinkOriginPos();
    painter->drawLine( 0, -2, from_coord.x(), from_coord.y() );

    painter->setPen( p );

}

void UI_Link::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    (void) event;
    //QGraphicsItem::mousePressEvent(event);

}

void UI_Link::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    //QGraphicsItem::mouseMoveEvent(event);
    if ( reportedDrag )
        return;

    const QPoint& eventPos = event->screenPos();
    const QPoint& mousePos = event->buttonDownScreenPos(Qt::LeftButton);

    if ( QLineF( eventPos, mousePos ).length() < QApplication::startDragDistance() )
        return;

    const QPointF& eventScene = event->scenePos();
    const QPointF posFrom = m_slotFrom->scenePos();
    const QPointF posTo   = m_slotTo->  scenePos();

    const float toStart = (posFrom - eventScene ).manhattanLength();
    const float toEnd   = (posTo   - eventScene ).manhattanLength();

    emit startDrag( this, ( toStart > toEnd ), m_dataType );
    this->setCursor(Qt::ClosedHandCursor);

    Global::verb( "UI_Link: emit startDrag" );
    reportedDrag = true;
}

void UI_Link::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    (void) event;
    //QGraphicsItem::mouseReleaseEvent(event);
    setCursor( Qt::ArrowCursor );
    reportedDrag = false;
}

void UI_Link::OnNodeMove( UI_Node* p_node )
{
    const QPointF posFrom = m_slotFrom->scenePos();
    const QPointF posTo   = m_slotTo->  scenePos();

    const float dx = posFrom.x() - posTo.x();
    const float dy = posFrom.y() - posTo.y();
    const float l  = 1 / sqrt( dx * dx + dy * dy );

    m_dirX = dx * l;
    m_dirY = dy * l;

    prepareGeometryChange();
    this->setPos( m_slotTo->scenePos() );
    this->update();
}
