/*
 * UI_Props.h
 *
 *  Created on: May 11, 2021
 *      Author: vlad
 */

#ifndef SRC_UI_UI_PROPS_H_
#define SRC_UI_UI_PROPS_H_

#include "qteditorfactory.h"
#include "qttreepropertybrowser.h"
#include "qtpropertybrowser.h"
#include "qteditorfactory.h"
//#include <FileEdit.h>
#include <QtWidgets/QLineEdit>

#include <array>
#include "stdint.h"
#include "core/Global.h"

class UI_Node;
class UI_Scene;
class FileEditFactory;

class FileEditFactoryPrivate
{
    FileEditFactory* q_ptr;
    Q_DECLARE_PUBLIC( FileEditFactory )
public:
    typedef QList< QLineEdit* > EditorList;
    typedef QMap< QtProperty*, EditorList > PropertyToEditorListMap;
    typedef QMap< QLineEdit*, QtProperty* > EditorToPropertyMap;

    QLineEdit* createEditor(        QtProperty* property, QWidget*parent );
    void       initializeEditor(    QtProperty* property, QLineEdit* e );
    void       slotEditorDestroyed( QObject* object);

    PropertyToEditorListMap  m_createdEditors;
    EditorToPropertyMap      m_editorToProperty;

    void slotPropertyChanged( QtProperty* property, const QString& value);
    void slotRegExpChanged(   QtProperty* property, const QRegExp& regExp);
    void slotSetValue( const QString& value);
};

class FileEditFactory : public QtAbstractEditorFactory<QtStringPropertyManager>
{
    Q_OBJECT
public:
    FileEditFactory(QObject *parent = 0);
    ~FileEditFactory();
protected:
    void connectPropertyManager(QtStringPropertyManager *manager);
    QWidget *createEditor(QtStringPropertyManager *manager, QtProperty *property,
                QWidget *parent);
    void disconnectPropertyManager(QtStringPropertyManager *manager);
private:
    QScopedPointer< FileEditFactoryPrivate > d_ptr;

    Q_DECLARE_PRIVATE( FileEditFactory )
    Q_DISABLE_COPY( FileEditFactory)

    Q_PRIVATE_SLOT(d_func(), void slotPropertyChanged(QtProperty *, const QString &))
    Q_PRIVATE_SLOT(d_func(), void slotRegExpChanged(QtProperty *, const QRegExp &))
    Q_PRIVATE_SLOT(d_func(), void slotSetValue(const QString &))
    Q_PRIVATE_SLOT(d_func(), void slotEditorDestroyed(QObject *))
};

class QtStructPropertyManager : public QtAbstractPropertyManager
{
    Q_OBJECT
public:
    QtStructPropertyManager(QObject *parent = 0);
    ~QtStructPropertyManager();

protected:
    QString valueText(const QtProperty *property) const;

    virtual void initializeProperty(QtProperty *property);
    virtual void uninitializeProperty(QtProperty *property);
};


class UI_Props : public QtTreePropertyBrowser
{
    Q_OBJECT
public:
    UI_Props( QWidget* parent );

private:
    QtGroupPropertyManager         m_groupManager;
    QtStructPropertyManager        m_structManager;
    QtStringPropertyManager        m_stringManager;
    QtIntPropertyManager           m_intManager;
    QtBoolPropertyManager          m_boolManager;
    QtDoublePropertyManager        m_doubleManager;
    //FilePathManager                m_fileManager;

    QtSpinBoxFactory               m_spinboxFactory;
    QtDoubleSpinBoxFactory         m_doubleSpinboxFactory;
    QtLineEditFactory              m_lineeditFactory;
    FileEditFactory                m_fileeditFactory;
    QtCheckBoxFactory              m_checkFactory;

    enum class ManagerType
    {
        None,
        Int,
        UInt,
        Double,
        String,
        MAX
    };

    struct PropID
    {
        strID       name;
        QtProperty* prop;
        int         idx;

        PropID( const strID& p_name, QtProperty* p_prop, const int& p_idx ) : name( p_name ), prop( p_prop ), idx( p_idx ) {}
        PropID( const strID& p_name, QtProperty* p_prop ) : name( p_name ), prop( p_prop ), idx(-1) {}
        PropID( const strID& p_name ) : name( p_name ), prop( nullptr ), idx(-1) {}
        PropID() : name(), prop( nullptr ), idx(-1) {}
    };

    std::vector< PropID >                                   m_properties;
    std::array< ManagerType, int(  AnyType::Types::MAX ) >  m_managerTypes;

    UI_Node*                       m_selectedNode;
    UI_Scene*                      m_scene;

    bool                           m_modelMode;

    void _valueChanged( QtProperty* property, const int intVal, const double doubleVal, const QString& strVal );
    void _addParamProperties( QtProperty* params, const std::vector< Global::Storage >& storage );

    // changeProp is for model rename only.
signals:
    void changeProp( const strID& prop, const QString& newValue );
    void selectedNode( UI_Node* p_node );

public slots:
    void objectUpdated(); // call this whenever currentlyConnectedObject is updated

    void valueChanged( QtProperty* property, bool   val );
    void valueChanged( QtProperty* property, int    val );
    void valueChanged( QtProperty* property, double val );
    void valueChanged( QtProperty* property, const QString& value );

    void showProps( UI_Node* p_node, UI_Scene* p_scene );
};

#endif /* SRC_UI_UI_PROPS_H_ */
