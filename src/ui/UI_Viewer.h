/*
 * UI_Viewver.h
 *
 *  Created on: Nov 15, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#ifndef SRC_UI_UI_VIEWER_H_
#define SRC_UI_UI_VIEWER_H_

#include <QWidget>

#include "core/AnyType.h"

namespace ads { class CDockWidget; }

class QToolBar;
class QHBoxLayout;
class QTableWidget;
class QScrollArea;
class QLabel;
class QOpenGLWidget;
class QAction;
class UI_Scene;
class UI_Node;

class UI_Viewer: public QWidget
{
    Q_OBJECT
public:

    enum class Mode
    {
        None,
        Tables,
        Vector,
        Color,
        Image,
        //Graph,
        //Audio,
        MAX,
    };

    enum class Unit
    {
        None,
        Int,
        Double,
        String,
        MAX,
    };
    enum class UI
    {
        None,
        Data,
        Graph2D,
        Graph3D,
        MAX,
    };

    struct ConfigSet
    {
        uint64_t supported;
        Mode     defMode;
        int      dimentionality0;
        int      dimentionality1;
        int      dimIdx0;
        int      dimIdx1;
    };

    UI_Viewer(  ads::CDockWidget* widget, QWidget* parent = nullptr );
    virtual ~UI_Viewer();

    void SetupToolbar();

    ads::CDockWidget* getContainer() const { return container; }

    inline Mode getDefaultMode( AnyType::Types p_type ) { return s_config[ int( p_type ) ].defMode; }
    void        changeMode( const Mode p_mode );

signals:
//    void clickTripleDot();
    void viewedNode( UI_Node* p_node );


public slots:
    void onToolBar( QAction* );
    void veiwMe( UI_Node* p_node, UI_Scene* p_scene );
    void updateView();

private:

    void RenderToTable( const AnyType& data );
    void RenderTo2D( const AnyType& data );

    void setupTableWidgets();
    void setupImageWidgets();
    void setupGLWidgets();

    void deleteTableWidgets();
    void deleteImageWidgets();
    void deleteGLWidgets();

    Mode                           m_mode;
    UI                             m_ui;
    UI_Node*                       m_viewedNode;
    UI_Scene*                      m_scene;
    ads::CDockWidget*              container;

    QHBoxLayout*                   ui_horizontalLayout;
    QTableWidget*                  ui_tableWidget;
    QScrollArea*                   ui_scrollArea;
    QLabel*                        ui_image;
    QOpenGLWidget*                 ui_openGLWidget;

    int                            m_time;
    int                            m_outIdx;
    AnyType::Types                 m_outType;
    int                            m_dimentionality0;
    int                            m_dimentionality1;
    int                            m_dimIdx0;
    int                            m_dimIdx1;

    std::vector< AnyType::Types >  m_nodeOutputTypes;

    const static std::array< ConfigSet, int( AnyType::Types::MAX ) > s_config;
    const static std::array< UI, int( UI_Viewer::Mode::MAX ) >       s_ui;


//    std::map<QAction*, strID > m_actionMap;
};


#endif /* SRC_UI_UI_VIEWER_H_ */
