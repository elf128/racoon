/*
 * UI_Canvas.cpp
 *
 *  Created on: Jan 12, 2021
 *      Author: vlad
 */

#include "UI_Canvas.h"
#include "UI_Scene.h"
#include "UI_NodeGraph.h"

UI_Canvas::UI_Canvas(QWidget *parent) :
    QGraphicsView(parent)
{
    //this->setAutoFillBackground( false );
    //QGLWidget( QGLFormat( QGL::SampleBuffers ), parent)

    setRenderHint(QPainter::Antialiasing, false);
    setDragMode(QGraphicsView::RubberBandDrag);
    setOptimizationFlags(QGraphicsView::DontSavePainterState);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    scene     = 0;

    setupMatrix();
}

UI_Scene* UI_Canvas::SetModel( NG_Structure* p_model, QWidget* p_props, UI_NodeGraph* widget, QWidget* p_viewer )
{
    if ( scene )
    {
        scene->disconnect( p_props );
        delete scene;
    }

    scene = new UI_Scene;
    setScene( scene );
    scene->SetModel( p_model );

    QObject::connect( scene,   SIGNAL( showProps( UI_Node*, UI_Scene* ) ),
                      p_props, SLOT(   showProps( UI_Node*, UI_Scene* ) ) );

    if ( p_viewer )
        QObject::connect( scene,    SIGNAL( veiwMe( UI_Node*, UI_Scene* ) ),
                          p_viewer, SLOT(   veiwMe( UI_Node*, UI_Scene* ) ) );

    QObject::connect( widget,  SIGNAL( clickTripleDot() ),
                      scene,   SLOT(   clickTripleDot() ) );

    QObject::connect( scene,   SIGNAL( Rename( const QString& ) ),
                      widget,  SLOT(   Rename( const QString& ) ) );

    QObject::connect( p_props, SIGNAL( changeProp( const strID&, const QString& ) ),
                      scene,   SLOT(   changeProp( const strID&, const QString& ) ) );

    scene->setBackgroundBrush( QBrush( QColor( 240, 240, 255 ), Qt::CrossPattern ) );

    return scene;
}

void UI_Canvas::wheelEvent(QWheelEvent *e)
{
    /*
    if (e->modifiers() & Qt::ControlModifier) {
        if (e->delta() > 0)
            view->zoomIn(6);
        else
            view->zoomOut(6);
        e->accept();
    } else {*/
        QGraphicsView::wheelEvent(e);
        //}
}

void UI_Canvas::mouseReleaseEvent(QMouseEvent *e)
{
    QGraphicsView::mouseReleaseEvent( e );
}

void UI_Canvas::setupMatrix()
{
    QMatrix matrix;
    matrix.scale( 1.0, 1.0 );

    QGraphicsView::setMatrix( matrix );
}

void UI_Canvas::togglePointerMode()
{
}

void UI_Canvas::zoomIn( int level )
{
    Q_UNUSED( level );
}

void UI_Canvas::zoomOut( int level )
{
    Q_UNUSED( level );
}

void UI_Canvas::resetView()
{
    this->setupMatrix();
}
