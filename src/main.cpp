#include "mainwindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>


int main(int argc, char *argv[])
{
    //Q_INIT_RESOURCE(mdi);

    QApplication a(argc, argv);
    QCoreApplication::setApplicationName( "Racoon" );
    QCoreApplication::setOrganizationName( "vbw" );
    QCoreApplication::setApplicationVersion( QT_VERSION_STR );

    QCommandLineParser parser;
    parser.setApplicationDescription( "Light weight Neural Network IDE" );
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument( "file", "The file to open." );
    parser.process( a );

    MainWindow w;

    foreach ( const QString &fileName, parser.positionalArguments() )
        w.openFile( fileName );

    w.show();
    return a.exec();
}
