/*
 * tests.cpp
 *
 *  Created on: Sep 16, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */


//! [0]
#include "tests.h"
//! [0]

//! [1]
#include "test_AnyType.cpp"
//! [1]

//! [2]

int    g_argc;
char **g_argv;

int main(int argc, char *argv[])
{
//    QApplication app(argc, argv);
    //app.setAttribute(Qt::AA_Use96Dpi, true);
//    QTEST_SET_MAIN_SOURCE_PATH

    int res = 0;
    Global::__init__( false );

#if !defined( NO_SHAPE_TEST )
    {
        TestAnyType t1;
        res = QTest::qExec(&t1, argc, argv);
    }
#endif

    g_argc = argc;
    g_argv = argv;

    return res;
}

#include "moc_test_AnyType.cpp"
//! [2]
