/*
 * test_AnyType.h
 *
 *  Created on: Sep 16, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#ifndef SRC_TESTS_TEST_ANYTYPE_H_
#define SRC_TESTS_TEST_ANYTYPE_H_

#include <yaml-cpp/yaml.h>

#include "core/AnyType.h"
#include "core/Global.h"

class TestAnyType : public QObject
{
    Q_OBJECT

    YAML::Node srcInt;
    YAML::Node srcCount;
    YAML::Node srcNumber;
    YAML::Node srcString;
    YAML::Node srcFileName;
    YAML::Node srcVec2;
    YAML::Node srcVec3;
    YAML::Node srcVec4;
    YAML::Node srcIVec2;
    YAML::Node srcIVec3;
    YAML::Node srcIVec4;
    YAML::Node srcUVec2;
    YAML::Node srcUVec3;
    YAML::Node srcUVec4;
    YAML::Node srcVector;
    YAML::Node srcVectorI;
    YAML::Node srcVectorU;

    int32_t     I32;
    float       F32;
    std::string str;
    std::vector< float >   vecF;
    std::vector< int32_t > vecI;

    template < typename T >
    void testRead( const YAML::Node& yaml, T& holder, const T target, const bool shouldWork, const char* typetext, const QString where  );

    template < typename T >
    void testFromYAMLInplace( AnyType& Unit, const YAML::Node& yaml, const AnyType::Types p_type, const T target, const bool shouldWork, const char* typetext, const QString where  );

    template < typename T >
    void testFromYAMLDirect(  AnyType& Unit, const YAML::Node& yaml, const AnyType::Types p_type, const T target, const bool shouldWork, const char* typetext, const QString where  );

    template < typename ItemT >
    void testFromYAMLArray(   AnyType& Unit, const YAML::Node& yaml, const AnyType::Types p_type, const std::vector< ItemT > target, const bool shouldWork, const char* typetext, const QString where  );

    template < typename ItemT >
    void testFromYAMLVector(  AnyType& Unit, const YAML::Node& yaml, const AnyType::Types p_type, const std::vector< ItemT > target, const bool shouldWork, const char* typetext, const QString where  );

    template < typename T >
    void testInplace( AnyType& Unit, const T target, const char* typetext, const QString where  );

    template < typename T >
    void testDirect(  AnyType& Unit, const T target, const char* typetext, const QString where  );

    template < typename ItemT >
    void testArray(   AnyType& Unit, const bool ignoreTarget, const std::vector< ItemT > target, const char* typetext, const QString where  );

    template < typename ItemT >
    void testVector(  AnyType& Unit, const bool ignoreTarget, const std::vector< ItemT > target, const char* typetext, const QString where  );

    void initUnitFromSrcYAML( AnyType& Unit, const AnyType::Types type );
    void testUnitFromSrcData( AnyType& Unit, const AnyType::Types type, const char* typetext, const QString where  );

private slots:
    void readYAML();
    void fromYAML();
    void Clone();
    void Size();

    void initTestCase();
    void cleanupTestCase();
};

#endif /* SRC_TESTS_TEST_ANYTYPE_H_ */
