/*
 * test_AnyType.cpp
 *
 *  Created on: Sep 16, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

//#include "test_AnyType.h"

//#define LOG_READ_YAML( A )
void TestAnyType::initTestCase()
{

    srcInt      = -15;
    srcCount    = 223;
    srcNumber   = -3.1415926;
    srcString   = "Hello world";
    srcFileName = YAML::Load("/tmp/testfile");
    srcVec2     = YAML::Load("[ 0.8, -0.5 ]");
    srcVec3     = YAML::Load("[ 0.8, -0.5, 1.0 ]");
    srcVec4     = YAML::Load("[ 0.707, -0.5, 3.1415, 0.707 ]");
    srcIVec2    = YAML::Load("[ 12, -13 ]");
    srcIVec3    = YAML::Load("[ 168, -12, 13 ]");
    srcIVec4    = YAML::Load("[ 192, -168, 0, -1 ]");
    srcUVec2    = YAML::Load("[ 2, 3 ]");
    srcUVec3    = YAML::Load("[ 5, 8, 13 ]");
    srcUVec4    = YAML::Load("[ 21, 34, 55, 89 ]");
    srcVector   = YAML::Load("[ 0.1, -0.5, 0.2, -0.4, 0.3, -0.3, 0.4, -0.2 ]");
    srcVectorI  = YAML::Load("[-1, 0, 1, 2, 3 ]");
    srcVectorU  = YAML::Load("[ 1, 2, 3, 4, 5 ]");

    I32         = 0;
    F32         = 0.0f;

    AnyType Unit;
    CHECK( Unit.data, nullptr );
    CHECK( Unit.type, AnyType::Types::Empty );
}

void TestAnyType::cleanupTestCase()
{

}

template < typename T >
void TestAnyType::testRead( const YAML::Node& yaml, T& holder, const T target, const bool shouldWork, const char* typetext, const QString where  )
{
#if defined( LOG_READ_YAML )
    qDebug().noquote() << QString( "try" ) << typetext;
#endif
    AnyType Unit;

    CHECK3( Unit.readYAML< T >( yaml, holder ), shouldWork, typetext, where );
    if ( shouldWork )
    {
        CHECK3( holder, target, typetext, where );
    }
#if defined( LOG_READ_YAML )
    else
        qDebug( "expected failure." );
#endif

}

template < typename T >
void TestAnyType::testInplace(
                            AnyType& Unit,
                            const T target,
                            const char* typetext,
                            const QString where )
{
    CHECK3( *( T* )( &Unit.data ), target, typetext, where );
}

template < typename T >
void TestAnyType::testDirect(
                            AnyType& Unit,
                            const T target,
                            const char* typetext,
                            const QString where )
{
    CHECK3( *reinterpret_cast< T* >( Unit.data ), target, typetext, where );
}

template < typename ItemT >
void TestAnyType::testArray(
                            AnyType& Unit,
                            const bool ignoreTarget,
                            const std::vector< ItemT > target,
                            const char* typetext,
                            const QString where  )
{
    for( size_t i = 0; i < target.size(); ++i )
    {
        QString note = QString("%1 in item %2").arg( typetext ).arg( i );
        if ( ignoreTarget )
        {
            CHECK3( reinterpret_cast< ItemT* >( Unit.data )[ i ], ItemT( 0 ), note, where );
        }
        else
        {
            CHECK3( reinterpret_cast< ItemT* >( Unit.data )[ i ], target[ i ], note, where );
        }
    }
}

template < typename ItemT >
void TestAnyType::testVector(
                            AnyType& Unit,
                            const bool ignoreTarget,
                            const std::vector< ItemT > target,
                            const char* typetext,
                            const QString where  )
{
    std::vector< ItemT >* vector = reinterpret_cast< std::vector< ItemT >* >( Unit.data );
    CHECK3( vector->size(), target.size(), typetext, where );

    for( size_t i = 0; i < target.size(); ++i )
    {
        QString note = QString("%1 in item %2").arg( typetext ).arg( i );
        if ( ignoreTarget )
        {
            CHECK3( (*vector)[ i ], ItemT( 0 ), note, where );
        }
        else
        {
            CHECK3( (*vector)[ i ], target[ i ], note, where );
        }

    }
}

template < typename T >
void TestAnyType::testFromYAMLInplace(
                            AnyType& Unit,
                            const YAML::Node& yaml,
                            const AnyType::Types p_type,
                            const T target,
                            const bool shouldWork,
                            const char* typetext,
                            const QString where )
{
#if defined( LOG_READ_YAML )
    qDebug().noquote() << QString( "try" ) << typetext;
#endif
    CHECK3( Unit.fromYAML( p_type, yaml ), shouldWork, typetext, where  );
    CHECK3( Unit.type, p_type, typetext, where   );

    if ( shouldWork )
        testInplace( Unit, target, typetext, where );
    else
    {
        testInplace( Unit, T(0), typetext, where );
#if defined( LOG_READ_YAML )
        qDebug( "expected failure." );
#endif
    }
}

template < typename T >
void TestAnyType::testFromYAMLDirect(
                            AnyType& Unit,
                            const YAML::Node& yaml,
                            const AnyType::Types p_type,
                            const T target,
                            const bool shouldWork,
                            const char* typetext,
                            const QString where )
{
#if defined( LOG_READ_YAML )
    qDebug().noquote() << QString( "try" ) << typetext;
#endif

    CHECK3( Unit.fromYAML( p_type, yaml ), shouldWork, typetext, where  );
    CHECK3( Unit.type, p_type, typetext, where );
    CHECK3( (Unit.data == nullptr), false, typetext, where );

    if ( shouldWork )
        testDirect( Unit, target, typetext, where );
    else
    {
        testDirect( Unit, T(), typetext, where );
#if defined( LOG_READ_YAML )
        qDebug( "expected failure." );
#endif
    }
}

template < typename ItemT >
void TestAnyType::testFromYAMLArray(
                            AnyType& Unit,
                            const YAML::Node& yaml,
                            const AnyType::Types p_type,
                            const std::vector< ItemT > target,
                            const bool shouldWork,
                            const char* typetext,
                            const QString where  )
{
//#if defined( LOG_READ_YAML )
    qDebug().noquote() << QString( "try" ) << typetext;
//#endif

    CHECK3( Unit.fromYAML( p_type, yaml ), shouldWork, typetext, where  );
    CHECK3( Unit.type, p_type, typetext, where );
    CHECK3( (Unit.data == nullptr), false, typetext, where );

    testArray( Unit, !shouldWork, target, typetext, where );

//#if defined( LOG_READ_YAML )
    if ( !shouldWork )
        qDebug( "expected failure." );
//#endif
}

template < typename ItemT >
void TestAnyType::testFromYAMLVector(
                            AnyType& Unit,
                            const YAML::Node& yaml,
                            const AnyType::Types p_type,
                            const std::vector< ItemT > target,
                            const bool shouldWork,
                            const char* typetext,
                            const QString where  )
{
#if defined( LOG_READ_YAML )
    qDebug().noquote() << QString( "try" ) << typetext;
#endif

    CHECK3( Unit.fromYAML( p_type, yaml ), shouldWork, typetext, where  );
    CHECK3( Unit.type, p_type, typetext, where );
    CHECK3( (Unit.data == nullptr), false, typetext, where );

    testVector( Unit, !shouldWork, target, typetext, where );

#if defined( LOG_READ_YAML )
    if ( !shouldWork )
        qDebug( "expected failure." );
#endif
}

void TestAnyType::readYAML()
{

    AnyType Unit;

    qDebug("------------------------------------------------------------");
    qDebug(  "srcInit" );
    testRead( srcInt, I32,  int32_t( -15 ),         true,  "Int to int32",   CHECKPOINT  );
    testRead( srcInt, F32,  -15.0f,                 true,  "Int to float",   CHECKPOINT  );
    testRead( srcInt, str,  std::string("-15"),     true,  "Int to string",  CHECKPOINT  );
    testRead( srcInt, vecF, std::vector<float>(),   false, "Int to vectorF", CHECKPOINT  );
    testRead( srcInt, vecI, std::vector<int32_t>(), false, "Int to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcCount" );
    testRead( srcCount, I32,  int32_t( 223 ),         true,  "Count to int32",   CHECKPOINT  );
    testRead( srcCount, F32,  223.0f,                 true,  "Count to float",   CHECKPOINT  );
    testRead( srcCount, str,  std::string("223"),     true,  "Count to string",  CHECKPOINT  );
    testRead( srcCount, vecF, std::vector<float>(),   false, "Count to vectorF", CHECKPOINT  );
    testRead( srcCount, vecI, std::vector<int32_t>(), false, "Count to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcNumber" );
    testRead( srcNumber, I32,  int32_t( -3 ),             true,  "Number to int32",   CHECKPOINT  );
    testRead( srcNumber, F32,  -3.1415926f,               true,  "Number to float",   CHECKPOINT  );
    testRead( srcNumber, str,  std::string("-3.1415926"), true,  "Number to string",  CHECKPOINT  );
    testRead( srcNumber, vecF, std::vector<float>(),      false, "Number to vectorF", CHECKPOINT  );
    testRead( srcNumber, vecI, std::vector<int32_t>(),    false, "Number to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcString" );
    testRead( srcString, I32,  int32_t( 0 ),               false, "String to int32",   CHECKPOINT  );
    testRead( srcString, F32,  0.0f,                       false, "String to float",   CHECKPOINT  );
    testRead( srcString, str,  std::string("Hello world"), true,  "String to string",  CHECKPOINT  );
    testRead( srcString, vecF, std::vector<float>(),       false, "String to vectorF", CHECKPOINT  );
    testRead( srcString, vecI, std::vector<int32_t>(),     false, "String to vectorI", CHECKPOINT  );

    const std::vector<float>   testVec2f = { 0.8, -0.5 };
    const std::vector<int32_t> testVec2i = { 0, 0 };
    const std::vector<float>   testVec3f = { 0.8, -0.5, 1.0 };
    const std::vector<int32_t> testVec3i = { 0, 0, 1 };
    const std::vector<float>   testVec4f = { 0.707, -0.5, 3.1415, 0.707 };
    const std::vector<int32_t> testVec4i = { 0, 0, 3, 0 };

    /*
    srcIVec2    = YAML::Load("[ 12, -13 ]");
    srcIVec3    = YAML::Load("[ 168, -12, 13 ]");
    srcIVec4    = YAML::Load("[ 192, -168, 0, -1 ]");
    srcUVec2    = YAML::Load("[ 2, 3 ]");
    srcUVec3    = YAML::Load("[ 5, 8, 13 ]");
    srcUVec4    = YAML::Load("[ 21, 34, 55, 89 ]");
     */
    const std::vector<float>   testIVec2f = { 12.0f, -13.0f };
    const std::vector<int32_t> testIVec2i = { 12,    -13 };
    const std::vector<float>   testIVec3f = { 168.0, -12.0, 13.0 };
    const std::vector<int32_t> testIVec3i = { 168, -12, 13 };
    const std::vector<float>   testIVec4f = { 192.0, -168.0, 0.0, -1.0 };
    const std::vector<int32_t> testIVec4i = { 192, -168, 0, -1 };

    const std::vector<float>   testVecFf = { 0.1, -0.5, 0.2, -0.4, 0.3, -0.3, 0.4, -0.2 };
    const std::vector<int32_t> testVecFi = { 0,    0,   0,    0,   0,    0,   0,    0 };
    const std::vector<float>   testVecIf  = { -1, 0, 1, 2, 3 };
    const std::vector<int32_t> testVecIi  = { -1, 0, 1, 2, 3 };
    const std::vector<float>   testVecUf  = {  1, 2, 3, 4, 5 };
    const std::vector<int32_t> testVecUi  = {  1, 2, 3, 4, 5 };

    qDebug("------------------------------------------------------------");
    qDebug(  "srcVec2" );
    testRead( srcVec2, I32,  int32_t( 0 ),    false,  "Vec2 to int32",   CHECKPOINT  );
    testRead( srcVec2, F32,  0.0f,            false,  "Vec2 to float",   CHECKPOINT  );
    testRead( srcVec2, str,  std::string(""), false,  "Vec2 to string",  CHECKPOINT  );
    testRead( srcVec2, vecF, testVec2f,       true,   "Vec2 to vectorF", CHECKPOINT  );
    testRead( srcVec2, vecI, testVec2i,       true,   "Vec2 to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcVec3" );
    testRead( srcVec3, I32,  int32_t( 0 ),    false,  "Vec3 to int32",   CHECKPOINT  );
    testRead( srcVec3, F32,  0.0f,            false,  "Vec3 to float",   CHECKPOINT  );
    testRead( srcVec3, str,  std::string(""), false,  "Vec3 to string",  CHECKPOINT  );
    testRead( srcVec3, vecF, testVec3f,       true,   "Vec3 to vectorF", CHECKPOINT  );
    testRead( srcVec3, vecI, testVec3i,       true,   "Vec3 to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcVec4" );
    testRead( srcVec4, I32,  int32_t( 0 ),    false,  "Vec4 to int32",   CHECKPOINT  );
    testRead( srcVec4, F32,  0.0f,            false,  "Vec4 to float",   CHECKPOINT  );
    testRead( srcVec4, str,  std::string(""), false,  "Vec4 to string",  CHECKPOINT  );
    testRead( srcVec4, vecF, testVec4f,       true,   "Vec4 to vectorF", CHECKPOINT  );
    testRead( srcVec4, vecI, testVec4i,       true,   "Vec4 to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcIVec2" );
    testRead( srcIVec2, I32,  int32_t( 0 ),    false,  "IVec2 to int32",   CHECKPOINT  );
    testRead( srcIVec2, F32,  0.0f,            false,  "IVec2 to float",   CHECKPOINT  );
    testRead( srcIVec2, str,  std::string(""), false,  "IVec2 to string",  CHECKPOINT  );
    testRead( srcIVec2, vecF, testIVec2f,      true,   "IVec2 to vectorF", CHECKPOINT  );
    testRead( srcIVec2, vecI, testIVec2i,      true,   "IVec2 to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcIVec3" );
    testRead( srcIVec3, I32,  int32_t( 0 ),    false,  "IVec3 to int32",   CHECKPOINT  );
    testRead( srcIVec3, F32,  0.0f,            false,  "IVec3 to float",   CHECKPOINT  );
    testRead( srcIVec3, str,  std::string(""), false,  "IVec3 to string",  CHECKPOINT  );
    testRead( srcIVec3, vecF, testIVec3f,       true,   "IVec3 to vectorF", CHECKPOINT  );
    testRead( srcIVec3, vecI, testIVec3i,       true,   "IVec3 to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcIVec4" );
    testRead( srcIVec4, I32,  int32_t( 0 ),    false,  "IVec4 to int32",   CHECKPOINT  );
    testRead( srcIVec4, F32,  0.0f,            false,  "IVec4 to float",   CHECKPOINT  );
    testRead( srcIVec4, str,  std::string(""), false,  "IVec4 to string",  CHECKPOINT  );
    testRead( srcIVec4, vecF, testIVec4f,       true,   "IVec4 to vectorF", CHECKPOINT  );
    testRead( srcIVec4, vecI, testIVec4i,       true,   "IVec4 to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcVector" );
    testRead( srcVector, I32,  int32_t( 0 ),    false,  "Vector to int32",   CHECKPOINT  );
    testRead( srcVector, F32,  0.0f,            false,  "Vector to float",   CHECKPOINT  );
    testRead( srcVector, str,  std::string(""), false,  "Vector to string",  CHECKPOINT  );
    testRead( srcVector, vecF, testVecFf,       true,   "Vector to vectorF", CHECKPOINT  );
    testRead( srcVector, vecI, testVecFi,       true,   "Vector to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcVectorI" );
    testRead( srcVectorI, I32,  int32_t( 0 ),    false,  "VectorI to int32",   CHECKPOINT  );
    testRead( srcVectorI, F32,  0.0f,            false,  "VectorI to float",   CHECKPOINT  );
    testRead( srcVectorI, str,  std::string(""), false,  "VectorI to string",  CHECKPOINT  );
    testRead( srcVectorI, vecF, testVecIf,       true,   "VectorI to vectorF", CHECKPOINT  );
    testRead( srcVectorI, vecI, testVecIi,       true,   "VectorI to vectorI", CHECKPOINT  );

    qDebug("------------------------------------------------------------");
    qDebug(  "srcVectorU" );
    testRead( srcVectorU, I32,  int32_t( 0 ),    false,  "VectorU to int32",   CHECKPOINT  );
    testRead( srcVectorU, F32,  0.0f,            false,  "VectorU to float",   CHECKPOINT  );
    testRead( srcVectorU, str,  std::string(""), false,  "VectorU to string",  CHECKPOINT  );
    testRead( srcVectorU, vecF, testVecUf,       true,   "VectorU to vectorF", CHECKPOINT  );
    testRead( srcVectorU, vecI, testVecUi,       true,   "VectorU to vectorI", CHECKPOINT  );
    qDebug("=========================  PASS  ===========================");

}


void TestAnyType::fromYAML()
{
    AnyType Unit;

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Int" );
    testFromYAMLInplace( Unit, srcInt,      AnyType::Types::Int, int32_t( -15 ), true,  "Int      as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcCount,    AnyType::Types::Int, int32_t( 223 ), true,  "Count    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcNumber,   AnyType::Types::Int, int32_t( -3  ), true,  "Number   as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcString,   AnyType::Types::Int, int32_t(  0  ), false, "String   as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcFileName, AnyType::Types::Int, int32_t(  0  ), false, "FileName as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec2,     AnyType::Types::Int, int32_t(  0  ), false, "Vec2     as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec3,     AnyType::Types::Int, int32_t(  0  ), false, "Vec3     as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec4,     AnyType::Types::Int, int32_t(  0  ), false, "Vec4     as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec2,    AnyType::Types::Int, int32_t(  0  ), false, "iVec2    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec3,    AnyType::Types::Int, int32_t(  0  ), false, "iVec3    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec4,    AnyType::Types::Int, int32_t(  0  ), false, "iVec4    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec2,    AnyType::Types::Int, int32_t(  0  ), false, "uVec2    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec3,    AnyType::Types::Int, int32_t(  0  ), false, "uVec3    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec4,    AnyType::Types::Int, int32_t(  0  ), false, "uVec4    as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVector,   AnyType::Types::Int, int32_t(  0  ), false, "Vector   as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVectorI,  AnyType::Types::Int, int32_t(  0  ), false, "VectorI  as Int", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVectorU,  AnyType::Types::Int, int32_t(  0  ), false, "VectorU  as Int", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 0 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Count" );
    testFromYAMLInplace( Unit, srcInt,      AnyType::Types::Count, uint32_t(  0  ), true,  "Int      as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcCount,    AnyType::Types::Count, uint32_t( 223 ), true,  "Count    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcNumber,   AnyType::Types::Count, uint32_t(  0  ), true,  "Number   as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcString,   AnyType::Types::Count, uint32_t(  0  ), false, "String   as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcFileName, AnyType::Types::Count, uint32_t(  0  ), false, "FileName as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec2,     AnyType::Types::Count, uint32_t(  0  ), false, "Vec2     as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec3,     AnyType::Types::Count, uint32_t(  0  ), false, "Vec3     as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec4,     AnyType::Types::Count, uint32_t(  0  ), false, "Vec4     as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec2,    AnyType::Types::Count, uint32_t(  0  ), false, "iVec2    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec3,    AnyType::Types::Count, uint32_t(  0  ), false, "iVec3    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec4,    AnyType::Types::Count, uint32_t(  0  ), false, "iVec4    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec2,    AnyType::Types::Count, uint32_t(  0  ), false, "uVec2    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec3,    AnyType::Types::Count, uint32_t(  0  ), false, "uVec3    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec4,    AnyType::Types::Count, uint32_t(  0  ), false, "uVec4    as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVector,   AnyType::Types::Count, uint32_t(  0  ), false, "Vector   as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVectorI,  AnyType::Types::Count, uint32_t(  0  ), false, "VectorI  as Count", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVectorU,  AnyType::Types::Count, uint32_t(  0  ), false, "VectorU  as Count", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 0 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Number" );
    testFromYAMLInplace( Unit, srcInt,      AnyType::Types::Number, -15.0f,      true,  "Int      as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcCount,    AnyType::Types::Number, 223.0f,      true,  "Count    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcNumber,   AnyType::Types::Number, -3.1415926f, true,  "Number   as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcString,   AnyType::Types::Number, 0.0f,        false, "String   as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcFileName, AnyType::Types::Number, 0.0f,        false, "FileName as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec2,     AnyType::Types::Number, 0.0f,        false, "Vec2     as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec3,     AnyType::Types::Number, 0.0f,        false, "Vec3     as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVec4,     AnyType::Types::Number, 0.0f,        false, "Vec4     as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec2,    AnyType::Types::Number, 0.0f,        false, "iVec2    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec3,    AnyType::Types::Number, 0.0f,        false, "iVec3    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcIVec4,    AnyType::Types::Number, 0.0f,        false, "iVec4    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec2,    AnyType::Types::Number, 0.0f,        false, "uVec2    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec3,    AnyType::Types::Number, 0.0f,        false, "uVec3    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcUVec4,    AnyType::Types::Number, 0.0f,        false, "uVec4    as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVector,   AnyType::Types::Number, 0.0f,        false, "Vector   as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVectorI,  AnyType::Types::Number, 0.0f,        false, "VectorI  as Number", CHECKPOINT );
    testFromYAMLInplace( Unit, srcVectorU,  AnyType::Types::Number, 0.0f,        false, "VectorU  as Number", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 0 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::String" );
    testFromYAMLDirect( Unit, srcInt,      AnyType::Types::String, std::string("-15"),           true,  "Int      as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcCount,    AnyType::Types::String, std::string("223"),           true,  "Count    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcNumber,   AnyType::Types::String, std::string("-3.1415926"),    true,  "Number   as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcString,   AnyType::Types::String, std::string("Hello world"),   true,  "String   as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcFileName, AnyType::Types::String, std::string("/tmp/testfile"), true,  "FileName as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVec2,     AnyType::Types::String, std::string(""),              false, "Vec2     as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVec3,     AnyType::Types::String, std::string(""),              false, "Vec3     as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVec4,     AnyType::Types::String, std::string(""),              false, "Vec4     as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcIVec2,    AnyType::Types::String, std::string(""),              false, "iVec2    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcIVec3,    AnyType::Types::String, std::string(""),              false, "iVec3    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcIVec4,    AnyType::Types::String, std::string(""),              false, "iVec4    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcUVec2,    AnyType::Types::String, std::string(""),              false, "uVec2    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcUVec3,    AnyType::Types::String, std::string(""),              false, "uVec3    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcUVec4,    AnyType::Types::String, std::string(""),              false, "uVec4    as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVector,   AnyType::Types::String, std::string(""),              false, "Vector   as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVectorI,  AnyType::Types::String, std::string(""),              false, "VectorI  as String", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVectorU,  AnyType::Types::String, std::string(""),              false, "VectorU  as String", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Filename" );
    testFromYAMLDirect( Unit, srcInt,      AnyType::Types::Filename, std::string("-15"),           true,  "Int      as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcCount,    AnyType::Types::Filename, std::string("223"),           true,  "Count    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcNumber,   AnyType::Types::Filename, std::string("-3.1415926"),    true,  "Number   as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcString,   AnyType::Types::Filename, std::string("Hello world"),   true,  "String   as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcFileName, AnyType::Types::Filename, std::string("/tmp/testfile"), true,  "FileName as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVec2,     AnyType::Types::Filename, std::string(""),              false, "Vec2     as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVec3,     AnyType::Types::Filename, std::string(""),              false, "Vec3     as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVec4,     AnyType::Types::Filename, std::string(""),              false, "Vec4     as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcIVec2,    AnyType::Types::Filename, std::string(""),              false, "iVec2    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcIVec3,    AnyType::Types::Filename, std::string(""),              false, "iVec3    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcIVec4,    AnyType::Types::Filename, std::string(""),              false, "iVec4    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcUVec2,    AnyType::Types::Filename, std::string(""),              false, "uVec2    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcUVec3,    AnyType::Types::Filename, std::string(""),              false, "uVec3    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcUVec4,    AnyType::Types::Filename, std::string(""),              false, "uVec4    as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVector,   AnyType::Types::Filename, std::string(""),              false, "Vector   as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVectorI,  AnyType::Types::Filename, std::string(""),              false, "VectorI  as Filename", CHECKPOINT );
    testFromYAMLDirect( Unit, srcVectorU,  AnyType::Types::Filename, std::string(""),              false, "VectorU  as Filename", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    //srcVec2     = YAML::Load("[ 0.8, -0.5 ]");
    //srcVec3     = YAML::Load("[ 0.8, -0.5, 1.0 ]");
    //srcVec4     = YAML::Load("[ 0.707, -0.5, 3.1415, 0.707 ]");
    //srcVector   = YAML::Load("[ 0.1, -0.5, 0.2, -0.4, 0.3, -0.3, 0.4, -0.2 ]");
    //srcVectorI  = YAML::Load("[-1, 0, 1, 2, 3 ]");
    //srcVectorU  = YAML::Load("[ 1, 2, 3, 4, 5 ]");
    //srcIVec2    = YAML::Load("[ 12, -13 ]");
    //srcIVec3    = YAML::Load("[ 168, -12, 13 ]");
    //srcIVec4    = YAML::Load("[ 192, -168, 0, -1 ]");
    //srcUVec2    = YAML::Load("[ 2, 3 ]");
    //srcUVec3    = YAML::Load("[ 5, 8, 13 ]");
    //srcUVec4    = YAML::Load("[ 21, 34, 55, 89 ]");

    const std::vector<float>    emptyF;
    const std::vector<int32_t>  emptyI;
    const std::vector<uint32_t> emptyU;

    const std::vector<float> exV2V2 = {  0.8f,   -0.5f   };
    const std::vector<float> exV3V2 = {  0.8f,   -0.5f   };
    const std::vector<float> exV4V2 = {  0.707f, -0.5f   };
    const std::vector<float> exI2V2 = {  12.0f,  -13.0f  };
    const std::vector<float> exI3V2 = {  168.0f, -12.0f  };
    const std::vector<float> exI4V2 = {  192.0f, -168.0f };
    const std::vector<float> exU2V2 = {  2.0f,    3.0f   };
    const std::vector<float> exU3V2 = {  5.0f,    8.0f   };
    const std::vector<float> exU4V2 = {  21.0f,   34.0f  };
    const std::vector<float> exVnV2 = {  0.1f,   -0.5f   };
    const std::vector<float> exViV2 = { -1.0f,    0.0f   };
    const std::vector<float> exVuV2 = {  1.0f,    2.0f   };

    const std::vector<int32_t> exV2I2 = {  0,      0   };
    const std::vector<int32_t> exV3I2 = {  0,      0   };
    const std::vector<int32_t> exV4I2 = {  0,      0   };
    const std::vector<int32_t> exI2I2 = {  12,   -13   };
    const std::vector<int32_t> exI3I2 = {  168,  -12   };
    const std::vector<int32_t> exI4I2 = {  192, -168   };
    const std::vector<int32_t> exU2I2 = {  2,      3   };
    const std::vector<int32_t> exU3I2 = {  5,      8   };
    const std::vector<int32_t> exU4I2 = {  21,    34   };
    const std::vector<int32_t> exVnI2 = {  0,      0   };
    const std::vector<int32_t> exViI2 = { -1,      0   };
    const std::vector<int32_t> exVuI2 = {  1,      2   };

    const std::vector<uint32_t> exV2U2 = {  0,      0   };
    const std::vector<uint32_t> exV3U2 = {  0,      0   };
    const std::vector<uint32_t> exV4U2 = {  0,      0   };
    const std::vector<uint32_t> exI2U2 = {  12,     0   };
    const std::vector<uint32_t> exI3U2 = {  168,    0   };
    const std::vector<uint32_t> exI4U2 = {  192,    0   };
    const std::vector<uint32_t> exU2U2 = {  2,      3   };
    const std::vector<uint32_t> exU3U2 = {  5,      8   };
    const std::vector<uint32_t> exU4U2 = {  21,    34   };
    const std::vector<uint32_t> exVnU2 = {  0,      0   };
    const std::vector<uint32_t> exViU2 = {  0,      0   };
    const std::vector<uint32_t> exVuU2 = {  1,      2   };

    const std::vector<float> exV2V3 = { 0.8f,   -0.5f,  0.0f };
    const std::vector<float> exV3V3 = { 0.8f,   -0.5f,  1.0f };
    const std::vector<float> exV4V3 = { 0.707f, -0.5f,  3.1415f };
    const std::vector<float> exI2V3 = { 12.0f,  -13.0f, 0.0f  };
    const std::vector<float> exI3V3 = { 168.0f, -12.0f, 13.0f };
    const std::vector<float> exI4V3 = { 192.0f, -168.0f, 0.0f };
    const std::vector<float> exU2V3 = { 2.0f,    3.0f,  0.0f  };
    const std::vector<float> exU3V3 = { 5.0f,    8.0f,  13.0f };
    const std::vector<float> exU4V3 = { 21.0f,   34.0f, 55.0f };
    const std::vector<float> exVnV3 = { 0.1f,   -0.5f,  0.2f };
    const std::vector<float> exViV3 = {-1.0f,   -0.0f,  1.0f };
    const std::vector<float> exVuV3 = { 1.0f,    2.0f,  3.0f };

    const std::vector<int32_t> exV2I3 = {   0,    0,   0 };
    const std::vector<int32_t> exV3I3 = {   0,    0,   1 };
    const std::vector<int32_t> exV4I3 = {   0,    0,   3 };
    const std::vector<int32_t> exI2I3 = {  12,  -13,   0 };
    const std::vector<int32_t> exI3I3 = { 168,  -12,  13 };
    const std::vector<int32_t> exI4I3 = { 192, -168,   0 };
    const std::vector<int32_t> exU2I3 = {   2,    3,   0 };
    const std::vector<int32_t> exU3I3 = {   5,    8,  13 };
    const std::vector<int32_t> exU4I3 = {  21,   34,  55 };
    const std::vector<int32_t> exVnI3 = {   0,    0,   0 };
    const std::vector<int32_t> exViI3 = {  -1,    0,   1 };
    const std::vector<int32_t> exVuI3 = {   1,    2,   3 };

    const std::vector<uint32_t> exV2U3 = {   0,    0,   0 };
    const std::vector<uint32_t> exV3U3 = {   0,    0,   1 };
    const std::vector<uint32_t> exV4U3 = {   0,    0,   3 };
    const std::vector<uint32_t> exI2U3 = {  12,    0,   0 };
    const std::vector<uint32_t> exI3U3 = { 168,    0,  13 };
    const std::vector<uint32_t> exI4U3 = { 192,    0,   0 };
    const std::vector<uint32_t> exU2U3 = {   2,    3,   0 };
    const std::vector<uint32_t> exU3U3 = {   5,    8,  13 };
    const std::vector<uint32_t> exU4U3 = {  21,   34,  55 };
    const std::vector<uint32_t> exVnU3 = {   0,    0,   0 };
    const std::vector<uint32_t> exViU3 = {   0,    0,   1 };
    const std::vector<uint32_t> exVuU3 = {   1,    2,   3 };

    const std::vector<float> exV2V4 = { 0.8f,  -0.5f, 0.0f,    0.0f   };
    const std::vector<float> exV3V4 = { 0.8f,  -0.5f, 1.0f,    0.0f   };
    const std::vector<float> exV4V4 = { 0.707f,-0.5f, 3.1415f, 0.707f };
    const std::vector<float> exI2V4 = { 12.0f,  -13.0f, 0.0f,  0.0f   };
    const std::vector<float> exI3V4 = { 168.0f, -12.0f, 13.0f, 0.0f   };
    const std::vector<float> exI4V4 = { 192.0f, -168.0f, 0.0f,-1.0f   };
    const std::vector<float> exU2V4 = { 2.0f,    3.0f,  0.0f,  0.0f   };
    const std::vector<float> exU3V4 = { 5.0f,    8.0f,  13.0f, 0.0f   };
    const std::vector<float> exU4V4 = { 21.0f,   34.0f, 55.0f, 89.0f  };
    const std::vector<float> exVnV4 = { 0.1f,  -0.5f, 0.2f,   -0.4f    };
    const std::vector<float> exViV4 = {-1.0f,   0.0f, 1.0f,    2.0f    };
    const std::vector<float> exVuV4 = { 1.0f,   2.0f, 3.0f,    4.0f };

    const std::vector<int32_t> exV2I4 = {   0,    0,   0,  0   };
    const std::vector<int32_t> exV3I4 = {   0,    0,   1,  0   };
    const std::vector<int32_t> exV4I4 = {   0,    0,   3,  0   };
    const std::vector<int32_t> exI2I4 = {  12,  -13,   0,  0   };
    const std::vector<int32_t> exI3I4 = { 168,  -12,  13,  0   };
    const std::vector<int32_t> exI4I4 = { 192, -168,   0, -1   };
    const std::vector<int32_t> exU2I4 = {   2,    3,   0,  0   };
    const std::vector<int32_t> exU3I4 = {   5,    8,  13,  0   };
    const std::vector<int32_t> exU4I4 = {  21,   34,  55, 89   };
    const std::vector<int32_t> exVnI4 = {   0,    0,   0,  0   };
    const std::vector<int32_t> exViI4 = {  -1,    0,   1,  2   };
    const std::vector<int32_t> exVuI4 = {   1,    2,   3,  4   };

    const std::vector<uint32_t> exV2U4 = {   0,    0,   0,  0   };
    const std::vector<uint32_t> exV3U4 = {   0,    0,   1,  0   };
    const std::vector<uint32_t> exV4U4 = {   0,    0,   3,  0   };
    const std::vector<uint32_t> exI2U4 = {  12,    0,   0,  0   };
    const std::vector<uint32_t> exI3U4 = { 168,    0,  13,  0   };
    const std::vector<uint32_t> exI4U4 = { 192,    0,   0,  0   };
    const std::vector<uint32_t> exU2U4 = {   2,    3,   0,  0   };
    const std::vector<uint32_t> exU3U4 = {   5,    8,  13,  0   };
    const std::vector<uint32_t> exU4U4 = {  21,   34,  55, 89   };
    const std::vector<uint32_t> exVnU4 = {   0,    0,   0,  0   };
    const std::vector<uint32_t> exViU4 = {   0,    0,   1,  2   };
    const std::vector<uint32_t> exVuU4 = {   1,    2,   3,  4   };

    const std::vector<float> exV2Vn = {   0.8f,   -0.5f };
    const std::vector<float> exV3Vn = {   0.8f,   -0.5f, 1.0f };
    const std::vector<float> exV4Vn = {   0.707f, -0.5f, 3.1415f, 0.707f };
    const std::vector<float> exI2Vn = {  12.0f,  -13.0f  };
    const std::vector<float> exI3Vn = { 168.0f,  -12.0f, 13.0f          };
    const std::vector<float> exI4Vn = { 192.0f, -168.0f, 0.0f, -1.0f   };
    const std::vector<float> exU2Vn = {   2.0f,    3.0f  };
    const std::vector<float> exU3Vn = {   5.0f,    8.0f, 13.0f };
    const std::vector<float> exU4Vn = {  21.0f,   34.0f, 55.0f, 89.0f  };
    const std::vector<float> exVnVn = { 0.1f,  -0.5f, 0.2f,   -0.4f,  0.3f, -0.3f, 0.4f, -0.2f };
    const std::vector<float> exViVn = {-1.0f,   0.0f, 1.0f,    2.0f,  3.0f };
    const std::vector<float> exVuVn = { 1.0f,   2.0f, 3.0f,    4.0f,  5.0f };

    const std::vector<int32_t> exV2Vi = {   0,    0          };
    const std::vector<int32_t> exV3Vi = {   0,    0,  1      };
    const std::vector<int32_t> exV4Vi = {   0,    0,  3, 0   };
    const std::vector<int32_t> exI2Vi = {  12,  -13          };
    const std::vector<int32_t> exI3Vi = { 168,  -12, 13      };
    const std::vector<int32_t> exI4Vi = { 192, -168,  0, -1   };
    const std::vector<int32_t> exU2Vi = {   2,    3          };
    const std::vector<int32_t> exU3Vi = {   5,    8, 13      };
    const std::vector<int32_t> exU4Vi = {  21,   34, 55, 89  };
    const std::vector<int32_t> exVnVi = {   0,    0, 0,   0, 0, 0, 0, 0 };
    const std::vector<int32_t> exViVi = {  -1,    0, 1,   2, 3 };
    const std::vector<int32_t> exVuVi = {   1,    2, 3,   4, 5 };

    const std::vector<uint32_t> exV2Vu = {   0,  0 };
    const std::vector<uint32_t> exV3Vu = {   0,  0, 1 };
    const std::vector<uint32_t> exV4Vu = {   0,  0, 3, 0 };
    const std::vector<uint32_t> exI2Vu = {  12,  0          };
    const std::vector<uint32_t> exI3Vu = { 168,  0, 13      };
    const std::vector<uint32_t> exI4Vu = { 192,  0,  0, 0   };
    const std::vector<uint32_t> exU2Vu = {   2,  3          };
    const std::vector<uint32_t> exU3Vu = {   5,  8, 13      };
    const std::vector<uint32_t> exU4Vu = {  21, 34, 55, 89  };
    const std::vector<uint32_t> exVnVu = {   0, 0, 0, 0, 0, 0, 0, 0 };
    const std::vector<uint32_t> exViVu = {   0, 0, 1, 2, 3 };
    const std::vector<uint32_t> exVuVu = {   1, 2, 3, 4, 5 };

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Vec2" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::Vec2, emptyF,  false, "Int      as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::Vec2, emptyF,  false, "Count    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::Vec2, emptyF,  false, "Number   as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::Vec2, emptyF,  false, "String   as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::Vec2, emptyF,  false, "FileName as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::Vec2, exV2V2,  true,  "Vec2     as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::Vec2, exV3V2,  true,  "Vec3     as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::Vec2, exV4V2,  true,  "Vec4     as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::Vec2, exI2V2,  true,  "iVec2    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::Vec2, exI3V2,  true,  "iVec3    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::Vec2, exI4V2,  true,  "iVec4    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::Vec2, exU2V2,  true,  "uVec2    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::Vec2, exU3V2,  true,  "uVec3    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::Vec2, exU4V2,  true,  "uVec4    as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::Vec2, exVnV2,  true,  "Vector   as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::Vec2, exViV2,  true,  "VectorI  as Vec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::Vec2, exVuV2,  true,  "VectorU  as Vec2", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Vec3" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::Vec3, emptyF,  false, "Int      as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::Vec3, emptyF,  false, "Count    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::Vec3, emptyF,  false, "Number   as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::Vec3, emptyF,  false, "String   as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::Vec3, emptyF,  false, "FileName as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::Vec3, exV2V3,  true,  "Vec2     as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::Vec3, exV3V3,  true,  "Vec3     as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::Vec3, exV4V3,  true,  "Vec4     as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::Vec3, exI2V3,  true,  "iVec2    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::Vec3, exI3V3,  true,  "iVec3    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::Vec3, exI4V3,  true,  "iVec4    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::Vec3, exU2V3,  true,  "uVec2    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::Vec3, exU3V3,  true,  "uVec3    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::Vec3, exU4V3,  true,  "uVec4    as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::Vec3, exVnV3,  true,  "Vector   as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::Vec3, exViV3,  true,  "VectorI  as Vec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::Vec3, exVuV3,  true,  "VectorU  as Vec3", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Vec4" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::Vec4, emptyF,  false, "Int      as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::Vec4, emptyF,  false, "Count    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::Vec4, emptyF,  false, "Number   as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::Vec4, emptyF,  false, "String   as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::Vec4, emptyF,  false, "FileName as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::Vec4, exV2V4,  true,  "Vec2     as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::Vec4, exV3V4,  true,  "Vec3     as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::Vec4, exV4V4,  true,  "Vec4     as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::Vec4, exI2V4,  true,  "iVec2    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::Vec4, exI3V4,  true,  "iVec3    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::Vec4, exI4V4,  true,  "iVec4    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::Vec4, exU2V4,  true,  "uVec2    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::Vec4, exU3V4,  true,  "uVec3    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::Vec4, exU4V4,  true,  "uVec4    as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::Vec4, exVnV4,  true,  "Vector   as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::Vec4, exViV4,  true,  "VectorI  as Vec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::Vec4, exVuV4,  true,  "VectorU  as Vec4", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::iVec2" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::IVec2, emptyI,  false, "Int      as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::IVec2, emptyI,  false, "Count    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::IVec2, emptyI,  false, "Number   as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::IVec2, emptyI,  false, "String   as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::IVec2, emptyI,  false, "FileName as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::IVec2, exV2I2,  true,  "Vec2     as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::IVec2, exV3I2,  true,  "Vec3     as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::IVec2, exV4I2,  true,  "Vec4     as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::IVec2, exI2I2,  true,  "iVec2    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::IVec2, exI3I2,  true,  "iVec3    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::IVec2, exI4I2,  true,  "iVec4    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::IVec2, exU2I2,  true,  "uVec2    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::IVec2, exU3I2,  true,  "uVec3    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::IVec2, exU4I2,  true,  "uVec4    as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::IVec2, exVnI2,  true,  "Vector   as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::IVec2, exViI2,  true,  "VectorI  as iVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::IVec2, exVuI2,  true,  "VectorU  as iVec2", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::iVec3" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::IVec3, emptyI,  false, "Int      as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::IVec3, emptyI,  false, "Count    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::IVec3, emptyI,  false, "Number   as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::IVec3, emptyI,  false, "String   as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::IVec3, emptyI,  false, "FileName as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::IVec3, exV2I3,  true,  "Vec2     as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::IVec3, exV3I3,  true,  "Vec3     as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::IVec3, exV4I3,  true,  "Vec4     as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::IVec3, exI2I3,  true,  "iVec2    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::IVec3, exI3I3,  true,  "iVec3    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::IVec3, exI4I3,  true,  "iVec4    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::IVec3, exU2I3,  true,  "uVec2    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::IVec3, exU3I3,  true,  "uVec3    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::IVec3, exU4I3,  true,  "uVec4    as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::IVec3, exVnI3,  true,  "Vector   as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::IVec3, exViI3,  true,  "VectorI  as iVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::IVec3, exVuI3,  true,  "VectorU  as iVec3", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::iVec4" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::IVec4, emptyI,  false, "Int      as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::IVec4, emptyI,  false, "Count    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::IVec4, emptyI,  false, "Number   as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::IVec4, emptyI,  false, "String   as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::IVec4, emptyI,  false, "FileName as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::IVec4, exV2I4,  true,  "Vec2     as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::IVec4, exV3I4,  true,  "Vec3     as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::IVec4, exV4I4,  true,  "Vec4     as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::IVec4, exI2I4,  true,  "iVec2    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::IVec4, exI3I4,  true,  "iVec3    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::IVec4, exI4I4,  true,  "iVec4    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::IVec4, exU2I4,  true,  "uVec2    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::IVec4, exU3I4,  true,  "uVec3    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::IVec4, exU4I4,  true,  "uVec4    as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::IVec4, exVnI4,  true,  "Vector   as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::IVec4, exViI4,  true,  "VectorI  as iVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::IVec4, exVuI4,  true,  "VectorU  as iVec4", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::uVec2" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::UVec2, emptyU,  false, "Int      as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::UVec2, emptyU,  false, "Count    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::UVec2, emptyU,  false, "Number   as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::UVec2, emptyU,  false, "String   as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::UVec2, emptyU,  false, "FileName as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::UVec2, exV2U2,  true,  "Vec2     as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::UVec2, exV3U2,  true,  "Vec3     as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::UVec2, exV4U2,  true,  "Vec4     as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::UVec2, exI2U2,  true,  "iVec2    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::UVec2, exI3U2,  true,  "iVec3    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::UVec2, exI4U2,  true,  "iVec4    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::UVec2, exU2U2,  true,  "uVec2    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::UVec2, exU3U2,  true,  "uVec3    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::UVec2, exU4U2,  true,  "uVec4    as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::UVec2, exVnU2,  true,  "Vector   as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::UVec2, exViU2,  true,  "VectorI  as uVec2", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::UVec2, exVuU2,  true,  "VectorU  as uVec2", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::uVec3" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::UVec3, emptyU,  false, "Int      as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::UVec3, emptyU,  false, "Count    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::UVec3, emptyU,  false, "Number   as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::UVec3, emptyU,  false, "String   as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::UVec3, emptyU,  false, "FileName as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::UVec3, exV2U3,  true,  "Vec2     as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::UVec3, exV3U3,  true,  "Vec3     as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::UVec3, exV4U3,  true,  "Vec4     as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::UVec3, exI2U3,  true,  "iVec2    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::UVec3, exI3U3,  true,  "iVec3    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::UVec3, exI4U3,  true,  "iVec4    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::UVec3, exU2U3,  true,  "uVec2    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::UVec3, exU3U3,  true,  "uVec3    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::UVec3, exU4U3,  true,  "uVec4    as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::UVec3, exVnU3,  true,  "Vector   as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::UVec3, exViU3,  true,  "VectorI  as uVec3", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::UVec3, exVuU3,  true,  "VectorU  as uVec3", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::uVec4" );
    testFromYAMLArray( Unit, srcInt,      AnyType::Types::UVec4, emptyU,  false, "Int      as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcCount,    AnyType::Types::UVec4, emptyU,  false, "Count    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcNumber,   AnyType::Types::UVec4, emptyU,  false, "Number   as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcString,   AnyType::Types::UVec4, emptyU,  false, "String   as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcFileName, AnyType::Types::UVec4, emptyU,  false, "FileName as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec2,     AnyType::Types::UVec4, exV2U4,  true,  "Vec2     as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec3,     AnyType::Types::UVec4, exV3U4,  true,  "Vec3     as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVec4,     AnyType::Types::UVec4, exV4U4,  true,  "Vec4     as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec2,    AnyType::Types::UVec4, exI2U4,  true,  "iVec2    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec3,    AnyType::Types::UVec4, exI3U4,  true,  "iVec3    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcIVec4,    AnyType::Types::UVec4, exI4U4,  true,  "iVec4    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec2,    AnyType::Types::UVec4, exU2U4,  true,  "uVec2    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec3,    AnyType::Types::UVec4, exU3U4,  true,  "uVec3    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcUVec4,    AnyType::Types::UVec4, exU4U4,  true,  "uVec4    as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVector,   AnyType::Types::UVec4, exVnU4,  true,  "Vector   as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorI,  AnyType::Types::UVec4, exViU4,  true,  "VectorI  as uVec4", CHECKPOINT );
    testFromYAMLArray( Unit, srcVectorU,  AnyType::Types::UVec4, exVuU4,  true,  "VectorU  as uVec4", CHECKPOINT );
#ifdef QT_DEBU
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::Vector" );
    testFromYAMLVector( Unit, srcInt,      AnyType::Types::Vector, emptyF,  false, "Int      as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcCount,    AnyType::Types::Vector, emptyF,  false, "Count    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcNumber,   AnyType::Types::Vector, emptyF,  false, "Number   as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcString,   AnyType::Types::Vector, emptyF,  false, "String   as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcFileName, AnyType::Types::Vector, emptyF,  false, "FileName as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec2,     AnyType::Types::Vector, exV2Vn,  true,  "Vec2     as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec3,     AnyType::Types::Vector, exV3Vn,  true,  "Vec3     as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec4,     AnyType::Types::Vector, exV4Vn,  true,  "Vec4     as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec2,    AnyType::Types::Vector, exI2Vn,  true,  "iVec2    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec3,    AnyType::Types::Vector, exI3Vn,  true,  "iVec3    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec4,    AnyType::Types::Vector, exI4Vn,  true,  "iVec4    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec2,    AnyType::Types::Vector, exU2Vn,  true,  "uVec2    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec3,    AnyType::Types::Vector, exU3Vn,  true,  "uVec3    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec4,    AnyType::Types::Vector, exU4Vn,  true,  "uVec4    as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcVector,   AnyType::Types::Vector, exVnVn,  true,  "Vector   as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcVectorI,  AnyType::Types::Vector, exViVn,  true,  "VectorI  as Vector", CHECKPOINT );
    testFromYAMLVector( Unit, srcVectorU,  AnyType::Types::Vector, exVuVn,  true,  "VectorU  as Vector", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::VectorI" );
    testFromYAMLVector( Unit, srcInt,      AnyType::Types::VectorI, emptyI,  false, "Int      as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcCount,    AnyType::Types::VectorI, emptyI,  false, "Count    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcNumber,   AnyType::Types::VectorI, emptyI,  false, "Number   as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcString,   AnyType::Types::VectorI, emptyI,  false, "String   as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcFileName, AnyType::Types::VectorI, emptyI,  false, "FileName as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec2,     AnyType::Types::VectorI, exV2Vi,  true,  "Vec2     as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec3,     AnyType::Types::VectorI, exV3Vi,  true,  "Vec3     as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec4,     AnyType::Types::VectorI, exV4Vi,  true,  "Vec4     as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec2,    AnyType::Types::VectorI, exI2Vi,  true,  "iVec2    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec3,    AnyType::Types::VectorI, exI3Vi,  true,  "iVec3    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec4,    AnyType::Types::VectorI, exI4Vi,  true,  "iVec4    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec2,    AnyType::Types::VectorI, exU2Vi,  true,  "uVec2    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec3,    AnyType::Types::VectorI, exU3Vi,  true,  "uVec3    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec4,    AnyType::Types::VectorI, exU4Vi,  true,  "uVec4    as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcVector,   AnyType::Types::VectorI, exVnVi,  true,  "Vector   as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcVectorI,  AnyType::Types::VectorI, exViVi,  true,  "VectorI  as VectorI", CHECKPOINT );
    testFromYAMLVector( Unit, srcVectorU,  AnyType::Types::VectorI, exVuVi,  true,  "VectorU  as VectorI", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("------------------------------------------------------------");
    qDebug(  "read into AnyType::Types::VectorU" );
    testFromYAMLVector( Unit, srcInt,      AnyType::Types::VectorU, emptyF,  false, "Int      as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcCount,    AnyType::Types::VectorU, emptyF,  false, "Count    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcNumber,   AnyType::Types::VectorU, emptyF,  false, "Number   as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcString,   AnyType::Types::VectorU, emptyF,  false, "String   as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcFileName, AnyType::Types::VectorU, emptyF,  false, "FileName as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec2,     AnyType::Types::VectorU, exV2Vu,  true,  "Vec2     as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec3,     AnyType::Types::VectorU, exV3Vu,  true,  "Vec3     as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcVec4,     AnyType::Types::VectorU, exV4Vu,  true,  "Vec4     as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec2,    AnyType::Types::VectorU, exI2Vu,  true,  "iVec2    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec3,    AnyType::Types::VectorU, exI3Vu,  true,  "iVec3    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcIVec4,    AnyType::Types::VectorU, exI4Vu,  true,  "iVec4    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec2,    AnyType::Types::VectorU, exU2Vu,  true,  "uVec2    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec3,    AnyType::Types::VectorU, exU3Vu,  true,  "uVec3    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcUVec4,    AnyType::Types::VectorU, exU4Vu,  true,  "uVec4    as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcVector,   AnyType::Types::VectorU, exVnVu,  true,  "Vector   as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcVectorI,  AnyType::Types::VectorU, exViVu,  true,  "VectorI  as VectorU", CHECKPOINT );
    testFromYAMLVector( Unit, srcVectorU,  AnyType::Types::VectorU, exVuVu,  true,  "VectorU  as VectorU", CHECKPOINT );
#ifdef QT_DEBUG
    CHECK( Unit.allocate_count, 1 );
#endif

    qDebug("=========================  PASS  ===========================");

}

void TestAnyType::initUnitFromSrcYAML( AnyType& Unit, const AnyType::Types type )
{
    switch( type )
    {
        case AnyType::Types::Empty:     Unit.destroy(); break;
        case AnyType::Types::Int:       Unit.fromYAML( AnyType::Types::Int,      srcInt );      break;
        case AnyType::Types::Count:     Unit.fromYAML( AnyType::Types::Count,    srcCount );    break;
        case AnyType::Types::String:    Unit.fromYAML( AnyType::Types::String,   srcString );   break;
        case AnyType::Types::Filename:  Unit.fromYAML( AnyType::Types::Filename, srcFileName ); break;
        case AnyType::Types::Number:    Unit.fromYAML( AnyType::Types::Number,   srcNumber );   break;
        case AnyType::Types::Vec2:      Unit.fromYAML( AnyType::Types::Vec2,     srcVec2 );     break;
        case AnyType::Types::Vec3:      Unit.fromYAML( AnyType::Types::Vec3,     srcVec3 );     break;
        case AnyType::Types::Vec4:      Unit.fromYAML( AnyType::Types::Vec4,     srcVec4 );     break;
        case AnyType::Types::IVec2:     Unit.fromYAML( AnyType::Types::IVec2,    srcIVec2 );    break;
        case AnyType::Types::IVec3:     Unit.fromYAML( AnyType::Types::IVec3,    srcIVec3 );    break;
        case AnyType::Types::IVec4:     Unit.fromYAML( AnyType::Types::IVec4,    srcIVec4 );    break;
        case AnyType::Types::UVec2:     Unit.fromYAML( AnyType::Types::UVec2,    srcUVec2 );    break;
        case AnyType::Types::UVec3:     Unit.fromYAML( AnyType::Types::UVec3,    srcUVec3 );    break;
        case AnyType::Types::UVec4:     Unit.fromYAML( AnyType::Types::UVec4,    srcUVec4 );    break;
        case AnyType::Types::Vector:    Unit.fromYAML( AnyType::Types::Vector,   srcVector );   break;
        case AnyType::Types::VectorI:   Unit.fromYAML( AnyType::Types::VectorI,  srcVectorI );  break;
        case AnyType::Types::VectorU:   Unit.fromYAML( AnyType::Types::VectorU,  srcVectorU );  break;
        default:
            qCritical().noquote() << "No initialization is defined for type" << type;
            QVERIFY( false );
            break;
    }
}

void TestAnyType::testUnitFromSrcData( AnyType& Unit, const AnyType::Types type, const char* typetext, const QString where )
{
    const int32_t     exInt          = -15;
    const uint32_t    exCount        = 223;
    const float       exNum          = -3.1415926f;
    const std::string exStr          = "Hello world";
    const std::string exFile         = "/tmp/testfile";
    const std::vector<float> exV2    = { 0.8f,  -0.5f };
    const std::vector<float> exV3    = { 0.8f,  -0.5f, 1.0f };
    const std::vector<float> exV4    = { 0.707f,-0.5f, 3.1415f, 0.707f };
    const std::vector<float> exVn    = { 0.1f,  -0.5f, 0.2f,   -0.4f,  0.3f, -0.3f, 0.4f, -0.2f };
    const std::vector<int32_t>  exVi = {-1, 0, 1, 2, 3 };
    const std::vector<uint32_t> exVu = { 1, 2, 3, 4, 5 };
    const std::vector<int32_t>  exI2 = { 12, -13 };
    const std::vector<int32_t>  exI3 = { 168, -12, 13 };
    const std::vector<int32_t>  exI4 = { 192, -168, 0, -1 };
    const std::vector<uint32_t> exU2 = { 2, 3 };
    const std::vector<uint32_t> exU3 = { 5, 8, 13 };
    const std::vector<uint32_t> exU4 = { 21, 34, 55, 89 };

    switch( type )
    {
        case AnyType::Types::Empty:     Unit.destroy(); break;
        case AnyType::Types::Int:       testInplace< int32_t>   ( Unit, exInt,   typetext, where ); break;
        case AnyType::Types::Count:     testInplace<uint32_t>   ( Unit, exCount, typetext, where ); break;
        case AnyType::Types::String:    testDirect<std::string> ( Unit, exStr,   typetext, where ); break;
        case AnyType::Types::Filename:  testDirect<std::string> ( Unit, exFile,  typetext, where ); break;
        case AnyType::Types::Number:    testInplace< float >    ( Unit, exNum,   typetext, where ); break;
        case AnyType::Types::Vec2:      testArray< float >      ( Unit, false, exV2, typetext, where ); break;
        case AnyType::Types::Vec3:      testArray< float >      ( Unit, false, exV3, typetext, where ); break;
        case AnyType::Types::Vec4:      testArray< float >      ( Unit, false, exV4, typetext, where ); break;
        case AnyType::Types::IVec2:     testArray< int32_t >    ( Unit, false, exI2, typetext, where ); break;
        case AnyType::Types::IVec3:     testArray< int32_t >    ( Unit, false, exI3, typetext, where ); break;
        case AnyType::Types::IVec4:     testArray< int32_t >    ( Unit, false, exI4, typetext, where ); break;
        case AnyType::Types::UVec2:     testArray< uint32_t >   ( Unit, false, exU2, typetext, where ); break;
        case AnyType::Types::UVec3:     testArray< uint32_t >   ( Unit, false, exU3, typetext, where ); break;
        case AnyType::Types::UVec4:     testArray< uint32_t >   ( Unit, false, exU4, typetext, where ); break;
        case AnyType::Types::Vector:    testVector< float >     ( Unit, false, exVn, typetext, where ); break;
        case AnyType::Types::VectorI:   testVector< int32_t >   ( Unit, false, exVi, typetext, where );  break;
        case AnyType::Types::VectorU:   testVector< uint32_t >  ( Unit, false, exVu, typetext, where );  break;
        default:
            qCritical().noquote() << "No test from source data is defined for type" << type;
            QVERIFY( false );
            break;
    }
}

void TestAnyType::Clone()
{

    AnyType srcUnit;
    qDebug("------------------------------------------------------------");
    //qDebug(  "" );
    for( int i = 0; i < int( AnyType::Types::StrList ); ++i )
    {
        qDebug().noquote().nospace() << "From " << AnyType::getTypeAsStr( AnyType::Types( i ) ).c_str();
        AnyType dstUnit;
        initUnitFromSrcYAML( srcUnit, AnyType::Types( i ) );
        testUnitFromSrcData( srcUnit, AnyType::Types( i ), "Check src prior cloning", CHECKPOINT );

#ifdef QT_DEBUG
    CHECK( srcUnit.allocate_count, ( srcUnit.isInPlace() ? 0 : 1 ) );
#endif
        for( int j = 0; j < int( AnyType::Types::StrList ); ++j )
        {
            //qDebug().noquote().nospace() << "  to " << AnyType::TypeNamesRev[ j ].name.c_str();
            initUnitFromSrcYAML( dstUnit, AnyType::Types( j ) );
            srcUnit.clone( &dstUnit );
#ifdef QT_DEBUG
            CHECK( srcUnit.allocate_count, ( srcUnit.isInPlace() ? 0 : 1 ) );
            CHECK( dstUnit.allocate_count, ( srcUnit.isInPlace() ? 0 : 1 ) );
#endif
            if ( srcUnit.isInPlace() == false )
            {
                CHECK( ( srcUnit.data != dstUnit.data ), true );
            }

            //qDebug().noquote().nospace() << "  got " << AnyType::TypeNamesRev[ static_cast< int >( dstUnit.getType() ) ].name.c_str();

            testUnitFromSrcData( dstUnit, AnyType::Types( i ), "Check data after cloning", CHECKPOINT );

            //qDebug().noquote().nospace() << " ---- ";
        }
        qDebug( "------------------------" );
    }

    qDebug("=========================  PASS  ===========================");

}

void TestAnyType::Size()
{
    qDebug("------------------------------------------------------------");
    CHECK( AnyType::getSize( AnyType::Types::Empty     ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::Int       ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::Count     ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::String    ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::Filename  ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::Number    ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::Vec2      ), 2 );
    CHECK( AnyType::getSize( AnyType::Types::Vec3      ), 3 );
    CHECK( AnyType::getSize( AnyType::Types::Vec4      ), 4 );
    CHECK( AnyType::getSize( AnyType::Types::IVec2     ), 2 );
    CHECK( AnyType::getSize( AnyType::Types::IVec3     ), 3 );
    CHECK( AnyType::getSize( AnyType::Types::IVec4     ), 4 );
    CHECK( AnyType::getSize( AnyType::Types::UVec2     ), 2 );
    CHECK( AnyType::getSize( AnyType::Types::UVec3     ), 3 );
    CHECK( AnyType::getSize( AnyType::Types::UVec4     ), 4 );
    CHECK( AnyType::getSize( AnyType::Types::Vector    ), 0 );
    CHECK( AnyType::getSize( AnyType::Types::VectorI   ), 0 );
    CHECK( AnyType::getSize( AnyType::Types::VectorU   ), 0 );
    CHECK( AnyType::getSize( AnyType::Types::StrList   ), 0 );
    CHECK( AnyType::getSize( AnyType::Types::Image     ), 1 );
    CHECK( AnyType::getSize( AnyType::Types::TensorCPU ), 1 );
    qDebug("=========================  PASS  ===========================");
}
QDebug& operator<<( QDebug& dbg, const std::string& str )
{
    dbg << str.c_str();
    return dbg;
}
//template
