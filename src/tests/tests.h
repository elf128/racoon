/*
 * tests.h
 *
 *  Created on: Sep 16, 2021
 *      Author: Vlad A. <elf128@gmail.com>
 */

#ifndef SRC_TESTS_TESTS_H_
#define SRC_TESTS_TESTS_H_

//#define LOG_READ_YAML

#include <QtTest/QtTest>
#include <QObject>
#include <QDebug>

#include "test_AnyType.h"

#define CHECKPOINT ( QString("%1:%2: error: Checked here").arg( __FILE__).arg(__LINE__) )

#define toStr2( A ) A
#define toStr( A ) toStr2( #A )

#define CHECK3( a,b, type, err ) if(a!=b) {\
    qCritical().noquote().nospace() << "\n" << __FILE__ << ":" << __LINE__ << ": error: " << type <<" check failed."; \
    qCritical().noquote() << #a << "≠" << #b;\
    qCritical().noquote() << #a << "=" << a;\
    qCritical().noquote() << #b << "=" << b;\
    qCritical().noquote() << err;\
    QVERIFY( false );}\

#define CHECK2( a, b, type ) CHECK3(a,b,type,"")

#define CHECK( a, b ) CHECK3(a,b,"","")

QDebug& operator<<( QDebug& dbg, const std::string& str );

#endif /* SRC_TESTS_TESTS_H_ */
