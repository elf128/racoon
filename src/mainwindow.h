#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "DockManager.h"

#include "ui/UI_NodeGraph.h"
#include "ui/UI_Viewer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void openFile( const QString& fileName );

private slots:
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSave_As_triggered();
    void on_actionViewer_toggled( bool show );
    void on_actionNew_triggered();
    void on_ViewerCloseRequested();

private:
    Ui::MainWindow*    ui;
    ads::CDockManager* m_DockManager;
    UI_Viewer*         m_viewer;

    QAction*           m_stop;
    QAction*           m_step;
    QAction*           m_play;

    UI_NodeGraph* createMdiChild( NG_Structure* name );
    UI_Viewer*    createViewerChild();
    UI_NodeGraph* getCurrentMdi();
};
#endif // MAINWINDOW_H
