QT       += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#YAML_CPP_INCLUDE = /usr
CONFIG += c++11

DEFINES += ADS_STATIC
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include($${PROPERTIES_PRI})

SOURCES += \
	core/UID.cpp \
    core/Global.cpp \
    core/AnyType.cpp \
    core/Compute.cpp \
    model/Node.cpp \
    model/Structure.cpp \
    model/Registry.cpp \
    model/Reflection.cpp \
    model/Shadow.cpp \
    reflection/M_Basic.cpp \
    reflection/Ref_FC.cpp \
    reflection/Ref_Img.cpp \
    reflection/Ref_VecSources.cpp \
    ui/UI_Canvas.cpp \
    ui/UI_Link.cpp \
    ui/UI_LinkDrag.cpp \
    ui/UI_Node.cpp \
    ui/UI_Scene.cpp \
    ui/UI_Socket.cpp \
    ui/UI_NodeGraph.cpp \
    ui/UI_Props.cpp \
    ui/UI_TripleDot.cpp \
    ui/UI_ViewSign.cpp \
    ui/UI_Viewer.cpp \
    ui/Form_ModelTypeSelector.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    core/Global.h \
    core/UID.h \
    core/AnyType.h \
    core/Compute.h \
    model/Node.h \
    model/Structure.h \
    model/Registry.h \
    model/Reflection.h \
    model/Shadow.h \
    reflection/M_Basic.h \
    reflection/Ref_FC.h \
    reflection/Ref_Img.h \
    reflection/Ref_VecSources.h \
    ui/UI_Canvas.h \
    ui/UI_Link.h \
    ui/UI_LinkDrag.h \
    ui/UI_Node.h \
    ui/UI_Scene.h \
    ui/UI_Socket.h \
    ui/UI_NodeGraph.h \
    ui/UI_Props.h \
    ui/UI_TripleDot.h \
    ui/UI_ViewSign.h \
    ui/UI_Viewer.h \
    ui/Form_ModelTypeSelector.h \
    mainwindow.h 

FORMS += \
    ui/NodeGraph.ui \
    ui/ModelTypeSelector.ui \
    ui/mainwindow.ui 
#    ui/View2D.ui \
#    ui/View3D.ui \
#    ui/ViewData.ui

RESOURCES += \
	../res/Res.qrc

TRANSLATIONS += \
    Racoon_en_US.ts

####
##  ADS stuff

RESOURCES += ads\ads.qrc

HEADERS += \
    ads/ads_globals.h \
    ads/DockAreaWidget.h \
    ads/DockAreaTabBar.h \
    ads/DockContainerWidget.h \
    ads/DockManager.h \
    ads/DockWidget.h \
    ads/DockWidgetTab.h \
    ads/DockingStateReader.h \
    ads/DockOverlay.h \
    ads/DockSplitter.h \
    ads/DockAreaTitleBar_p.h \
    ads/DockAreaTitleBar.h \
    ads/FloatingDockContainer.h \
    ads/FloatingDragPreview.h \
    ads/ElidingLabel.h \
    ads/IconProvider.h \
    ads/DockFocusController.h \
    ads/DockComponentsFactory.h

SOURCES += \
    ads/ads_globals.cpp \
    ads/DockAreaWidget.cpp \
    ads/DockAreaTabBar.cpp \
    ads/DockContainerWidget.cpp \
    ads/DockManager.cpp \
    ads/DockWidget.cpp \
    ads/DockingStateReader.cpp \
    ads/DockWidgetTab.cpp \
    ads/FloatingDockContainer.cpp \
    ads/FloatingDragPreview.cpp \
    ads/DockOverlay.cpp \
    ads/DockSplitter.cpp \
    ads/DockAreaTitleBar.cpp \
    ads/ElidingLabel.cpp \
    ads/IconProvider.cpp \
    ads/DockComponentsFactory.cpp \
    ads/DockFocusController.cpp

unix:!macx {
HEADERS += ads/linux/FloatingWidgetTitleBar.h
SOURCES += ads/linux/FloatingWidgetTitleBar.cpp
QT += x11extras
LIBS += -lxcb -lyaml-cpp
INCLUDEPATH += /usr/include/python3.8
}

INCLUDEPATH += ads
INCLUDEPATH += $${INCLUDE_EXT}
LIBS += -L$${INCLUDE_EXT} -lmicroNN

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
