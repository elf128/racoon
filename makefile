
JOB_NUMBER   = -j6

QT152_DIR    = /fast/usr/Qt/5.15.2
QT132_DIR    = /fast/usr/Qt/5.13.2
QT128_DIR    = /fast/usr/Qt/5.12.8
QT123_DIR    = /fast/usr/Qt/5.12.3
QT96_DIR     = /fast/usr/Qt/5.9.6
QT92_DIR     = /fast/usr/Qt/5.9.2

QT152_ID     = qt5_15_2
QT132_ID     = qt5_13_2
QT128_ID     = qt5_12_8
QT123_ID     = qt5_12_3
QT96_ID      = qt5_9_6
QT92_ID      = qt5_9_2

LINUX_BIN    = gcc_64/bin

BUILD_DIR_R_LIN64_Q152 = $(PROJECT_ROOT)/.build/x64_release_$(QT152_ID)
BUILD_DIR_D_LIN64_Q152 = $(PROJECT_ROOT)/.build/x64_debug_$(QT152_ID)
BUILD_DIR_P_LIN64_Q152 = $(PROJECT_ROOT)/.build/x64_profile_$(QT152_ID)

BUILD_DIR_R_LIN64_Q132 = $(PROJECT_ROOT)/.build/x64_release_$(QT132_ID)
BUILD_DIR_D_LIN64_Q132 = $(PROJECT_ROOT)/.build/x64_debug_$(QT132_ID)
BUILD_DIR_P_LIN64_Q132 = $(PROJECT_ROOT)/.build/x64_profile_$(QT132_ID)

BUILD_DIR_R_LIN64_Q128 = $(PROJECT_ROOT)/.build/x64_release_$(QT128_ID)
BUILD_DIR_D_LIN64_Q128 = $(PROJECT_ROOT)/.build/x64_debug_$(QT128_ID)
BUILD_DIR_P_LIN64_Q128 = $(PROJECT_ROOT)/.build/x64_profile_$(QT128_ID)

BUILD_DIR_R_LIN64_Q123 = $(PROJECT_ROOT)/.build/x64_release_$(QT123_ID)
BUILD_DIR_D_LIN64_Q123 = $(PROJECT_ROOT)/.build/x64_debug_$(QT123_ID)
BUILD_DIR_P_LIN64_Q123 = $(PROJECT_ROOT)/.build/x64_profile_$(QT123_ID)

BUILD_DIR_R_LIN64_Q96  = $(PROJECT_ROOT)/.build/x64_release_$(QT96_ID)
BUILD_DIR_D_LIN64_Q96  = $(PROJECT_ROOT)/.build/x64_debug_$(QT96_ID)
BUILD_DIR_P_LIN64_Q96  = $(PROJECT_ROOT)/.build/x64_profile_$(QT96_ID)

BUILD_DIR_R_LIN64_Q92  = $(PROJECT_ROOT)/.build/x64_release_$(QT92_ID)
BUILD_DIR_D_LIN64_Q92  = $(PROJECT_ROOT)/.build/x64_debug_$(QT92_ID)
BUILD_DIR_P_LIN64_Q92  = $(PROJECT_ROOT)/.build/x64_profile_$(QT92_ID)

BUILD_DIR_R_TESTS_Q152 = $(PROJECT_ROOT)/.build/test_release_$(QT152_ID)
BUILD_DIR_D_TESTS_Q152 = $(PROJECT_ROOT)/.build/test_debug_$(QT152_ID)
BUILD_DIR_P_TESTS_Q152 = $(PROJECT_ROOT)/.build/test_profile_$(QT152_ID)

BUILD_DIR_R_TESTS_Q132 = $(PROJECT_ROOT)/.build/test_release_$(QT132_ID)
BUILD_DIR_D_TESTS_Q132 = $(PROJECT_ROOT)/.build/test_debug_$(QT132_ID)
BUILD_DIR_P_TESTS_Q132 = $(PROJECT_ROOT)/.build/test_profile_$(QT132_ID)

BUILD_DIR_R_TESTS_Q128 = $(PROJECT_ROOT)/.build/test_release_$(QT128_ID)
BUILD_DIR_D_TESTS_Q128 = $(PROJECT_ROOT)/.build/test_debug_$(QT128_ID)
BUILD_DIR_P_TESTS_Q128 = $(PROJECT_ROOT)/.build/test_profile_$(QT128_ID)

BUILD_DIR_R_TESTS_Q123 = $(PROJECT_ROOT)/.build/test_release_$(QT123_ID)
BUILD_DIR_D_TESTS_Q123 = $(PROJECT_ROOT)/.build/test_debug_$(QT123_ID)
BUILD_DIR_P_TESTS_Q123 = $(PROJECT_ROOT)/.build/test_profile_$(QT123_ID)

BUILD_DIR_R_TESTS_Q96  = $(PROJECT_ROOT)/.build/test_release_$(QT96_ID)
BUILD_DIR_D_TESTS_Q96  = $(PROJECT_ROOT)/.build/test_debug_$(QT96_ID)
BUILD_DIR_P_TESTS_Q96  = $(PROJECT_ROOT)/.build/test_profile_$(QT96_ID)

BUILD_DIR_R_TESTS_Q92  = $(PROJECT_ROOT)/.build/test_release_$(QT92_ID)
BUILD_DIR_D_TESTS_Q92  = $(PROJECT_ROOT)/.build/test_debug_$(QT92_ID)
BUILD_DIR_P_TESTS_Q92  = $(PROJECT_ROOT)/.build/test_profile_$(QT92_ID)

L_MAKE = /usr/bin/make

PROJECT_ROOT = $(abspath $(PWD))
PRO_FILE     = $(PROJECT_ROOT)/src/Racoon.pro
PRO_TEST     = $(PROJECT_ROOT)/src/Tests.pro

MICRONN_DIR    = $(PROJECT_ROOT)/../microNN
export PRO_LIB = $(PROJECT_ROOT)/../microNN/microNN.pro

INCLUDE_EXT_D  = $(PROJECT_ROOT)/.build/microNN_d
INCLUDE_EXT_P  = $(PROJECT_ROOT)/.build/microNN_p
INCLUDE_EXT_R  = $(PROJECT_ROOT)/.build/microNN_p

PROPERTIES_PRI = /Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowser.pri
###

RELEASE_QMAKE_FLAGS = -spec linux-g++
DEBUG_QMAKE_FLAGS   = -spec linux-g++ CONFIG+=debug 
PROFILE_QMAKE_FLAGS = -spec linux-g++ CONFIG+=force_debug_info CONFIG+=separate_debug_info

##
microNN_x64_d_q%:
	$(MAKE) -f $(MICRONN_DIR)/makefile lib_x64_d_q${*} -C $(PROJECT_ROOT)

microNN_x64_p_q%:
	$(MAKE) -f $(MICRONN_DIR)/makefile lib_x64_p_q${*} -C $(PROJECT_ROOT)

microNN_x64_r_q%:
	$(MAKE) -f $(MICRONN_DIR)/makefile lib_x64_p_q${*} -C $(PROJECT_ROOT)

###
qmake_x64_d_q%:
	@echo "qmake for debug"
	mkdir -p $(BUILD_DIR_D_LIN64_Q${*})
	cd $(BUILD_DIR_D_LIN64_Q${*}) && $(QT${*}_DIR)/$(LINUX_BIN)/qmake $(PRO_FILE) $(DEBUG_QMAKE_FLAGS) INCLUDE_EXT=$(INCLUDE_EXT_D)_$(QT${*}_ID) PROPERTIES_PRI=$(QT${*}_DIR)$(PROPERTIES_PRI) && $(L_MAKE) qmake_all

qmake_x64_r_q%:
	@echo "qmake for release"
	mkdir -p $(BUILD_DIR_R_LIN64_Q${*})
	cd $(BUILD_DIR_R_LIN64_Q${*}) && $(QT${*}_DIR)/$(LINUX_BIN)/qmake $(PRO_FILE) $(RELEASE_QMAKE_FLAGS) INCLUDE_EXT=$(INCLUDE_EXT_R)_$(QT${*}_ID) PROPERTIES_PRI=$(QT${*}_DIR)$(PROPERTIES_PRI) && $(L_MAKE) qmake_all

qmake_x64_p_q%:
	@echo "qmake for profile"
	mkdir -p $(BUILD_DIR_P_LIN64_Q${*})
	cd $(BUILD_DIR_P_LIN64_Q${*}) && $(QT${*}_DIR)/$(LINUX_BIN)/qmake $(PRO_FILE) $(PROFILE_QMAKE_FLAGS) INCLUDE_EXT=$(INCLUDE_EXT_P)_$(QT${*}_ID) PROPERTIES_PRI=$(QT${*}_DIR)$(PROPERTIES_PRI) && $(L_MAKE) qmake_all

$(BUILD_DIR_D_LIN64_Q132)/makefile: qmake_x64_d_q132

###
x64_d_q%: microNN_x64_d_q% qmake_x64_d_q%
	cd $(BUILD_DIR_D_LIN64_Q${*}) && $(L_MAKE) $(JOB_NUMBER)

x64_r_q%: microNN_x64_r_q% qmake_x64_r_q%
	cd $(BUILD_DIR_R_LIN64_Q${*}) && $(L_MAKE) $(JOB_NUMBER)

x64_p_q%: microNN_x64_p_q% qmake_x64_p_q%
	cd $(BUILD_DIR_P_LIN64_Q${*}) && $(L_MAKE) $(JOB_NUMBER)

###
mk_tests_d_q%:
	@echo "Racoon: making tests"
	@mkdir -p $(BUILD_DIR_D_TESTS_Q${*})
	@cd $(BUILD_DIR_D_TESTS_Q${*}) && $(QT${*}_DIR)/$(LINUX_BIN)/qmake $(PRO_TEST) $(DEBUG_QMAKE_FLAGS) INCLUDE_EXT=$(INCLUDE_EXT_D)_$(QT${*}_ID) && $(L_MAKE) qmake_all
	@cd $(BUILD_DIR_D_TESTS_Q${*}) && $(L_MAKE) $(JOB_NUMBER) 

mk_tests_p_q%:
	@echo "Racoon: making tests"
	@mkdir -p $(BUILD_DIR_P_TESTS_Q${*})
	@cd $(BUILD_DIR_P_TESTS_Q${*}) && $(QT${*}_DIR)/$(LINUX_BIN)/qmake $(PRO_TEST) $(DEBUG_QMAKE_FLAGS) INCLUDE_EXT=$(INCLUDE_EXT_P)_$(QT${*}_ID) && $(L_MAKE) qmake_all
	@cd $(BUILD_DIR_P_TESTS_Q${*}) && $(L_MAKE) $(JOB_NUMBER) 

###
tests_d_q%: mk_tests_d_q%
	@echo
	@echo "Racoon: Running tests in debug"
	@cd $(PROJECT_ROOT) && $(BUILD_DIR_D_TESTS_Q${*})/Tests

tests_p_q%: mk_tests_p_q%
	@echo
	@echo "Racoon: Running tests in profile"
	@cd $(PROJECT_ROOT) && $(BUILD_DIR_P_TESTS_Q${*})/Tests

###
clean_d_q%:
	cd $(BUILD_DIR_D_LIN64_Q${*}) && $(L_MAKE) clean $(JOB_NUMBER)

clean_r_q%:
	cd $(BUILD_DIR_R_LIN64_Q${*}) && $(L_MAKE) clean $(JOB_NUMBER)

clean_p_q%:
	cd $(BUILD_DIR_P_LIN64_Q${*}) && $(L_MAKE) clean $(JOB_NUMBER)

clean_tests_d_q%:
	cd $(BUILD_DIR_D_TESTS_Q${*}) && $(L_MAKE) clean $(JOB_NUMBER)

clean_tests_p_q%:
	cd $(BUILD_DIR_P_TESTS_Q${*}) && $(L_MAKE) clean $(JOB_NUMBER)

clean_all_q%: clean_d_q% clean_r_q% clean_p_q% clean_tests_d_q% clean_tests_p_q%

###
all: all_q152 all_q132 all_q128 all_q123 all_q96 all_q92
